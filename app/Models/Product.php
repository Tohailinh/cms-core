<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Product
 * @package App\Models
 * @version December 3, 2018, 10:51 pm +07
 *
 * @property \App\Models\Category category
 * @property \App\Models\Country country
 * @property \App\Models\Provider provider
 * @property \App\Models\User user
 * @property \Illuminate\Database\Eloquent\Collection BillDetail
 * @property \Illuminate\Database\Eloquent\Collection news
 * @property \Illuminate\Database\Eloquent\Collection ProductImage
 * @property string name
 * @property string type
 * @property string style
 * @property string alias
 * @property string size
 * @property string image
 * @property string thumb
 * @property boolean price
 * @property boolean price_sale
 * @property string description
 * @property string title_seo
 * @property string description_seo
 * @property string keyword_seo
 * @property string content
 * @property boolean status
 * @property integer category_id
 * @property integer provider_id
 * @property integer country_id
 * @property integer user_id
 */
class Product extends Model
{
    use SoftDeletes;

    public $table = 'products';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'type',
        'style',
        'alias',
        'size',
        'image',
        'thumb',
        'price',
        'price_sale',
        'description',
        'title_seo',
        'description_seo',
        'keyword_seo',
        'content',
        'content_info',
        'note',
        'follow',
        'seat',
        'status',
        'type',
        'category_id',
        'provider_id',
        'country_id',
        'user_id',
        'start_point',
        'end_point',
        'sum_time',
        'transport',
        'date_start',
        'date_end',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [

    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|max:255',
        'category_id' => 'required',
        'date_start' => 'required',
        'date_end' => 'required',
//        'image' => 'mimes:jpeg,bmp,png|max:2048|image',
    ];


    public static $messages = [
        'name.required' => 'Tiêu đề là trường bắt buộc!',
        'name.max' => 'Tiêu đề tối đa 255!',
        'category_id.required' => 'Danh mục cha là trường bắt buộc!',
        'date_start.required' => 'Ngày đi là trường bắt buộc!',
        'date_end.required' => 'Ngày về là trường bắt buộc!',
//        'image.mimes' => 'Ảnh không đúng định dạng !',
//        'image.max' => 'Ảnh vượt quá tối đa kích thước!',

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function category()
    {
        return $this->belongsTo(\App\Models\Category::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
//    public function country()
//    {
//        return $this->belongsTo(\App\Models\Country::class);
//    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function provider()
    {
        return $this->belongsTo(\App\Models\Provider::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function admins()
    {
        return $this->belongsTo(\App\Models\Admin::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function billDetails()
    {
        return $this->hasMany(\App\Models\BillDetail::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function productImages()
    {
        return $this->hasMany(\App\Models\ProductImage::class);
    }
}
