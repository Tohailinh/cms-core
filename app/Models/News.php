<?php

namespace App\Models;

use App\Repositories\Interfaces\NewRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

/**
 * Class New
 * @package App\Models
 * @version December 3, 2018, 10:53 pm +07
 *
 * @property \App\Models\Category category
 * @property \App\Models\User user
 * @property \Illuminate\Database\Eloquent\Collection billDetail
 * @property string title
 * @property string description
 * @property string image
 * @property string thumb
 * @property string content
 * @property string tags
 * @property string slug
 * @property string title_seo
 * @property string description_seo
 * @property string keyword_seo
 * @property integer category_id
 * @property integer user_id
 * @property boolean status
 */
class News extends Model
{
    use SoftDeletes;

    public $table = 'news';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'title',
        'description',
        'image',
        'thumb',
        'content',
        'tags',
        'slug',
        'title_seo',
        'description_seo',
        'keyword_seo',
        'category_id',
        'user_id',
        'status',
        'type',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'description' => 'string',
        'image' => 'string',
        'thumb' => 'string',
        'content' => 'string',
        'tags' => 'string',
        'slug' => 'string',
        'title_seo' => 'string',
        'description_seo' => 'string',
        'keyword_seo' => 'string',
        'category_id' => 'integer',
        'user_id' => 'integer',
        'status' => 'string',
        'type' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */

    public static $rules = [
        'title' => 'required|max:255',
        'category_id' => 'required',
        'image' => 'mimes:jpeg,bmp,png|max:2048|image',
    ];


    public static $messages = [
        'title.required' => 'Tiêu đề là trường bắt buộc!',
        'title.max' => 'Tiêu đề tối đa 255!',
        'category_id.required' => 'Danh mục cha là trường bắt buộc!',
        'image.mimes' => 'Ảnh không đúng định dạng !',
        'image.max' => 'Ảnh vượt quá tối đa kích thước!',

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function category()
    {
        return $this->belongsTo(\App\Models\Category::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public static function typeNew()
    {
        return [
          NewRepository::TYPE_DEFAUFT => 'Tin tức mặc định',
          NewRepository::TYPE_PROMOTION => 'Tin tức khuyến mại',
        ];
    }

}
