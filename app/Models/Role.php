<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $guarded = [];

    public static $roles = [
        'name' => 'required|unique:roles'
    ];

}
