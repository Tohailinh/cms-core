<?php

namespace App\Repositories\Caches;

use App\Repositories\Eloquents\DbNewRepository;
use App\Repositories\Interfaces\NewRepository;

class CacheNewRepository extends CacheRepository implements NewRepository
{
    function __construct(DbNewRepository $dbRepository)
    {
        $this->dbRepository = $dbRepository;

    }
}
