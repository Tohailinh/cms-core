<?php

namespace App\Repositories\Caches;

use App\Repositories\Eloquents\DbProvinceRepository;
use App\Repositories\Interfaces\ProvinceRepository;

class CacheProvinceRepository extends CacheRepository implements ProvinceRepository
{
    function __construct(DbProvinceRepository $dbProvinceRepository)
    {
        $this->dbRepository = $dbProvinceRepository;
    }

    public function getIdBySlug($slug)
    {
        return $this->dbRepository->getIdBySlug($slug);
    }

    /**
     * @param $slug
     * @return bool
     */
    public function getBySlug($slug)
    {
        return $this->dbRepository->getBySlug($slug);
    }
}