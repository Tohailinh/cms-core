<?php

namespace App\Repositories\Eloquents;


use App\Models\Product;
use App\Repositories\Interfaces\ProductRepository;

class DbProductRepository extends DbRepository implements ProductRepository
{
    function __construct(Product $model)
    {
        $this->model = $model;
    }

    public function listTransport()
    {
        return [
            ProductRepository::TRANSPORT_CAR => 'Ô tô',
            ProductRepository::TRANSPORT_TRAIN => 'Tàu hỏa',
            ProductRepository::TRANSPORT_PLANE => 'Máy bay',
        ];
    }

    public function listType()
    {
        return [
            ProductRepository::TYPE_DEFAULT => 'Tour thường',
            ProductRepository::TYPE_HOT => 'Tour nổi bật',
            ProductRepository::TYPE_FORCUS => 'Tour tiêu điểm',
            ProductRepository::TYPE_DISCOUNT => 'Tour giảm giá',
        ];
    }


}
