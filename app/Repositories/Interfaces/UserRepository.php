<?php

namespace App\Repositories\Interfaces;


interface UserRepository extends BaseRepository
{
    const ACTIVEATED = 1;
    const STATUS_ACTIVE = 1;
    const STATUS_DEACTIVE = 0;

    const CREATED_ON_APP = 0;
    const CREATED_ON_CMS = 1;

    function checkExistWithMobileNumber($mobileNumber);

    function findUserByPhone($phoneNumber);
}
