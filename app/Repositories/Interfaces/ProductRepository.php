<?php

namespace App\Repositories\Interfaces;


interface ProductRepository  extends BaseRepository
{
    const STATUS_ACTIVE = 1;
    const STATUS_DEACTIVE = 0;

    const  TYPE_DEFAULT = 0;
    const  TYPE_HOT = 1;
    const  TYPE_FORCUS = 2;
    const  TYPE_DISCOUNT = 3;

    const TRANSPORT_CAR = 0;
    const TRANSPORT_TRAIN = 1;
    const TRANSPORT_PLANE = 2;


    public function listTransport();
    public function listType();
}
