<?php

namespace App\Http\Controllers\Backend;
use App\Helpers\UploadHelper;
use App\Http\Controllers\AppBaseController;
use App\Models\User;
use App\Repositories\Interfaces\RoleRepository;
use App\Repositories\Interfaces\UserRepository;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Carbon;
use Response, URL, Auth;
use Validator;
use Illuminate\Support\Facades\Hash;

class UserController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;
    protected $roleRepository;

    public function __construct(UserRepository $userRepo, RoleRepository $roleRepository)
    {
        parent::__construct();
        $this->userRepository = $userRepo;
        $this->roleRepository = $roleRepository;
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit', config('system.perPage'));
        //filter
        $filter = [];
        // filter date
        if (!empty($request->input('from_date'))) {
            $date = Carbon::createFromFormat('d/m/Y', $request->input('from_date'))->startOfDay()->toDateTimeString();
            $filter[] = ['created_at', '>=', $date];
        }

        if (!empty($request->input('to_date'))) {
            $date = Carbon::createFromFormat('d/m/Y', $request->input('to_date'))->endOfDay()->toDateTimeString();
            $filter[] = ['created_at', '<=', $date];
        }

        if (!empty($request->input('search_key'))) {
            $searchKey = '%' . $request->input('search_key') . '%';
            $filter[] = [[
                ['username', 'like', $searchKey, 'OR'],
                ['email', 'like', $searchKey, 'OR'],
                ['fullname', 'like', $searchKey, 'OR'],
                ['phone', 'like', $searchKey, 'OR'],
            ]];
        }

        if ($request->input('filter_status') !== null) {
            $filter[] = ['status', (int)$request->input('filter_status')];
        }

        if ($request->input('filter_gender') !== null) {
            $filter[] = ['gender', (int)$request->input('filter_gender')];
        }

        $users = $this->userRepository->paginateList($limit, $filter, [], ['created_at' => 'DESC']);

        return view('backend.users.index', compact('limit', 'users'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('backend.users.create');
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, User::$rules_cr, User::$messages_cr);
        $data = $request->only([ 'fullname', 'email', 'avatar', 'phone', 'gender', 'address', 'password', 'status']);
        if ($request->hasFile('image')) {
            $data['avatar'] = UploadHelper::uploadImgFile($request->file('image'), 'users');
        }

        if (!isset($data['status'])) {
            $data['status'] = UserRepository::STATUS_DEACTIVE;
        }

        $data['password'] = Hash::make($request->get('password'));

        try {
            $this->userRepository->create($data);
            Flash::success('Thêm mới khách hàng thành công!');
            return redirect(route('users.index'));
        } catch (\Exception $ex) {
            Flash::error('Thêm mới khách hàng thất bại!');
            return redirect(route('users.index'));
        }
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function show($id)
    {
        $user = $this->userRepository->findBy('id', (int)$id);
        if (empty($user)) {
            Flash::error('Không tìm thấy khách hàng!');
            return redirect(route('users.index'));
        }
        return view('backend.users.show', compact('user'));
    }


    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id)
    {
        $model = $this->userRepository->findBy('id', (int)$id);
        if (empty($model)) {
            Flash::error('Không tìm thấy khách hàng !');
            return redirect(route('users.index'));
        }
        //check status change password
        return view("backend.users.edit", compact('model'));
    }

    /**
     * @param                          $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, User::$rules_edit, User::$messages_edit);
        $model = $this->userRepository->findBy('id', (int)$id);
        //check id user not exist
        if (empty($model)) {
            Flash::error('Not found user!');
            return redirect(route('users.index'));
        }
        $data = $request->only([ 'fullname', 'email', 'avatar', 'phone', 'gender', 'address', 'password', 'status']);

        if (!isset($data['status'])) {
            $data['status'] = UserRepository::STATUS_DEACTIVE;
        }

        //change password
        if ($request->input('changepassword') == "on") {
            $data['password'] = Hash::make($request->get('password'));
            //add validator
            $this->validate($request, [
                'password' => 'required',
                'confirm_password' => 'required|same:password',
            ]);
        }

        if ($request->hasFile('image')) {
            $data['avatar'] = UploadHelper::uploadImgFile($request->file('image'), 'users');
        }

        try {
            $this->userRepository->update($data, $id);
            Flash::success('Cập nhật khách hàng thành công!');
            return redirect(route('users.index'));
        } catch (\Exception $ex) {
            Flash::error('Cập nhật khách hàng thất bại!!');
            return redirect(route('users.index'));
        }

    }


    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function bulkActions(Request $request)
    {
        $input = $request->all();

        if (empty($input['ids']) || empty($input['key']) || !isset($input['value'])) {
            return $this->responseApp(false, 404, 'Invalid information. Please try again.');
        }

        $key = $input['key'];
        $value = $input['value'];
        $ids = explode(',', $input['ids']);
        $datas = $this->userRepository->findByListId($ids);

        if ($key == 'delete') {
            foreach ($datas as $item) {
                $item->delete();

            }
        } else if ($key == 'status') {
            foreach ($datas as $item) {
                $item->status = $value;
                $item->save();
            }
        } else if ($key == 'activated') {
            foreach ($datas as $item) {
                $item->activated = $value;
                $item->save();
            }
        }

        return $this->responseApp(true, 200, 'successfully.');
    }
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function itemActions(Request $request)
    {
        $input = $request->all();

        if (empty($input['id']) || empty($input['key']) || !isset($input['value'])) {
            return $this->responseApp(false, 404, 'Invalid information. Please try again.');
        }

        $key = $input['key'];
        $value = $input['value'];
        $id = $input['id'];
        $data = $this->userRepository->findById($id);
        if (empty($data)) {
            return $this->responseApp(false, 404, 'This user was not found.');
        }

        if ($key == 'delete') {
            $data->delete();
        } else if ($key == 'status') {
            $data->status = $value;
            $data->save();
        } else if ($key == 'activated') {
            $data->activated = $value;
            $data->save();
        }

        return $this->responseApp(true, 200, 'successfully.');
    }
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getInfoByPhone(Request $request)
    {
        if ($request->input('phone_number')) {
            $user = $this->userRepository->findBy('mobile', $request->input('phone_number'));

            if ($user) {
                return response()->json(['success' => true, 'user' => $user]);
            } else {
                return response()->json(['success' => false, 'message' => trans('app.user_not_found')]);
            }
        } else {
            return response()->json(['success' => false, 'message' => trans('app.missing_phone_number_param')]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function phoneAutoComplete(Request $request)
    {
        if ($request->input('phone_number')) {
            $users = $this->userRepository->findUserByPhone($request->input('phone_number'));
            $users = $users->transform(function($item, $key) {
                return [
                    'data' => $item->mobile,
                    'value' => $item->mobile,
                    'full_name' => $item->fullname
                ];
            })->toArray();
            return response()->json(['success' => true, 'suggestions' => $users]);
        } else {
            return response()->json(['success' => false, 'message' => trans('app.missing_phone_number_param')]);
        }
    }
}
