<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\Role;
use App\Repositories\Interfaces\RoleRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use URL;
class RoleController extends Controller
{
    protected $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit', config('system.perPage'));
        $filter = [];

        if (!empty($request->input('filter_name'))) {
            $filter[] = ['name', 'LIKE', '%' . $request->input('filter_name') . '%'];
        }

        if (!is_null($request->input('active'))) {
            $filter[] = ['active', $request->input('active')];
        }

        $roles = $this->roleRepository->paginateList($limit, $filter, []);

        // Get list permissions
        $permissions = Permission::listPermissionByGroup();

        return view('backend.role.index',  compact('roles', 'countNumber', 'permissions','limit'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        try {
            $validate = Validator::make($request->all(), Role::$roles);

            if ($validate->fails()) {
                return response()->json(['success' => false, 'message' => $validate->errors()->first()]);
            }
            $data = $request->only([ 'type','name', 'description', 'active', 'permission']);
            if (!isset($data['active'])) {
                $data['active'] = RoleRepository::STATUS_DEACTIVE;
            }
            // Update permission
            if (isset($data['permission']) && is_array($data['permission'])) {
                $data['permission'] = implode(',', $data['permission']);
            } else {
                $data['permission'] = null;
            }
            $role = $this->roleRepository->create($data);
            return response()->json(['success' => true, 'message' => '', 'role' => $role]);
        } catch (\Exception $ex) {
            Log::error($ex->getMessage() . '. ' . $ex->getFile() . ':' . $ex->getLine());
            return response()->json(['success' => false, 'message' => $ex->getMessage()]);
        }
    }

    public function getInfo(Request $request)
    {
        $role = $this->roleRepository->findById($request->input('role_id'));

        if ($role) {
            // Get list permissions
            $permissions = Permission::listPermissionByGroup();

            $view = View::make('backend.role.form', ['role' => $role, 'permissions' => $permissions]);
            return response()->json(['success' => true, 'message' => '', 'formHtml' => $view->render()]);
        } else {
            return response()->json(['success' => false, 'message' => 'Không tìm thấy thông tin nhóm quyền tương ứng']);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $roleId = $request->input('id');

        if (!empty($roleId)) {
            $role = $this->roleRepository->findById($roleId);
            if ($role) {
                try {
                    // Update role
                    $updateData = $request->only(['type','name', 'description', 'active', 'permission']);
                    if (!isset($updateData['active'])) {
                        $updateData['active'] = RoleRepository::STATUS_DEACTIVE;
                    }
                    // Update permission
                    if (isset($updateData['permission']) && is_array($updateData['permission'])) {
                        $updateData['permission'] = implode(',', $updateData['permission']);
                    } else {
                        $updateData['permission'] = null;
                    }
                    $this->roleRepository->update($updateData, $roleId);
                    return response()->json(['success' => true, 'message' => 'Cập nhật thông tin nhóm quyền thành công!']);
                } catch (\Exception $ex) {
                    Log::error($ex->getMessage());
                    return response()->json(['success' => false, 'message' => $ex->getMessage()]);
                }
            }
        }

        return response()->json(['success' => false, 'message' => 'Không tìm thấy thông tin nhóm quyền tương ứng']);
    }

    public function destroy(Request $request)
    {
        $roleId = $request->input('id');

        if (!$roleId) {
            return response()->json(['success' => false, 'message' => 'Không tìm thấy thông tin nhóm quyền tương ứng',
                'meta' => [
                    'code' => 1,
                    'message' => 'Không tìm thấy thông tin nhóm quyền tương ứng'
                ]]);
        } else {
            $role = $this->roleRepository->findById($roleId);
            $role->delete();
            return response()->json(['success' => true, 'message' => 'Xóa nhóm quyền thành công!',
                'meta' => [
                    'code' => 200,
                    'message' => 'Xóa nhóm quyền thành công!'
                ]]);
        }
    }
}
