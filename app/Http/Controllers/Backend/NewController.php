<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\UploadHelper;
use App\Facades\Helper;
use App\Http\Controllers\AppBaseController;
use App\Models\News;
use App\Repositories\Interfaces\NewRepository;
use App\Repositories\Interfaces\CategoryRepository;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Response, URL;
use Validator;

class NewController extends AppBaseController
{
    private $newRepository;
    private $categoryRepository;
    public function __construct(
        NewRepository $newRepo,
        CategoryRepository $categoryRepository
    )
    {
        $this->newRepository = $newRepo;
        $this->categoryRepository = $categoryRepository;
        parent::__construct();
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit', config('system.perPage'));
        //filter
        $filter = [];
        if (!empty($request->input('search_key'))) {
            $searchKey = '%' . $request->input('search_key') . '%';
            $filter[] = [[
                ['username', 'like', $searchKey, 'OR'],
                ['email', 'like', $searchKey, 'OR'],
                ['fullname', 'like', $searchKey, 'OR'],
            ]];
        }
        if (!is_null($request->input('filter_status'))) {
            $filter[] = ['status', (int)$request->input('filter_status')];
        }
        $news = $this->newRepository->paginateList($limit, $filter, [], ['id' => 'DESC']);
        return view('backend.news.index', compact('limit', 'news'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $categores = $this->categoryRepository->pluck('title', 'id')->toArray();
        return view('backend.news.create', compact('categores'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, News::$rules, News::$messages);
        $data = $request->only(
            [ 'title','description', 'image', 'content', 'tags', 'slug', 'title_seo', 'description_seo', 'keyword_seo', 'category_id', 'user_id', 'status', 'type'
            ]);
        if (!isset($data['status'])) {
            $data['status'] = NewRepository::STATUS_DEACTIVE;
        }

        if (!isset($data['type'])) {
            $data['type'] = NewRepository::TYPE_DEFAUFT;
        }
        $data['slug'] = Str::slug($data['title'], '-');
        $data['title_seo'] = Str::slug($data['title'], '-');
        $data['user_id'] = Auth::id();
        //Update for image
        if ($request->hasFile('image')) {
            $data['image'] = UploadHelper::uploadImgFile($request->file('image'), 'news');
        }

        try {
            $this->newRepository->create($data);
            Flash::success('Taọ mới tin tức thành công! ');
            return redirect(route('news.index'));
        } catch (\Exception $ex) {
            Flash::error('Tạo mới tin tức thất bại!');
            return redirect(route('news.create'));
        }
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function show($id)
    {
        $new = $this->newRepository->findById((int)$id);
        if (empty($new)) {
            Flash::error('Người quản trị này không tồn tại!!');
            return redirect(route('backend.news.index'));
        }
        return view('backend.news.show', compact(  'new'));
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id)
    {
        $categores = $this->categoryRepository->pluck('title', 'id')->toArray();
        $model = $this->newRepository->findById((int)$id);
        if (empty($model)) {
            Flash::error('Tin tức này không tồn tại!!');
            return redirect(route('news.index'));
        }
        return view('backend.news.edit', compact('model', 'categores'));
    }

    /**
     * @param                          $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, News::$rules, News::$messages);
        $model = $this->newRepository->findById((int)$id);
        //check id new not exist
        if (empty($model)) {
            Flash::error('Tin tức này không tồn tại!!');
            return redirect(route('news.index'));
        }
        $data = $request->only(['title', 'description', 'image', 'content', 'tags', 'slug', 'title_seo', 'description_seo', 'keyword_seo', 'category_id', 'user_id', 'status']);
        if (!isset($data['status'])) {
            $data['status'] = NewRepository::STATUS_DEACTIVE;
        }
        if (!empty($request->input('title'))) {
            $data['slug'] = Str::slug($data['title'], '-');
            $data['title_seo'] = Str::slug($data['title'], '-');
        }

        $data['user_id'] = Auth::id();
        //Update for image
        if ($request->hasFile('image')) {
            $data['image'] = UploadHelper::uploadImgFile($request->file('image'), 'news');
        }

        try {
            $this->newRepository->update($data, $id);
            Flash::success('Cập nhật người quản trị thành công!');
            return redirect(route('news.index'));
        } catch (\Exception $ex) {
            Flash::error('Cập nhật người quản trị thất bại!');
            return redirect(route('news.index'));
        }
    }

    //action all
    public function bulkActions(Request $request)
    {
        $input = $request->all();

        if (empty($input['ids']) || empty($input['key']) || !isset($input['value'])) {
            return $this->responseApp(false, 404, 'Thông tin không hợp lệ. Vui lòng thử lại!');
        }

        $key = $input['key'];
        $value = $input['value'];
        $ids = explode(',', $input['ids']);
        $datas = $this->newRepository->findByListId($ids);

        if ($datas->isEmpty()) {
            return $this->responseApp(false, 1, 'Người dùng không tồn tại!');
        }
        if ($key == 'delete') {
            foreach ($datas as $item) {
                if ($item->id == Auth::id()) {
                    return $this->responseApp(false, 1, 'Bạn không thể thao tác với chính mình!');
                } else {
                    $item->delete();
                }
            }
        } else if ($key == 'status') {
            foreach ($datas as $item) {
                $item->status = $value;
                $item->save();
            }
        }

        return $this->responseApp(true, 200, 'successfully.');
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function itemActions(Request $request)
    {
        $input = $request->all();

        if (empty($input['id']) || empty($input['key']) || !isset($input['value'])) {
            return $this->responseApp(false, 404, 'Thông tin không hợp lệ. Vui lòng thử lại!');
        }

        $key = $input['key'];
        $value = $input['value'];
        $id = $input['id'];

        try {
            $data = $this->newRepository->findById($id);
        } catch (\Exception $ex) {
            return $this->responseApp(false, 404, 'Người dùng không tồn tại!');
        }

        if ($key == 'delete') {
            $data->delete();
        } else if ($key == 'status') {
            $data->status = $value;
            $data->save();
        }

        return $this->responseApp(true, 200, 'Thành công!');
    }
}
