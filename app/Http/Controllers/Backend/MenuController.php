<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\UploadHelper;
use App\Facades\Helper;
use App\Http\Controllers\AppBaseController;
use App\Models\Menu;
use App\Repositories\Interfaces\MenuRepository;
use App\Repositories\Interfaces\CategoryRepository;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Response, URL;
use Validator;

class MenuController extends AppBaseController
{
    private $menuRepository;
    private $categoryRepository;

    public function __construct(
        MenuRepository $menuRepo,
        CategoryRepository $categoryRepository
    )
    {
        $this->menuRepository = $menuRepo;
        $this->categoryRepository = $categoryRepository;
        parent::__construct();
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {

        $limit = $request->input('limit', config('system.perPage'));
        //filter
        $filter = [];
        if (!empty($request->input('search_key'))) {
            $searchKey = '%' . $request->input('search_key') . '%';
            $filter[] = [[
                ['name', 'like', $searchKey, 'OR'],
            ]];
        }

        if (!is_null($request->input('filter_status'))) {
            $filter[] = ['status', (int)$request->input('filter_status')];
        }


        $menus = $this->menuRepository->paginateList($limit, $filter, [], ['location' => 'ASC']);
        return view('backend.menus.index', compact('limit', 'menus'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $levels = $this->menuRepository->all(['*'])->toArray();
        return view('backend.menus.create', compact('levels'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, Menu::$rules, Menu::$messages);
        $data = $request->only([ 'name','location', 'level', 'parent', 'route', 'status', 'order']);
        if (!isset($data['status'])) {
            $data['status'] = MenuRepository::STATUS_DEACTIVE;
        }
        if (empty($data['parent'])){
            $data['parent'] = 0;
        }
        try {
            $this->menuRepository->create($data);
            Flash::success('Taọ mới menu thành công! ');
            return redirect(route('menus.index'));
        } catch (\Exception $ex) {
            Flash::error('Tạo mới menu thất bại!');
            return redirect(route('menus.create'));
        }
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function show($id)
    {
        $menu = $this->menuRepository->findById((int)$id);
        if (empty($menu)) {
            Flash::error('Menu này không tồn tại!!');
            return redirect(route('backend.menus.index'));
        }
        return view('backend.menus.show', compact(  'menu'));
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id)
    {
        $categores = $this->categoryRepository->pluck('title', 'id')->toArray();
        $levels = $this->menuRepository->all(['*'])->toArray();
        $model = $this->menuRepository->findById((int)$id);
        if (empty($model)) {
            Flash::error('Menu này không tồn tại!!');
            return redirect(route('menus.index'));
        }
        return view('backend.menus.edit', compact('model', 'categores', 'levels'));
    }

    /**
     * @param                          $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, Menu::$rules_edit, Menu::$messages_edit);
        $model = $this->menuRepository->findById((int)$id);
        //check id menu not exist
        if (empty($model)) {
            Flash::error('Menu này không tồn tại!!');
            return redirect(route('menus.index'));
        }
        $data = $request->only([ 'name','location', 'level', 'parent', 'route', 'status', 'order']);
        if (!isset($data['status'])) {
            $data['status'] = MenuRepository::STATUS_DEACTIVE;
        }
        if (empty($data['parent'])){
            $data['parent'] = 0;
        }
        try {
            $this->menuRepository->update($data, $id);
            Flash::success('Cập nhật menu thành công!');
            return redirect(route('menus.index'));
        } catch (\Exception $ex) {
            Flash::error('Cập nhật menu thất bại!');
            return redirect(route('menus.index'));
        }
    }

    //action all
    public function bulkActions(Request $request)
    {
        $input = $request->all();

        if (empty($input['ids']) || empty($input['key']) || !isset($input['value'])) {
            return $this->responseApp(false, 404, 'Thông tin không hợp lệ. Vui lòng thử lại!');
        }

        $key = $input['key'];
        $value = $input['value'];
        $ids = explode(',', $input['ids']);
        $datas = $this->menuRepository->findByListId($ids);

        if ($datas->isEmpty()) {
            return $this->responseApp(false, 1, 'menu không tồn tại!');
        }
        if ($key == 'delete') {
            foreach ($datas as $item) {
                $item->delete();
            }
        } else if ($key == 'status') {
            foreach ($datas as $item) {
                $item->status = $value;
                $item->save();
            }
        }

        return $this->responseApp(true, 200, 'Thành công!');
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function itemActions(Request $request)
    {
        $input = $request->all();

        if (empty($input['id']) || empty($input['key']) || !isset($input['value'])) {
            return $this->responseApp(false, 404, 'Thông tin không hợp lệ. Vui lòng thử lại!');
        }

        $key = $input['key'];
        $value = $input['value'];
        $id = $input['id'];

        try {
            $data = $this->menuRepository->findById($id);
        } catch (\Exception $ex) {
            return $this->responseApp(false, 404, 'Người dùng không tồn tại!');
        }

        if ($key == 'delete') {
            $data->delete();
        } else if ($key == 'status') {
            $data->status = $value;
            $data->save();
        }

        return $this->responseApp(true, 200, 'Thành công!');
    }
}
