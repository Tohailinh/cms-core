<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\UploadHelper;
use App\Facades\Helper;
use App\Http\Controllers\AppBaseController;
use App\Models\Admin;
use App\Models\Role;
use App\Repositories\Interfaces\AdminRepository;
use App\Repositories\Interfaces\RoleRepository;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Hash;
use Response, URL, Auth;
use Validator;

class AdminController extends AppBaseController
{

    /** @var  AdminRepository */
    private $adminRepository;
    protected $roleRepository;

    /**
     * AdminController constructor.
     * @param AdminRepository $adminRepo
     */
    public function __construct(
        AdminRepository $adminRepo,
        RoleRepository $roleRepository
    )
    {
        $this->adminRepository = $adminRepo;
        $this->roleRepository = $roleRepository;
        parent::__construct();
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit', config('system.perPage'));
        $roleModel = new Role();
        //filter
        $filter = [];
        if (!empty($request->input('search_key'))) {
            $searchKey = '%' . $request->input('search_key') . '%';
            $filter[] = [[
                ['username', 'like', $searchKey, 'OR'],
                ['email', 'like', $searchKey, 'OR'],
                ['fullname', 'like', $searchKey, 'OR'],
            ]];
        }
        if (!is_null($request->input('filter_status'))) {
            $filter[] = ['status', (int)$request->input('filter_status')];
        }
        $admins = $this->adminRepository->paginateList($limit, $filter, [], ['id' => 'DESC']);
        return view('backend.admins.index', compact('limit', 'admins', 'roleModel'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $roles = $this->roleRepository->all(['id', 'name'],[['active', '=', 1]]);
        return view('backend.admins.create', compact(  'roles'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, Admin::$rules_cr, Admin::$messages_cr);
        $role = implode(',',$request->input('roles'));
        $data = $request->only([ 'avatar','username', 'fullname', 'roles', 'email', 'password', 'status']);

        if (!isset($data['status'])) {
            $data['status'] = AdminRepository::STATUS_DEACTIVE;
        }
        $data['password'] = Hash::make($request->get('password'));
        $data['roles'] = $role;
        //Update for image
        if ($request->hasFile('image')) {
            $data['avatar'] = UploadHelper::uploadImgFile($request->file('image'), 'admins');
        }
        try {
            $this->adminRepository->create($data);
            Flash::success('Taọ mới người dùng thành công! ');
            return redirect(route('admins.index'));
        } catch (\Exception $ex) {
            Flash::error('Tạo mới người dùng thất bại!');
            return redirect(route('admins.create'));
        }
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function show($id)
    {
        $admin = $this->adminRepository->findBy('id', (int)$id);
        if (empty($admin)) {
            Flash::error('Người quản trị này không tồn tại!!');
            return redirect(route('backend.admins.index'));
        }
        return view('backend.admins.show', compact(  'admin'));
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id)
    {
        $model = $this->adminRepository->findBy('id', (int)$id);
        if (empty($model)) {
            Flash::error('Người quản trị này không tồn tại!!');
            return redirect(route('admins.index'));
        }
        $model_roles = explode(",", $model->roles);
        $roles = $this->roleRepository->all(['id', 'name'],[['active', '=', 1]]);

        //check user login and not fix yourself
        if ($model->id == Auth::id()) {
            Flash::error('Bạn không thể cập nhật chính mình !!');
            return redirect(route('admins.index'));
        }
        return view('backend.admins.edit', compact('model', 'roles', 'model_roles'));
    }

    /**
     * @param                          $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, Admin::$rules_edit, Admin::$messages_edit);
        $input = $request->all();
        $model = $this->adminRepository->findBy('id', (int)$id);
        $role = implode(',',$input['roles']);

        //check id admin not exist
        if (empty($model)) {
            Flash::error('Người quản trị này không tồn tại!!');
            return redirect(route('admins.index'));
        }
        $data = $request->only([ 'avatar','username', 'fullname', 'roles', 'email', 'password', 'status']);

        if (!isset($data['status'])) {
            $data['status'] = AdminRepository::STATUS_DEACTIVE;
        }
        //change password
        if ($request->changepassword == "on") {
            $data['password'] = Hash::make($request->get('password'));
            //add validator
            $this->validate($request, [
                'password' => 'required',
                'confirm_password' => 'required|same:password',
            ]);
        }
//       Update for image
        if ($request->hasFile('image')) {
            $data['avatar'] = UploadHelper::uploadImgFile($request->file('image'), 'admins');
        }
        $data['roles'] = $role;
        try {
            $model->update($data);
            Flash::success('Cập nhật người quản trị thành công!');
            return redirect(route('admins.index'));
        } catch (\Exception $ex) {
            Flash::error('Cập nhật người quản trị thất bại!');
            return redirect(route('admins.index'));
        }
    }

    //action all
    public function bulkActions(Request $request)
    {
        $input = $request->all();

        if (empty($input['ids']) || empty($input['key']) || !isset($input['value'])) {
            return $this->responseApp(false, 404, 'Thông tin không hợp lệ. Vui lòng thử lại!');
        }

        $key = $input['key'];
        $value = $input['value'];
        $ids = explode(',', $input['ids']);
        $datas = $this->adminRepository->findByListId($ids);

        if ($datas->isEmpty()) {
            return $this->responseApp(false, 1, 'Người dùng không tồn tại!');
        }
        if ($key == 'delete') {
            foreach ($datas as $item) {
                if ($item->id == Auth::id()) {
                    return $this->responseApp(false, 1, 'Bạn không thể thao tác với chính mình!');
                } else {
                    $item->delete();
                }
            }
        } else if ($key == 'status') {
            foreach ($datas as $item) {
                $item->status = $value;
                $item->save();
            }
        }

        return $this->responseApp(true, 200, 'successfully.');
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function itemActions(Request $request)
    {
        $input = $request->all();

        if (empty($input['id']) || empty($input['key']) || !isset($input['value'])) {
            return $this->responseApp(false, 404, 'Thông tin không hợp lệ. Vui lòng thử lại!');
        }

        $key = $input['key'];
        $value = $input['value'];
        $id = $input['id'];

        try {
            $data = $this->adminRepository->findById($id);
        } catch (\Exception $ex) {
            return $this->responseApp(false, 404, 'Người dùng không tồn tại!');
        }

        if ($data->id == Auth::id()) {
            return $this->responseApp(false, 404, 'Bạn không thể xóa chính mình');
        }

        if ($key == 'delete') {
            $data->delete();
        } else if ($key == 'status') {
            $data->status = $value;
            $data->save();
        }

        return $this->responseApp(true, 200, 'Thành công!');
    }

    public function changePassword($id)
    {
        $admin = $this->adminRepository->findBy('id', (int)$id);
        return view('backend.admins.changepassword', compact('admin'));
    }

    public function postChangePassword($id, Request $request)
    {
        $input = $request->all();
        $this->validate($request, Admin::$rules_change, Admin::$messages_change);
        $current_password = Auth::User()->password;
        if (Hash::check($input['current_password'], $current_password)) {
            $admin_id = Auth::User()->id;
            $admin = $this->adminRepository->findBy('id', (int)$admin_id);
            $admin->password = Hash::make($input['password']);
            $admin->save();
            Flash::success('Cập nhật mật khẩu thành công!');
            Auth::logout();

            return redirect(route('admins.index'));
        } else {

            Flash::error('Mật khẩu hiện tại không chính xác!');
            return redirect(route('admins.changepassword', $id));
        }
    }

}
