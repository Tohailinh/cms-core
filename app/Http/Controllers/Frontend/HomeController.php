<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;

class HomeController extends AppBaseController
{
    public function __construct(
//        RoleRepository $roleRepository
    )
    {
//        $this->adminRepository = $adminRepo;
//        $this->roleRepository = $roleRepository;
        parent::__construct();
    }

    public function index()
    {
        return view('frontend.index.home', compact(''));
    }

    public function new()
    {
        return view('frontend.new.index', compact(''));
    }

    public function newInfo($slug)
    {
        return view('frontend.new.new_info', compact(''));
    }

    public function tour()
    {
        return view('frontend.tour.index', compact(''));
    }

    public function tourInfo($slug)
    {
        return view('frontend.tour.tour_info', compact(''));
    }

    public function promotion()
    {
        return view('frontend.new.promotion', compact(''));
    }

    public function promotionInfo($slug)
    {
        return view('frontend.new.promotion_info', compact(''));
    }

    public function lienhe()
    {
        return view('frontend.lienhe.index', compact(''));
    }

    public function visa()
    {
        return view('frontend.visa.index', compact(''));
    }
}
