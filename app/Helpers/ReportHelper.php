<?php

namespace App\Helpers;


use App\Repositories\Interfaces\SettingRepository;
use Illuminate\Support\Carbon;

class ReportHelper
{

    /**
     * Get report time from date time filter
     *
     * @param $fromDate
     * @param $toDate
     * @param string $date_format
     * @return array
     */
    public static function getReportTime($fromDate, $toDate, $date_format = 'Y-m-d')
    {
        $settingRepository = app(SettingRepository::class);

        // Check report time setting
        $time_setting = $settingRepository->getSettingValue('order_date_statistic_time');
        if (empty($time_setting)) {
            $time_setting = '00:00';
        }

        // Check date format
        switch ($date_format) {
            case 'Y-m-d':
                $fromDate = Carbon::parse($fromDate);
                $toDate = Carbon::parse($toDate);
                break;
            default:
                $fromDate = Carbon::createFromFormat($date_format, $fromDate);
                $toDate = Carbon::createFromFormat($date_format, $toDate);
                break;
        }

        if ($time_setting !== '00:00') {
            return [
                'from_time' => $fromDate->setTimeFromTimeString($time_setting)->startOfMinute()->toDateTimeString(),
                'to_time' => $toDate->addDay()->setTimeFromTimeString($time_setting)->startOfMinute()->subSecond()->toDateTimeString()
            ];
        } else {
            return [
                'from_time' => Carbon::parse($fromDate)->startOfDay()->toDateTimeString(),
                'to_time' => Carbon::parse($toDate)->endOfDay()->toDateTimeString()
            ];
        }
    }
}
