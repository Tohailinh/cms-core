<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | File path
    |--------------------------------------------------------------------------
    |
    | Intervention Image supports "GD Library" and "Imagick" to process images
    | internally. You may choose one of them according to your PHP
    | configuration. By default PHP's "GD Library" implementation is used.
    |
    | Supported: "gd", "imagick"
    |
    */

    'no_image' => '/uploads/default/no-image.jpg',

    'user_images' => public_path('uploads/users'),
    'user_image_show' => '/uploads/users/',


    'agent_images' => public_path('uploads/agents'),
    'agent_image_show' => '/uploads/agents/',

    'bank_images' => public_path('uploads/banks'),
    'bank_image_show' => '/uploads/banks/',

    'company_images' => public_path('uploads/company'),
    'company_image_show' => '/uploads/company/',

    'product_images' => public_path('uploads/product_images'),
    'product_image_show' => '/uploads/product_images/',
);