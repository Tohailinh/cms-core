process.env.DISABLE_NOTIFIER = true;
var elixir = require('laravel-elixir');
var gulp = require('gulp');
var concat = require('gulp-concat');
var spritesmith = require('gulp.spritesmith');

require('laravel-elixir-livereload');


elixir(function(mix) {

    //css cho dashboard
    mix.sass([
        './resources/assets/sass/dashboard.scss'
    ], './public/css/dashboard.css');

    //js cho backend
    mix.scripts([
        './resources/assets/js/myjs.js'
    ], './public/js/myjs.js');

    //==== livereload ====
    mix.livereload([
        'resources/assets/**/*',
        'resources/assets/**/**/*.scss',
        'resources/views/**/*'
    ]);



});

// // Watch
// gulp.task('watch', function() {
//     gulp.watch('./resources/assets/sass/*.scss', ['default']);
//     gulp.watch('./resources/assets/js/*.js', ['default']);
// });
