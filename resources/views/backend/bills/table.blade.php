<table class="table table-responsive" id="bills-table">
    <thead>
        <tr>
            <th>Total</th>
        <th>Date Order</th>
        <th>Payment</th>
        <th>Note</th>
        <th>Size</th>
        <th>User Id</th>
        <th>Status</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($bills as $bill)
        <tr>
            <td>{!! $bill->total !!}</td>
            <td>{!! $bill->date_order !!}</td>
            <td>{!! $bill->payment !!}</td>
            <td>{!! $bill->note !!}</td>
            <td>{!! $bill->size !!}</td>
            <td>{!! $bill->user_id !!}</td>
            <td>{!! $bill->status !!}</td>
            <td>
                {!! Form::open(['route' => ['bills.destroy', $bill->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('bills.show', [$bill->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('bills.edit', [$bill->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>