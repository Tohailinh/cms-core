@extends('backend.layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Cập nhật câu hỏi
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::open(['route' => ['faq.update', $model->id], 'class' => 'form jsvalidation', 'novalidate' => 'novalidate', 'files' => true , 'enctype' => 'multipart/form-data', 'id' => 'formid']) !!}

                   @include('backend.faq.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection
