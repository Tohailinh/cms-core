

{!! plugin(
[
'jquery','popper','bootstrap','adminlte','icheck','select2','datatables.net',
'responsive','datatables.net-bs','moment','daterangepicker','bootstrap-datepicker',
'sweetalert','jquery-loading','waitMe','toastr','ckeditor','bootstrap-tagsinput'
]
, 'js') !!}
{!! js(['myjs.js', 'fileinput.min.js', 'theme.js']) !!}

<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.1/themes/fas/theme.min.js"></script>--}}
