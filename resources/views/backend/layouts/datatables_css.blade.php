

{!! plugin(
['font-awesome', 'bootstrap', 'ionicons','adminlte',
    '_all-skins','select2','alt','datatables.net-bs','datatables.net',
    'responsive','bootstrap-datepicker','sweetalert','jquery-loading',
    'daterangepicker','waitMe','toastr','bootstrap-tagsinput'
]
, 'css') !!}

{!! css(['mycss.css', 'fileinput.min.css', 'theme.min.css']) !!}
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
{{--<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.1/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />--}}
