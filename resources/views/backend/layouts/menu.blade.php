@php
    $menus = [
        [
            'title' => 'Người quản trị',
            'route' => 'admins.index',
            'route_params' => [],
            'check_active_prefix' => 'admins*',
            'icon_class' => 'fa fa-users',
            'sub_menus' => []
        ],

        [
            'title' => 'Quản lý nhóm quyền',
            'route' => 'role.index',
            'route_params' => [],
            'check_active_prefix' => 'role*',
            'icon_class' => 'fa fa-lock',
            'sub_menus' => []
        ],

        [
            'title' => 'Quản lý Slide',
            'route' => 'slides.index',
            'route_params' => [],
            'check_active_prefix' => 'slides*',
            'icon_class' => 'fa fa-user-plus',
            'sub_menus' => []
        ],



        [
            'title' => 'Quản lý khách hàng',
            'route' => 'users.index',
            'route_params' => [],
            'check_active_prefix' => 'users*',
            'icon_class' => 'fa fa-user-plus',
            'sub_menus' => []
        ],

        [
            'title' => 'Tin tức',
            'route' => 'news.index',
            'route_params' => [],
            'check_active_prefix' => 'news*',
            'icon_class' => 'fa fa-user-plus',
            'sub_menus' => []
        ],

         [
            'title' => 'Sản phẩm',
            'route' => 'products.index',
            'route_params' => [],
            'check_active_prefix' => 'products*',
            'icon_class' => 'fa fa-user-plus',
            'sub_menus' => []
        ],

        [
            'title' => 'Danh mục',
            'route' => 'categories.index',
            'route_params' => [],
            'check_active_prefix' => 'categories*',
            'icon_class' => 'fa fa-user-plus',
            'sub_menus' => []
        ],

        [
            'title' => 'Menu',
            'route' => 'menus.index',
            'route_params' => [],
            'check_active_prefix' => 'menus*',
            'icon_class' => 'fa fa-user-plus',
            'sub_menus' => []
        ],


        [
            'title' => 'FAQ',
            'route' => 'faq.index',
            'route_params' => [],
            'check_active_prefix' => 'faq*',
            'icon_class' => 'fa fa-user-plus',
            'sub_menus' => []
        ],

        /*[
            'title' => 'Thể loại',
            'route' => 'categories.index',
            'route_params' => [],
            'check_active_prefix' => 'categories*',
            'icon_class' => 'fa fa-user-plus',
            'sub_menus' => []
        ],

        [
            'title' => 'Sản phẩm',
            'route' => 'products.index',
            'route_params' => [],
            'check_active_prefix' => 'products*',
            'icon_class' => 'fa fa-user-plus',
            'sub_menus' => []
        ],

        [
            'title' => 'Ảnh sản phẩm',
            'route' => 'productImages.index',
            'route_params' => [],
            'check_active_prefix' => 'productImages*',
            'icon_class' => 'fa fa-user-plus',
            'sub_menus' => []
        ],
        [
            'title' => 'Tin tức',
            'route' => 'news.index',
            'route_params' => [],
            'check_active_prefix' => 'news*',
            'icon_class' => 'fa fa-user-plus',
            'sub_menus' => []
        ],

        [
            'title' => 'Contacts',
            'route' => 'contacts.index',
            'route_params' => [],
            'check_active_prefix' => 'contacts*',
            'icon_class' => 'fa fa-user-plus',
            'sub_menus' => []
        ],

        [
            'title' => 'Tuyển dụng',
            'route' => 'recruitments.index',
            'route_params' => [],
            'check_active_prefix' => 'recruitments*',
            'icon_class' => 'fa fa-user-plus',
            'sub_menus' => []
        ],*/

      ];
@endphp

{{--<li class="{{ Request::is('providers*') ? 'active' : '' }}">--}}
{{--<a href="{!! route('providers.index') !!}"><i class="fa fa-edit"></i><span>Providers</span></a>--}}
{{--</li>--}}

{{--<li class="{{ Request::is('billDetails*') ? 'active' : '' }}">--}}
{{--<a href="{!! route('billDetails.index') !!}"><i class="fa fa-edit"></i><span>Bill Details</span></a>--}}
{{--</li>--}}

{{--<li class="{{ Request::is('bills*') ? 'active' : '' }}">--}}
{{--<a href="{!! route('bills.index') !!}"><i class="fa fa-edit"></i><span>Bills</span></a>--}}

@if(!empty($menus))
    @foreach($menus as $menu)
        <li class="@if(!empty($menu['sub_menus'])) treeview @endif {{ Request::is($menu['check_active_prefix']) ? 'active' : '' }}">
            <a href="{{ !empty($menu['route']) ? route($menu['route']) : 'javascript:' }}">
                @if(!empty($menu['icon_class']))<i class="{{ $menu['icon_class'] }}"></i>@endif<span>{{ $menu['title'] }} </span>
                @if(!empty($menu['sub_menus']))
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                @endif
            </a>
            @if(!empty($menu['sub_menus']))
                <ul class="treeview-menu">
                    @foreach($menu['sub_menus'] as $subMenu)
                        <li>
                            <a href="{{ !empty($subMenu['route']) ? route($subMenu['route']) : 'javascript:' }}">@if(!empty($subMenu['icon_class']))
                                    <i class="{{ $subMenu['icon_class'] }}"></i>@endif{{ $subMenu['title'] }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            @endif
        </li>
    @endforeach
@endif

