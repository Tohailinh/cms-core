
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>@yield('title') GUTINA</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    @include('backend.layouts.datatables_css')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    @yield('css')
</head>

<body class="skin-blue sidebar-mini">
@if (!Auth::guest())
    <div class="wrapper">
        <!-- Main Header -->
        <header class="main-header">

            <!-- Logo -->
            <a href="/" class="logo">
                <img src="{!! URL::to('img/gutina.png') !!}" class="img-responsive" style="width: 80%; padding: 10px"/>
            </a>

            <!-- Header Navbar -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li class="dropdown tasks-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-flag-o"></i>
                                <span class="label label-danger counter-processing-order"></span>
                            </a>
                        </li>
                        <!-- User Account Menu -->
                        <li class="dropdown user user-menu">
                            <!-- Menu Toggle Button -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <!-- The user image in the navbar-->
                                <img src="{!! URL::to('img/gutina.png') !!}"
                                     class="user-image" alt="User Image"/>
                                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                <span class="hidden-xs">{!! Auth::user()->name !!}</span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- The user image in the menu -->
                                <li class="user-header">
                                    <img src="/img/logo_2.png"
                                         class="img-circle" alt="User Image"/>
                                    <p>
                                        {!! Auth::user()->name !!}
                                        {{--<small>Member since {!! Auth::user()->created_at->format('M. Y') !!}</small>--}}
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="{!! route('admins.changepassword', [Auth::id()])!!}" class="btn btn-default btn-flat">Đổi mật khẩu </a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="{{ url('/logout') }}" class="btn btn-default btn-flat"
                                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            Sign out
                                        </a>
                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

        <!-- Left side column. contains the logo and sidebar -->
        @include('backend.layouts.sidebar')
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <div class="col-sm-12">
                <div class="breadcrumb bg-white">
                    <li><a href="{{ URL::to('/home/') }}"><i class="fa fa-home"></i>Trang điều khiển</a></li>
                    @yield('breadcrumb')
                </div>
            </div>
            <div class="clearfix"></div>
            @yield('content')
        </div>

        {{--<!-- Main Footer -->--}}
        {{--<footer class="main-footer" style="max-height: 100px;text-align: center">--}}
        {{--</footer>--}}

    </div>
@endif
@if(Auth::guest())
    <script type="text/javascript">
        window.location = "{{ route('auth.login') }}";//here double curly bracket
    </script>
@endif

@include('backend.layouts.datatables_js')
@yield('scripts')
</body>
</html>
