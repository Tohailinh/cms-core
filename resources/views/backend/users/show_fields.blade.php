<div class=" col-md-12 col-sm-12 col-xs-12">
    <h3 class="profile-username text-center">  {!! $user->fullname !!}</h3>
    <div id="crop-avatar" class="text-center"
         style="background: url('{!! (!empty($user->avatar)) ? $user->avatar : config('filepath.no_image') !!}') no-repeat center center; background-size: cover;">
    </div>
</div>

<!-- Profile Image -->
<div class=" col-md-6 col-sm-6 col-xs-12">
    <div class="box-body box-profile">

        <ul class="list-group list-group-unbordered">
            <li class="list-group-item">
                <b>Email : </b> <a class="pull-right">{!! isset($user->email) ? $user->email : '...' !!}</a>
            </li>
            <li class="list-group-item">
                <b>Điện thoại : </b> <a class="pull-right">{!! isset($user->phone) ? $user->phone : '...' !!}</a>
            </li>

            <li class="list-group-item">
                <b>Địa chỉ : </b> <a class="pull-right">{!! isset($user->address) ? $user->address : '...' !!}</a>
            </li>

            {{--<li class="list-group-item">--}}
                {{--<b>Ngày sinh : </b> <a class="pull-right">@if(!empty($user) && $user->birthdate){!! Helper::formatDate($user->birthdate) !!}@endif</a>--}}
            {{--</li>--}}

            <li class="list-group-item">
                <b>Giới tính : </b> <a class="pull-right">{!! \App\Models\User::genders($user->gender) !!}</a>
            </li>

        </ul>
    </div>

</div>

<!-- Profile Image -->
<div class=" col-md-6 col-sm-6 col-xs-12">
    <div class="box-body box-profile">
        <ul class="list-group list-group-unbordered">

            <li class="list-group-item">
                <b>Tạo lúc : </b> <a class="pull-right">{!! isset($user->created_at) ? $user->created_at : '...' !!}</a>
            </li>

            <li class="list-group-item">
                <b>Cập nhật lúc : </b> <a class="pull-right">{!! isset($user->updated_at) ? $user->updated_at : '...' !!}</a>
            </li>

            <li class="list-group-item">
                <b>Trạng thái : </b>
                @if ($user->status == 1)
                    <a class='pull-right btn  btn-xs btn btn-success btn-edit item_actions btn-mg-2'>Kích hoạt</a>
                @else
                    <a class='pull-right btn btn-xs btn-danger  btn-edit item_actions btn-mg-2'>Vô hiệu</a>
                @endif
            </li>

        </ul>
    </div>

</div>
<!-- /.box-body -->
