@extends('backend.layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Chi tiết tin tức
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                @include('backend.news.show_fields')
                <!-- Submit Field -->
                    <div class="form-group col-sm-12 col-xs-12 row text-center">
                        <a href="{!! route('news.index') !!}" class="btn btn-default">Quay lại </a>
                    </div>

                    <div class="clearfix"></div>

                </div>
            </div>
        </div>
    </div>
@endsection
