<div class="col-md-6 col-sm-6 col-xs-12">
    <!-- Fullname Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('fullname', 'Tên đầy đủ :') !!}
        {!! Form::text('fullname', (!empty($model)) ? $model->fullname : old('fullname'), ['placeholder'=>'Tên đầy đủ', 'class' => 'form-control']) !!}
    </div>

    <!-- Fullname Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('roles', 'Quyền :') !!}
        <span class="text-danger">(*)</span>
        <select name="roles[]" class="form-control roles" multiple="multiple" id="roles">
            @foreach($roles as $k => $role)
                <option value="{{ $role->id }}"
                        @if (!empty($model_roles)) @foreach($model_roles as $k1=> $value) @if($role->id == $value)selected="selected"@endif @endforeach @endif>{{ $role->name }}</option>
            @endforeach
        </select>
    </div>

@if(\Route::current()->getName() === 'admins.edit')
    <!-- Password Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('password', 'Mật khẩu mới:') !!}
            <input type="checkbox" name="changepassword" id="changepassword" title="">

            <input class="form-control password" name="password" placeholder="Mật khẩu mới"
                   id="up_admin_password" type="password" disabled="disabled">
        </div>

        <!-- Password Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('password', 'Xác nhận mật khẩu mới:') !!}
            <input class="form-control password" name="confirm_password" placeholder="Xác nhận mật khẩu mới"
                   id="confirm_password" type="password" disabled="disabled">
        </div>
@else
    <!-- Password Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('password', 'Password:') !!}
            {!! Form::password('password', ['class' => 'form-control', 'placeholder'=>'password']) !!}
        </div>

@endif

<!-- Username Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('username', 'Tài khoản :') !!}
        <span class="text-danger">(*)</span>
        {!! Form::text('username', (!empty($model)) ? $model->username : old('username'), ['placeholder'=>'Tài khoản', 'class' => 'form-control', !empty($model->username)? 'readonly' : '']) !!}
    </div>

    <!-- Email Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('email', 'Email:') !!}
        <span class="text-danger">(*)</span>
        {!! Form::text('email', (!empty($model)) ? $model->email : old('email'), ['placeholder'=>'Email', 'class' => 'form-control','id' => 'email-user', !empty($model->email)? 'readonly' : '' ]) !!}

    </div>

</div>


<div class="col-md-6 col-sm-6 col-xs-12">
    <div class="form-group col-md-12 col-sm-12 col-xs-12 text-center">
        <img id="img-upload" class="img-fluid center-block" width="200px" height="200px"/>
        @if(isset($model))
            <div id="img-upload-remove">
                @if (isset($model->avatar))
                    <img src="{{$model->avatar }}" class="  img-fluid img-thumbnail center-block "
                         width="200px" height="200px">
                    <input type="hidden" name="image_tmp" value="{{$model->avatar}}"/>
                @else
                    <img src="{{ config('filepath.no_image') }}" class="  img-fluid img-thumbnail center-block "
                         width="200px" height="200px">

                @endif
            </div>
        @endif
    </div>

    <div class="form-group col-md-offset-5 col-sm-offset-5 col-md-2 col-sm-2 col-xs-12 text-center">
        {!! Html::decode(Form::label('image','Ảnh <span class="required_data">*</span>',['class' => 'font-weight-bold float-left mr-4'])) !!}
        {!! Form::file('image', ['class' => 'form-control w-75' ,'accept' => 'image/*' , 'name' => 'image' ,'placeholder' => 'Ảnh' , 'id' => 'image' ]) !!}
    </div>

    <!-- Status Field -->
    <div class="form-group col-sm-12 col-xs-12 col-md-12">
        {!! Form::label('activated', 'Trạng thái :') !!}
        <div class="material-switch float-left">
            {!! Form::hidden('status', false) !!}
            <?php
            $array_atr = ['id' => 'someSwitchOptionSuccess'];
            if (!empty($model) && $model != '') {
                if ($model->status == 1) {
                    $array_atr['checked'] = 'checked';
                }
            } else {
                $array_atr['checked'] = 'checked';
            }
            ?>
            {!! Form::checkbox('status', 1, null, $array_atr) !!}
            <label for="someSwitchOptionSuccess" class="label-success"></label>

        </div>

    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 col-xs-12 row text-center">
    <a href="{!! route('admins.index') !!}" class="btn btn-default">Thoát </a>
    {!! Form::submit('Lưu', ['class' => 'btn btn-success']) !!}
</div>
<div class="clearfix"></div>
