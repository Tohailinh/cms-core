<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $recruitment->id !!}</p>
</div>

<!-- Position Field -->
<div class="form-group">
    {!! Form::label('position', 'Position:') !!}
    <p>{!! $recruitment->position !!}</p>
</div>

<!-- Alias Field -->
<div class="form-group">
    {!! Form::label('alias', 'Alias:') !!}
    <p>{!! $recruitment->alias !!}</p>
</div>

<!-- Quantity Field -->
<div class="form-group">
    {!! Form::label('quantity', 'Quantity:') !!}
    <p>{!! $recruitment->quantity !!}</p>
</div>

<!-- Type Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:') !!}
    <p>{!! $recruitment->type !!}</p>
</div>

<!-- Experience Field -->
<div class="form-group">
    {!! Form::label('experience', 'Experience:') !!}
    <p>{!! $recruitment->experience !!}</p>
</div>

<!-- Salary Field -->
<div class="form-group">
    {!! Form::label('salary', 'Salary:') !!}
    <p>{!! $recruitment->salary !!}</p>
</div>

<!-- Diploma Field -->
<div class="form-group">
    {!! Form::label('diploma', 'Diploma:') !!}
    <p>{!! $recruitment->diploma !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $recruitment->description !!}</p>
</div>

<!-- Benefit Field -->
<div class="form-group">
    {!! Form::label('benefit', 'Benefit:') !!}
    <p>{!! $recruitment->benefit !!}</p>
</div>

<!-- Requirement Field -->
<div class="form-group">
    {!! Form::label('requirement', 'Requirement:') !!}
    <p>{!! $recruitment->requirement !!}</p>
</div>

<!-- Profile Field -->
<div class="form-group">
    {!! Form::label('profile', 'Profile:') !!}
    <p>{!! $recruitment->profile !!}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    <p>{!! $recruitment->image !!}</p>
</div>

<!-- Thumb Field -->
<div class="form-group">
    {!! Form::label('thumb', 'Thumb:') !!}
    <p>{!! $recruitment->thumb !!}</p>
</div>

<!-- Time Out Field -->
<div class="form-group">
    {!! Form::label('time_out', 'Time Out:') !!}
    <p>{!! $recruitment->time_out !!}</p>
</div>

<!-- Time Field -->
<div class="form-group">
    {!! Form::label('time', 'Time:') !!}
    <p>{!! $recruitment->time !!}</p>
</div>

<!-- Category Id Field -->
<div class="form-group">
    {!! Form::label('category_id', 'Category Id:') !!}
    <p>{!! $recruitment->category_id !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $recruitment->status !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $recruitment->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $recruitment->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $recruitment->deleted_at !!}</p>
</div>

