@extends('frontend.layouts.master')
@section('content')
<div class="container">



    <!-- breadcrumb -->
    <div class="n3-breadcrumb mt-30">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="breadcrumb">
                        <span><a href="" itemprop="url"><span itemprop="title">Du lịch</span></a></span> »
                        <span><a href="" itemprop="url"><span itemprop="title">Du lịch trong nước</span></a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container n3-news mg-bot40">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 l">
                <div class="frame-dt-top">
                    <h1 class="news-detail-title">
                        Nét thu Hà Thành
                    </h1>
                    <div class="frame-date">
                        <div class="f-left"><img src="{{asset('')}}layouts/images/i-date.png" alt="date"></div>
                        <div class="f-left date">29/08/2019</div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="frame-dt-bot">
                    Mỗi độ heo may về, người dân thủ đô không thể không nhớ nét thu Hà thành gắn liền với mùi hương hoa sữa nồng nàn giăng đầy trên phố, những con đường dài xào xạc lá vàng thoát nét mộng mị, bờ hồ nằm lặng im ngắm mùa lá đổ… Và bức tranh thu Hà Nội sẽ không hoàn chỉnh nếu thiếu vắng mùi hương hoa sữa hay nhúm nếp xanh bọc trong lá sen mang hương thơm ngầy ngậy của cốm làng Vòng.
                </div>
                <div class="frame-dt-content">
                </div>
                <div class="frame-dt-content">
                    <h2><strong>Nồng n&#224;n hương sắc m&#249;a thu</strong></h2>
                    <img rel="full" src="{{asset('')}}layouts/images/hoa-sua-ha-noi.jpg" title="Nồng n&#224;n hương sắc m&#249;a thu" alt="Nồng n&#224;n hương sắc m&#249;a thu">
                    <div>
                        <div style="text-align: justify;">Như đã mặc định sẵn, hương hoa sữa trở thành hương thu của Hà Nội trong tâm trí người bản địa lẫn du khách thập phương. Không rực cháy như phượng vĩ ngày hè, cũng chẳng phô sắc tím lộng lẫy giống bằng lăng… hoa sữa âm thầm nở khi trời thủ đô chuyển mình sang thu. Loài hoa quyến rũ lạ thường có sức mạnh làm thức tỉnh cùng lúc nhiều giác quan và cứ thế nhẹ nhàng đi vào lòng người.<br>
                            <br>
                            Mỗi mùa hoa rộ, cái nồng nàn theo gió đưa đi len lỏi khắp các con đường ngang dọc ru tâm hồn lãng du vào miền tình tứ, hứa hẹn mùa yêu thương chớm nở. Những cô gái đang mỹ miều tuổi hoa, những chàng trai Hà thành thanh lịch, họ đợi chờ nhau đầu con hẻm nhỏ, hoa sữa đứng đó hồn nhiên trở thành nhân chứng đặc biệt của mối lương duyên. Hương hoa quyện vào heo may khẽ vuốt nhẹ suối tóc buông dài, thoang thoảng trên đôi vai trần mềm mại người thiếu nữ. Nhớ những chiều rảo bước dạo hồ Tây lộng gió, ngắt một chùm thơm ngát cài lên mái tóc thơ, ghi lại mùa thu xao xuyến…<br>
                             </div>

                    </div>
                    <img rel="full" src="{{asset('')}}layouts/images/hoa-sua_1.jpg" title="N&#233;t thu H&#224; th&#224;nh" alt="N&#233;t thu H&#224; th&#224;nh">
                    <div>
                        <div style="text-align: justify;">​Những đêm thanh tĩnh là lúc thưởng hoa trọn vẹn nhất. Khi phố thị dần xa nhịp sống ồn ã, cái chói chang của ánh mặt trời nhường chỗ cho nét dịu dàng ánh trăng, không gian trầm lặng tỏa đèn vàng, cũng là lúc hoa sữa độc tôn. Ai từng thong thả tản bộ dưới phố lên đèn, cảm nhận hương thu Hà Nội vương vấn nơi nơi, ướp đều mọi góc phố mới thấy yêu da diết, mặn nồng.<br>
                            <br>
                            Chẳng biết từ bao giờ, hoa sữa gắn liền với nét thu Hà thành, với nét thanh lịch của người <a title="Tour Tràng An" href="https://travel.com.vn/mien-bac/tour-trang-an.aspx?utm_source=internalbl&utm_medium=click&utm_campaign=ATLinks" target="_blank">Tràng An</a> và thân thuộc đến thế. Hoa được trồng dọc các con đường, những người yêu hoa cũng dựng cho mình không gian riêng tại sân vườn, đêm đêm vừa được thưởng thức hương nồng vừa nhâm nhi tách trà thơm buông vài câu chuyện thì còn gì bằng. Lâu dần, mỗi độ nghe mùi hoa sữa thoang thoảng trong gió là người ta biết được thu đã về ngang ngõ.</div>

                    </div>
                    <h2><strong>Hương vị hồn qu&#234; đất Việt</strong></h2>
                    <img rel="full" src="{{asset('')}}layouts/images/com-ha-noi.jpg" title="Hương vị hồn qu&#234; đất Việt" alt="Hương vị hồn qu&#234; đất Việt">
                    <div>
                        <div style="text-align: justify;">Nhắc đến cốm làng Vòng, người ta nhớ ngay vị ngọt lành, hạt dẻo màu xanh mát dịu cùng hương thơm đặc trưng phảng phất mùi lúa non lan tỏa ở đầu lưỡi. Thức quà mang hồn quê chân chất ấy là sự kết tinh độc đáo từ hương vị đất trời và bàn tay đảm đang, tỉ mỉ của người làm cốm. <br>
                            <br>
                            Để cho ra lò một mẻ cốm ngon tròn vị, người ta phải rất cầu kỳ, cẩn thận và chỉnh chu ngay từ khâu chọn hạt lúa ban đầu. Hạt nếp non mọng sữa được thu hoạch đúng thời điểm thì thành phẩm nấu ra mới dẻo thơm đúng điệu. Thật không hề đơn giản vì gặt hái sớm hạt cốm sẽ không dẻo, còn nếu muộn thì mất đi hương thơm đặc trưng. Hạt thóc được đãi thật sạch, sau khi rang trên ngọn lửa đều tay rất cẩn thận rồi được giã vừa độ nhiều lần thì gói vào lớp lá ray xanh mát giữ độ mềm dẻo và không phai nhạt màu xanh ngọc. Cuối cùng, cốm được gói trọn trong chiếc lá sen buộc lại bằng sơi lạt mềm, chỉ có lá sen mới giữ nguyên vẹn hương vị tươi ngon cho đến khi thưởng thức. Có lẽ vì vậy mà không ai không biết đến mối “lương duyên” trời định giữa cốm xanh và lá sen, đó là nét đẹp mùa thu Hà thành được gói vào chiếc khăn thương nhớ.<br>
                             </div>

                    </div>
                    <img rel="full" src="{{asset('')}}layouts/images/com-non-ha-noi.jpg" title="N&#233;t thu H&#224; th&#224;nh" alt="N&#233;t thu H&#224; th&#224;nh">
                    <div>
                        <div style="text-align: justify;">Có người từng nói rằng: “Nhiều người nhớ thương Hà Nội vì nhớ hương cốm non ngày gió heo may ùa về”. Quả thật như thế, khi hơi sương giăng đầy con phố, cơn gió nhẹ thổi quanh mặt hồ, những chiếc lá sen to dần chuyển già, kết đọng nét tinh túy nhất của trời thu, cũng là lúc hương cốm mùa mới theo chân gánh hàng rong đi qua từng con hẻm nhỏ… Hương thơm thanh mát ấy rất dễ ru lòng người vào xúc cảm nao nao. <br>
                            <br>
                            Ai trót nặng lòng vì thu Hà Nội sẽ không tránh khỏi những phút bâng khuâng khi hoài niệm về miền ký ức xưa cũ. Ngày trước, người sành ẩm thực sẽ không lãng phí những buổi sáng ngồi thưởng thức cốm thơm và nhâm nhi chén chè xanh ngọt lành. Mà kỳ lạ thay, chỉ có ăn cốm vào mùa thu mới cảm nhận hết được mỹ vị từ đất trời. Sự hòa quyện tuyệt vời cả hương, sắc lẫn vị trong từng hạt dẻo cộng thêm chất thanh tao của nước chè xanh ấm nóng, nâng đỡ cho cuộc nhân duyên mùa thu thêm ý nhị. <br>
                            <br>
                            Rồi đây, mỗi độ thu về, người yêu nét thu Hà thành lại lâng lâng cảm giác thèm muốn mua gói cốm Vòng để vừa thưởng thức vừa tận hưởng hết nét đẹp trong lòng phố. Và rồi ai cũng sẽ nhắc mãi đến một mùa thu thủ đô – “mùa hoa sữa về thơm từng cơn gió, mùa cốm xanh về thơm bàn tay nhỏ, cốm sữa vỉa hè thơm bước chân qua…”</div>

                    </div>
                    <h2><strong>M&#243;n ngon từ cốm l&#224;ng V&#242;ng</strong></h2>
                    <img rel="full" src="{{asset('')}}layouts/images/banh-com_1.jpg" title="M&#243;n ngon từ cốm l&#224;ng V&#242;ng" alt="M&#243;n ngon từ cốm l&#224;ng V&#242;ng">
                    <div>
                        <div style="text-align: justify;">Chè cốm: Món chè được lòng rất nhiều thực khách sành ăn với vị ngọt thanh tao, hương thơm thoang thoảng, vị nhè nhẹ nơi đầu lưỡi rồi tan ra đậm đà. Phải thật chậm rãi khi nhấm nháp thì mới có thể cảm nhận hết sự tinh tế của mòn ăn này.<br>
                            <br>
                            Xôi cốm: Đây không chỉ là “mỹ vị” mùa thu mà còn trông thật “mỹ quan”: Màu xanh non của cốm hòa vào màu trắng ngà hạt sen điểm xuyết màu vàng đỗ xanh và trắng sữa từ cọng dừa… Hạt xôi dẻo mềm mang sự hòa quyện tinh tế giữa hương vị thuần khiết và màu sắc, không những khiến thực khách vừa ý với mùi vị mà còn mãn nhãn cùng món ngon.<br>
                            <br>
                            Chả cốm: Món ăn không thể thiếu trên bàn ăn mỗi độ thu về. Miếng chả nóng hổi quện cốm dính vào thịt xay cho ta vị ngọt đậm đà bên trong và lớp vỏ ngoài giòn rụm khi rán lên, món ngon thấm vào vị giác những xúc cảm khó tả được bằng lời.<br>
                            <br>
                            Bánh cốm: Loại bánh không thể thiếu trong lễ hỏi cưới của người Kinh Kỳ. Mỗi chiếc bánh mỏng, vỏ ngoài màu xanh lá mạ từ cốm và có thể nhìn rõ lớp đậu xanh vàng óng bên trong nhân bánh.<br>
                             </div>

                    </div>
                    <img rel="full" src="{{asset('')}}layouts/images/qua-hong.jpg" title="N&#233;t thu H&#224; th&#224;nh" alt="N&#233;t thu H&#224; th&#224;nh">
                    <div>
                        <div style="text-align: justify;">Ngoài ra, cốm làng Vòng khi ăn cùng quả hồng trứng chín mộng hay chuối tiêu trứng cuốc cũng sẽ tặng cho người ăn hương vị đặc sắc khó quên.</div>

                    </div>
                </div>
                <div class="frame-dt-content">
                </div>

                <div class="tinlienquan">
                    <div class="i-title-sub">TIN TỨC KHÁC</div>
                    <ul class="row list-tinkhac">
                        <li class="col-md-6 col-sm-6 col-xs-12 dot-dot cut-name" style="overflow-wrap: break-word;">
                            <a href="new_info.html" title="N&#233;t thu H&#224; th&#224;nh">N&#233;t thu H&#224; th&#224;nh</a>
                        </li>
                        <li class="col-md-6 col-sm-6 col-xs-12 dot-dot cut-name" style="overflow-wrap: break-word;">
                            <a href="new_info.html" title="Săn bắc cực quang ở Yellowknife">Săn bắc cực quang ở Yellowknife</a>
                        </li>
                        <li class="col-md-6 col-sm-6 col-xs-12 dot-dot cut-name" style="overflow-wrap: break-word;">
                            <a href="new_info.html" title="Đẹp dịu d&#224;ng thu v&#224;ng phương Bắc">Đẹp dịu d&#224;ng thu v&#224;ng phương Bắc</a>
                        </li>
                        <li class="col-md-6 col-sm-6 col-xs-12 dot-dot cut-name" style="overflow-wrap: break-word;">
                            <a href="new_info.html" title="Th&#225;ng 9 n&#224;y, l&#234;n Mộc Ch&#226;u đ&#243;n Tết Độc Lập">Th&#225;ng 9 n&#224;y, l&#234;n Mộc Ch&#226;u đ&#243;n Tết Độc Lập</a>
                        </li>
                        <li class="col-md-6 col-sm-6 col-xs-12 dot-dot cut-name" style="overflow-wrap: break-word;">
                            <a href="new_info.html" title="Ngắm m&#249;a thu tại “thủ đ&#244; tri thức” nước Mỹ">Ngắm m&#249;a thu tại “thủ đ&#244; tri thức” nước Mỹ</a>
                        </li>
                        <li class="col-md-6 col-sm-6 col-xs-12 dot-dot cut-name" style="overflow-wrap: break-word;">
                            <a href="new_info.html" title="C&#243; g&#236; &#39;hot&#39; tại Hội chợ Du lịch Quốc tế ITE 2019?">C&#243; g&#236; &#39;hot&#39; tại Hội chợ Du lịch Quốc tế ITE 2019?</a>
                        </li>
                        <li class="col-md-6 col-sm-6 col-xs-12 dot-dot cut-name" style="overflow-wrap: break-word;">
                            <a href="new_info.html" title="“Điểm danh” c&#225;c loại b&#225;nh đặc sắc miền T&#226;y">“Điểm danh” c&#225;c loại b&#225;nh đặc sắc miền T&#226;y</a>
                        </li>
                        <li class="col-md-6 col-sm-6 col-xs-12 dot-dot cut-name" style="overflow-wrap: break-word;">
                            <a href="new_info.html" title="Vietravel Long An ưu đ&#227;i &#39;khủng&#39; kỷ niệm 1 năm th&#224;nh lập">Vietravel Long An ưu đ&#227;i &#39;khủng&#39; kỷ niệm 1 năm th&#224;nh lập</a>
                        </li>
                        <li class="col-md-6 col-sm-6 col-xs-12 dot-dot cut-name" style="overflow-wrap: break-word;">
                            <a href="new_info.html" title="C&#225;t B&#224;, sơn thủy hữu t&#236;nh">C&#225;t B&#224;, sơn thủy hữu t&#236;nh</a>
                        </li>
                        <li class="col-md-6 col-sm-6 col-xs-12 dot-dot cut-name" style="overflow-wrap: break-word;">
                            <a href="new_info.html" title="C&#243; g&#236; ở lễ hội hoa Floriade lớn nhất Nam b&#225;n cầu?">C&#243; g&#236; ở lễ hội hoa Floriade lớn nhất Nam b&#225;n cầu?</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!--NEW-->
    <div class="container n3-tour-hour">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="title mg-bot10">
                    <a href="">Tour hấp dẫn</a>
                </h2>
            </div>
            <div id="TourHot">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 mg-bot30 wow animated rollIn">
                    <a href="tour_info.html" title="Nha Trang - Hòn Lao - Thế Giới Giải Trí Vinpearl Land (Xe. Tour Mua Ngay) - Giá đã giảm 1.000.000đồng/1 khách - Lễ 2/9">
                        <div class="pos-relative">
                            <img src="{{asset('')}}layouts/images/lm_190828011845_382500.jpg" class="img-responsive pic-tgc" alt="Nha Trang - Hòn Lao - Thế Giới Giải Trí Vinpearl Land (Xe. Tour Mua Ngay) - Giá đã giảm 1.000.000đồng/1 khách - Lễ 2/9">
                            <div class="frame-tgc1">
                                <div class="row">
                                    <div class="col-lg-9 col-md-8 col-sm-8 col-xs-8">
                                        <div class="f-left">
                                            <img src="{{asset('')}}layouts/images/i-date-w.png" alt="date">
                                        </div>
                                        <div class="f-left date"><span class="yellow">30/08/2019</span> - <span class="yellow">4 ngày</span></div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4">
                                        <div class="f-left">
                                            <img src="{{asset('')}}layouts/images/i-chair-w.png" alt="chair">
                                        </div>
                                        <div class="f-left chair"><span class="yellow">5 chỗ </span></div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="frame-tgc2">

                            <div class="tgc-title dot-dot-ajax cut-tgc ddd-truncated" style="overflow-wrap: break-word;">Nha Trang - Hòn Lao - Thế Giới Giải Trí Vinpearl… </div>

                            <div class="tgc-line"></div>
                            <div class="mg-bot10">
                                <img src="{{asset('')}}layouts/images/i-price.png" class="f-left" alt="price">
                                <div class="f-left tgc-info"><span class="price-o">3,790,000đ
                                </span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="price-n">3,290,000 đ</span>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 mg-bot30 wow animated rollIn">
                    <a href="tour_info.html" title="Nha Trang - Hòn Lao - Thế Giới Giải Trí Vinpearl Land (Xe. Tour Mua Ngay) - Giá đã giảm 1.000.000đồng/1 khách - Lễ 2/9">
                        <div class="pos-relative">
                            <img src="{{asset('')}}layouts/images/lm_190826042058_677653.jpg" class="img-responsive pic-tgc" alt="Nha Trang - Hòn Lao - Thế Giới Giải Trí Vinpearl">
                            <div class="frame-tgc1">
                                <div class="row">
                                    <div class="col-lg-9 col-md-8 col-sm-8 col-xs-8">
                                        <div class="f-left">
                                            <img src="{{asset('')}}layouts/images/i-date-w.png" alt="date">
                                        </div>
                                        <div class="f-left date"><span class="yellow">30/08/2019</span> - <span class="yellow">4 ngày</span></div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4">
                                        <div class="f-left">
                                            <img src="{{asset('')}}layouts/images/i-chair-w.png" alt="chair">
                                        </div>
                                        <div class="f-left chair"><span class="yellow">5 chỗ </span></div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="frame-tgc2">

                            <div class="tgc-title dot-dot-ajax cut-tgc ddd-truncated" style="overflow-wrap: break-word;">Nha Trang - Hòn Lao - Thế Giới Giải Trí Vinpearl… </div>

                            <div class="tgc-line"></div>
                            <div class="mg-bot10">
                                <img src="{{asset('')}}layouts/images/i-price.png" class="f-left" alt="price">
                                <div class="f-left tgc-info"><span class="price-o">3,790,000đ
                                </span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="price-n">3,290,000 đ</span>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 mg-bot30 wow animated rollIn">
                    <a href="tour_info.html" title="Nha Trang - Hòn Lao - Thế Giới Giải Trí Vinpearl Land (Xe. Tour Mua Ngay) - Giá đã giảm 1.000.000đồng/1 khách - Lễ 2/9">
                        <div class="pos-relative">
                            <img src="{{asset('')}}layouts/images/lm_190826022923_622124.jpg" class="img-responsive pic-tgc" alt="Nha Trang - Hòn Lao - Thế Giới Giải Trí Vinpearl Land (Xe. Tour Mua Ngay) - Giá đã giảm 1.000.000đồng/1 khách - Lễ 2/9">
                            <div class="frame-tgc1">
                                <div class="row">
                                    <div class="col-lg-9 col-md-8 col-sm-8 col-xs-8">
                                        <div class="f-left">
                                            <img src="{{asset('')}}layouts/images/i-date-w.png" alt="date">
                                        </div>
                                        <div class="f-left date"><span class="yellow">30/08/2019</span> - <span class="yellow">4 ngày</span></div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4">
                                        <div class="f-left">
                                            <img src="{{asset('')}}layouts/images/i-chair-w.png" alt="chair">
                                        </div>
                                        <div class="f-left chair"><span class="yellow">5 chỗ </span></div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="frame-tgc2">

                            <div class="tgc-title dot-dot-ajax cut-tgc ddd-truncated" style="overflow-wrap: break-word;">Nha Trang - Hòn Lao - Thế Giới Giải Trí Vinpearl… </div>

                            <div class="tgc-line"></div>
                            <div class="mg-bot10">
                                <img src="{{asset('')}}layouts/images/i-price.png" class="f-left" alt="price">
                                <div class="f-left tgc-info"><span class="price-o">3,790,000đ
                                </span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="price-n">3,290,000 đ</span>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>



</div>
@endsection
