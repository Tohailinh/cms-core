@extends('frontend.layouts.master')
@section('content')
<div class="container">

    <!-- breadcrumb -->
    <div class="n3-breadcrumb mt-30">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="breadcrumb">
                        <span><a href="" itemprop="url"><span itemprop="title">Du lịch</span></a></span> »
                        <span><a href="" itemprop="url"><span itemprop="title">Du lịch trong nước</span></a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container n3-news mg-bot40">
        <div class="row">
            <div class="col-xs-12 mg-bot30">
                <div class="title"><h1>Khuyến mại</h1></div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 l">
                <div class="row mg-bot30">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 mg-bot">
                        <a href="chitiet_khuyenmai.html" title="Có gì ở lễ hội hoa Floriade lớn nhất Nam bán cầu?">
                            <img src="{{asset('')}}layouts/images/le-hoi-hoa-mua-xuan-nuoc-uc.jpg" class="img-responsive pic-news-l" alt="Có gì ở lễ hội hoa Floriade lớn nhất Nam bán cầu?">
                        </a>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                        <div class="frame-news">
                            <div class="frame-top">
                                <h2 class="news-title-l">
                                    <a href="chitiet_khuyenmai.html" title="Có gì ở lễ hội hoa Floriade lớn nhất Nam bán cầu?" class="dot-dot cut-name" style="overflow-wrap: break-word;">
                                        Có gì ở lễ hội hoa Floriade lớn nhất Nam bán cầu?
                                    </a>
                                </h2>
                                <div class="frame-date">
                                    <div class="f-left"><img src="{{asset('')}}layouts/images/i-date.png" alt="date"></div>
                                    <div class="f-left date">21/08/2019</div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="frame-bot">
                                <div class="des-content dot-dot cut-content ddd-truncated" style="overflow-wrap: break-word;">
                                    Floriade là tên của một lễ hội hoa mùa xuân ở Úc. Lễ hội kéo dài một tháng, và là lễ hội lớn nhất phía Nam bán cầu.
                                </div>
                                <div class="text-right">
                                    <a href="chitiet_khuyenmai.html" class="view_more" title="Có gì ở lễ hội hoa Floriade lớn nhất Nam bán cầu?">Xem th&#234;m &nbsp;<i class="fas fa-long-arrow-alt-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mg-bot30">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 mg-bot">
                        <a href="chitiet_khuyenmai.html" title="Vietravel độc quyền cung ứng v&#233; bay Druk Air - Royal Bhutan Airlines tại Việt Nam"><img src="{{asset('')}}layouts/images/0-VietravelinBhutan.jpg" class="img-responsive pic-news-l" alt="Vietravel độc quyền cung ứng v&#233; bay Druk Air - Royal Bhutan Airlines tại Việt Nam"></a>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                        <div class="frame-news">
                            <div class="frame-top">
                                <h2 class="news-title-l">
                                    <a href="chitiet_khuyenmai.html" title="Vietravel độc quyền cung ứng v&#233; bay Druk Air - Royal Bhutan Airlines tại Việt Nam" class="dot-dot cut-name" style="overflow-wrap: break-word;">Vietravel độc quyền cung ứng v&#233; bay Druk Air - Royal Bhutan Airlines tại Việt Nam</a>
                                </h2>
                                <div class="frame-date">
                                    <div class="f-left"><img src="{{asset('')}}layouts/images/i-date.png" alt="date"></div>
                                    <div class="f-left date">20/08/2019</div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="frame-bot">
                                <div class="des-content dot-dot cut-content ddd-truncated" style="overflow-wrap: break-word;">
                                    Ng&#224;y 19/8/2019 - C&#244;ng ty Du lịch Vietravel đ&#227; tổ chức Lễ k&#253; kết hợp đồng hợp t&#225;c ph&#225;t triển giữa Vietravel v&#224; Druk Air - Royal Bhutan Airlines tại Trụ sở ch&#237;nh (190 Pasteur, P.6, Q.3, TP.HCM). Theo đ&#243;, từ th&#225;ng 8/2019, Vietravel ch&#237;nh thức trở th&#224;nh Tổng Đại l&#253; độc quyền ph&#226;n phối v&#233; m&#225;y bay của H&#224;ng kh&#244;ng quốc gia Bhutan (Druk Air) tại thị trường Việt Nam.
                                </div>
                                <div class="text-right">
                                    <a href="chitiet_khuyenmai.html" class="view_more" title="Vietravel độc quyền cung ứng v&#233; bay Druk Air - Royal Bhutan Airlines tại Việt Nam">Xem th&#234;m &nbsp;<i class="fas fa-long-arrow-alt-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mg-bot30">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 mg-bot">
                        <a href="chitiet_khuyenmai.html" title="Ưu đ&#227;i đến 1.000.000 đồng/kh&#225;ch từ c&#225;c đối t&#225;c t&#224;i ch&#237;nh"><img src="{{asset('')}}layouts/images/uu-dai-tai-chinh-den-1trieu_640x424.jpg" class="img-responsive pic-news-l" alt="Ưu đ&#227;i đến 1.000.000 đồng/kh&#225;ch từ c&#225;c đối t&#225;c t&#224;i ch&#237;nh"></a>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                        <div class="frame-news">
                            <div class="frame-top">
                                <h2 class="news-title-l">
                                    <a href="chitiet_khuyenmai.html" title="Ưu đ&#227;i đến 1.000.000 đồng/kh&#225;ch từ c&#225;c đối t&#225;c t&#224;i ch&#237;nh" class="dot-dot cut-name" style="overflow-wrap: break-word;">Ưu đ&#227;i đến 1.000.000 đồng/kh&#225;ch từ c&#225;c đối t&#225;c t&#224;i ch&#237;nh</a>
                                </h2>
                                <div class="frame-date">
                                    <div class="f-left"><img src="{{asset('')}}layouts/images/i-date.png" alt="date"></div>
                                    <div class="f-left date">19/08/2019</div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="frame-bot">
                                <div class="des-content dot-dot cut-content ddd-truncated" style="overflow-wrap: break-word;">
                                    Ưu đ&#227;i đến 1.000.000 đồng/kh&#225;ch từ c&#225;c đối t&#225;c t&#224;i ch&#237;nh
                                </div>
                                <div class="text-right">
                                    <a href="chitiet_khuyenmai.html" class="view_more" title="Ưu đ&#227;i đến 1.000.000 đồng/kh&#225;ch từ c&#225;c đối t&#225;c t&#224;i ch&#237;nh">Xem th&#234;m &nbsp;<i class="fas fa-long-arrow-alt-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mg-bot30">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 mg-bot">
                        <a href="chitiet_khuyenmai.html" title="&quot;Đi t&#236;m&quot; m&#249;a xu&#226;n tại Nam b&#225;n cầu giữa ng&#224;y thu"><img src="{{asset('')}}layouts/images/mua-xuan-nam-ban-cau.jpg" class="img-responsive pic-news-l" alt="&quot;Đi t&#236;m&quot; m&#249;a xu&#226;n tại Nam b&#225;n cầu giữa ng&#224;y thu"></a>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                        <div class="frame-news">
                            <div class="frame-top">
                                <h2 class="news-title-l">
                                    <a href="chitiet_khuyenmai.html" title="&quot;Đi t&#236;m&quot; m&#249;a xu&#226;n tại Nam b&#225;n cầu giữa ng&#224;y thu" class="dot-dot cut-name" style="overflow-wrap: break-word;">&quot;Đi t&#236;m&quot; m&#249;a xu&#226;n tại Nam b&#225;n cầu giữa ng&#224;y thu</a>
                                </h2>
                                <div class="frame-date">
                                    <div class="f-left"><img src="{{asset('')}}layouts/images/i-date.png" alt="date"></div>
                                    <div class="f-left date">19/08/2019</div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="frame-bot">
                                <div class="des-content dot-dot cut-content ddd-truncated" style="overflow-wrap: break-word;">
                                    Khi m&#224; nhiều nước tr&#234;n thế giới đang chuẩn bị đ&#243;n một m&#249;a thu dịu d&#224;ng, l&#227;ng mạn th&#236; v&#224;o khoảng th&#225;ng 9 đến th&#225;ng 11 lại l&#224; m&#249;a xu&#226;n tại Nam b&#225;n cầu với gam m&#224;u rực sắc hoa phượng t&#237;m ở Nam Phi hay &#218;c. Nếu bạn đ&#227; từng m&#234; mẩn m&#249;a thu nhuộm trong m&#224;u l&#225; đỏ th&#236; chắc hẳn bạn cũng sẽ ng&#226;y ngất với &#225;nh t&#237;m nhẹ nh&#224;ng của hoa phượng.
                                </div>
                                <div class="text-right">
                                    <a href="chitiet_khuyenmai.html" class="view_more" title="&quot;Đi t&#236;m&quot; m&#249;a xu&#226;n tại Nam b&#225;n cầu giữa ng&#224;y thu">Xem th&#234;m &nbsp;<i class="fas fa-long-arrow-alt-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mg-bot30">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 mg-bot">
                        <a href="chitiet_khuyenmai.html" title="Vietravel &#39;Go Green&#39; c&#249;ng ng&#224;nh du lịch g&#243;p phần thu h&#250;t kh&#225;ch quốc tế đến Việt Nam"><img src="{{asset('')}}layouts/images/0-go-green-2019.jpg" class="img-responsive pic-news-l" alt="Vietravel &#39;Go Green&#39; c&#249;ng ng&#224;nh du lịch g&#243;p phần thu h&#250;t kh&#225;ch quốc tế đến Việt Nam"></a>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                        <div class="frame-news">
                            <div class="frame-top">
                                <h2 class="news-title-l">
                                    <a href="chitiet_khuyenmai.html" title="Vietravel &#39;Go Green&#39; c&#249;ng ng&#224;nh du lịch g&#243;p phần thu h&#250;t kh&#225;ch quốc tế đến Việt Nam" class="dot-dot cut-name" style="overflow-wrap: break-word;">Vietravel &#39;Go Green&#39; c&#249;ng ng&#224;nh du lịch g&#243;p phần thu h&#250;t kh&#225;ch quốc tế đến Việt Nam</a>
                                </h2>
                                <div class="frame-date">
                                    <div class="f-left"><img src="{{asset('')}}layouts/images/i-date.png" alt="date"></div>
                                    <div class="f-left date">16/08/2019</div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="frame-bot">
                                <div class="des-content dot-dot cut-content ddd-truncated" style="overflow-wrap: break-word;">
                                    Ng&#224;y 16/8/2019 tại TP. Đ&#224; Nẵng, Vietravel phối hợp c&#249;ng Sở, ban, ng&#224;nh Đ&#224; Nẵng, ch&#237;nh thức ph&#225;t động chương tr&#236;nh “Go Green - Du lịch xanh” c&#249;ng ng&#224;nh Du lịch. Đẩy mạnh hoạt động bảo vệ m&#244;i trường du lịch đ&#225;p ứng y&#234;u cầu ph&#225;t triển bền vững, v&#224; g&#243;p phần tăng cường thu h&#250;t kh&#225;ch quốc tế đến Việt Nam.
                                </div>
                                <div class="text-right">
                                    <a href="chitiet_khuyenmai.html" class="view_more" title="Vietravel &#39;Go Green&#39; c&#249;ng ng&#224;nh du lịch g&#243;p phần thu h&#250;t kh&#225;ch quốc tế đến Việt Nam">Xem th&#234;m &nbsp;<i class="fas fa-long-arrow-alt-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mg-bot30">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 mg-bot">
                        <a href="chitiet_khuyenmai.html" title="Hokkaido, v&#249;ng đất đ&#243;n m&#249;a thu sớm nhất Nhật Bản"><img src="{{asset('')}}layouts/images/0-hokkaido_1.jpg" class="img-responsive pic-news-l" alt="Hokkaido, v&#249;ng đất đ&#243;n m&#249;a thu sớm nhất Nhật Bản"></a>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                        <div class="frame-news">
                            <div class="frame-top">
                                <h2 class="news-title-l">
                                    <a href="chitiet_khuyenmai.html" title="Hokkaido, v&#249;ng đất đ&#243;n m&#249;a thu sớm nhất Nhật Bản" class="dot-dot cut-name" style="overflow-wrap: break-word;">Hokkaido, v&#249;ng đất đ&#243;n m&#249;a thu sớm nhất Nhật Bản</a>
                                </h2>
                                <div class="frame-date">
                                    <div class="f-left"><img src="{{asset('')}}layouts/images/i-date.png" alt="date"></div>
                                    <div class="f-left date">14/08/2019</div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="frame-bot">
                                <div class="des-content dot-dot cut-content ddd-truncated" style="overflow-wrap: break-word;">
                                    Nằm ở ph&#237;a Bắc, Hokkaido l&#224; v&#249;ng đất đ&#243;n m&#249;a thu sớm nhất Nhật Bản. Đến đ&#226;y v&#224;o cuối th&#225;ng 9, du kh&#225;ch đ&#227; c&#243; thể “bắt trọn” khoảnh khắc thu v&#244; cũng l&#227;ng mạn.
                                </div>
                                <div class="text-right">
                                    <a href="chitiet_khuyenmai.html" class="view_more" title="Hokkaido, v&#249;ng đất đ&#243;n m&#249;a thu sớm nhất Nhật Bản">Xem th&#234;m &nbsp;<i class="fas fa-long-arrow-alt-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mg-bot30">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 mg-bot">
                        <a href="chitiet_khuyenmai.html" title="Bộ ảnh &#39;Trải nghiệm vượt thời gian&#39; - C&#249;ng Vietravel &#39;Đi trọn thế gian, Thu v&#224;ng hẹn ước&#39;"><img src="{{asset('')}}layouts/images/0-thu-2019.jpg" class="img-responsive pic-news-l" alt="Bộ ảnh &#39;Trải nghiệm vượt thời gian&#39; - C&#249;ng Vietravel &#39;Đi trọn thế gian, Thu v&#224;ng hẹn ước&#39;"></a>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                        <div class="frame-news">
                            <div class="frame-top">
                                <h2 class="news-title-l">
                                    <a href="chitiet_khuyenmai.html" title="Bộ ảnh &#39;Trải nghiệm vượt thời gian&#39; - C&#249;ng Vietravel &#39;Đi trọn thế gian, Thu v&#224;ng hẹn ước&#39;" class="dot-dot cut-name" style="overflow-wrap: break-word;">Bộ ảnh &#39;Trải nghiệm vượt thời gian&#39; - C&#249;ng Vietravel &#39;Đi trọn thế gian, Thu v&#224;ng hẹn ước&#39;</a>
                                </h2>
                                <div class="frame-date">
                                    <div class="f-left"><img src="{{asset('')}}layouts/images/i-date.png" alt="date"></div>
                                    <div class="f-left date">12/08/2019</div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="frame-bot">
                                <div class="des-content dot-dot cut-content ddd-truncated" style="overflow-wrap: break-word;">
                                    M&#249;a thu n&#224;y, h&#227;y y&#234;u nhau thật trọn vẹn. Y&#234;u b&#249; những khoảnh khắc thanh xu&#226;n chưa được c&#249;ng nhau v&#224; y&#234;u lu&#244;n cho t&#236;nh y&#234;u của ch&#250;ng ta sau n&#224;y. C&#249;ng người thương “Đi khắp thế gian” đắm m&#236;nh v&#224;o vẻ đẹp n&#234;n thơ của m&#249;a thu khắp chốn v&#224; lưu giữ những khoảnh khắc t&#236;nh y&#234;u tuyệt vời.
                                </div>
                                <div class="text-right">
                                    <a href="chitiet_khuyenmai.html" class="view_more" title="Bộ ảnh &#39;Trải nghiệm vượt thời gian&#39; - C&#249;ng Vietravel &#39;Đi trọn thế gian, Thu v&#224;ng hẹn ước&#39;">Xem th&#234;m &nbsp;<i class="fas fa-long-arrow-alt-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mg-bot30">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 mg-bot">
                        <a href="chitiet_khuyenmai.html" title="Khuyến mại Thu 2019: &#39;Đi trọn thế gian, Thu v&#224;ng hẹn ước&#39; - Tận hưởng niềm vui du lịch m&#249;a thu"><img src="{{asset('')}}layouts/images/Vietravel-thu2019.jpg" class="img-responsive pic-news-l" alt="Khuyến mại Thu 2019: &#39;Đi trọn thế gian, Thu v&#224;ng hẹn ước&#39; - Tận hưởng niềm vui du lịch m&#249;a thu"></a>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                        <div class="frame-news">
                            <div class="frame-top">
                                <h2 class="news-title-l">
                                    <a href="chitiet_khuyenmai.html" title="Khuyến mại Thu 2019: &#39;Đi trọn thế gian, Thu v&#224;ng hẹn ước&#39; - Tận hưởng niềm vui du lịch m&#249;a thu" class="dot-dot cut-name" style="overflow-wrap: break-word;">Khuyến mại Thu 2019: &#39;Đi trọn thế gian, Thu v&#224;ng hẹn ước&#39; - Tận hưởng niềm vui du lịch m&#249;a thu</a>
                                </h2>
                                <div class="frame-date">
                                    <div class="f-left"><img src="{{asset('')}}layouts/images/i-date.png" alt="date"></div>
                                    <div class="f-left date">10/08/2019</div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="frame-bot">
                                <div class="des-content dot-dot cut-content ddd-truncated" style="overflow-wrap: break-word;">
                                    Từ ng&#224;y 10/8 đến 20/10/2019, Vietravel mang đến cho Qu&#253; kh&#225;ch h&#224;ng chương tr&#236;nh khuyến mại Thu với tổng gi&#225; trị ưu đ&#227;i l&#234;n đến 5 tỷ đồng. Với th&#244;ng điệp “Đi trọn thế gian, Thu v&#224;ng hẹn ước”, m&#249;a du lịch Thu năm nay Vietravel sẽ mang đến cho du kh&#225;ch những chuyến đi với nhiều trải nghiệm ấn tượng, c&#249;ng người th&#226;n tận hưởng n&#233;t đặc trưng của m&#249;a thu tại những v&#249;ng đất lạ.
                                </div>
                                <div class="text-right">
                                    <a href="chitiet_khuyenmai.html" class="view_more" title="Khuyến mại Thu 2019: &#39;Đi trọn thế gian, Thu v&#224;ng hẹn ước&#39; - Tận hưởng niềm vui du lịch m&#249;a thu">Xem th&#234;m &nbsp;<i class="fas fa-long-arrow-alt-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mg-bot30">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 mg-bot">
                        <a href="chitiet_khuyenmai.html" title="Ngập tr&#224;n ưu đ&#227;i M&#249;a Du Lịch Thu c&#249;ng Vietravel"><img src="{{asset('')}}layouts/images/13_mo-mua-thu-2019_1.JPG" class="img-responsive pic-news-l" alt="Ngập tr&#224;n ưu đ&#227;i M&#249;a Du Lịch Thu c&#249;ng Vietravel"></a>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                        <div class="frame-news">
                            <div class="frame-top">
                                <h2 class="news-title-l">
                                    <a href="chitiet_khuyenmai.html" title="Ngập tr&#224;n ưu đ&#227;i M&#249;a Du Lịch Thu c&#249;ng Vietravel" class="dot-dot cut-name" style="overflow-wrap: break-word;">Ngập tr&#224;n ưu đ&#227;i M&#249;a Du Lịch Thu c&#249;ng Vietravel</a>
                                </h2>
                                <div class="frame-date">
                                    <div class="f-left"><img src="{{asset('')}}layouts/images/i-date.png" alt="date"></div>
                                    <div class="f-left date">10/08/2019</div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="frame-bot">
                                <div class="des-content dot-dot cut-content ddd-truncated" style="overflow-wrap: break-word;">
                                    Ng&#224;y 10/08/2019, Vietravel đ&#227; tổ chức sự kiện mở m&#249;a Thu 2019 để khởi động cho chương tr&#236;nh Khuyến mại “Đi trọn thế gian - Thu v&#224;ng hẹn ước”, với tổng gi&#225; trị ưu đ&#227;i l&#234;n đến 5 tỷ đồng. M&#249;a du lịch Thu năm nay, Vietravel hứa hẹn sẽ mang đến cho du kh&#225;ch những chuyến đi với nhiều trải nghiệm ấn tượng, c&#249;ng người th&#226;n tận hưởng n&#233;t đặc trưng của m&#249;a thu tại những v&#249;ng đất lạ.
                                </div>
                                <div class="text-right">
                                    <a href="chitiet_khuyenmai.html" class="view_more" title="Ngập tr&#224;n ưu đ&#227;i M&#249;a Du Lịch Thu c&#249;ng Vietravel">Xem th&#234;m &nbsp;<i class="fas fa-long-arrow-alt-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mg-bot30">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 mg-bot">
                        <a href="chitiet_khuyenmai.html" title="Thể lệ chương tr&#236;nh khuyến mại c&#249;ng Sacombank"><img src="{{asset('')}}layouts/images/uu-dai-sacombank-2_640x424.jpg" class="img-responsive pic-news-l" alt="Thể lệ chương tr&#236;nh khuyến mại c&#249;ng Sacombank"></a>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                        <div class="frame-news">
                            <div class="frame-top">
                                <h2 class="news-title-l">
                                    <a href="chitiet_khuyenmai.html" title="Thể lệ chương tr&#236;nh khuyến mại c&#249;ng Sacombank" class="dot-dot cut-name" style="overflow-wrap: break-word;">Thể lệ chương tr&#236;nh khuyến mại c&#249;ng Sacombank</a>
                                </h2>
                                <div class="frame-date">
                                    <div class="f-left"><img src="{{asset('')}}layouts/images/i-date.png" alt="date"></div>
                                    <div class="f-left date">10/08/2019</div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="frame-bot">
                                <div class="des-content dot-dot cut-content ddd-truncated" style="overflow-wrap: break-word;">
                                    &#193;p dụng với kh&#225;ch h&#224;ng thanh to&#225;n bằng thẻ t&#237;n dụng quốc tế Sacombank mua tất cả c&#225;c tour du lịch do Vietravel cung cấp.
                                </div>
                                <div class="text-right">
                                    <a href="chitiet_khuyenmai.html" class="view_more" title="Thể lệ chương tr&#236;nh khuyến mại c&#249;ng Sacombank">Xem th&#234;m &nbsp;<i class="fas fa-long-arrow-alt-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 text-right mg-bot30">

                    <div class="pager_simple_orange">
                        <table>
                            <tr>
                                <td class=""><a href="tintuc.html"> « </a></td>
                                <td class="active"><a href="tintuc.html">1</a></td>
                                <td><a href="tintuc.html">2</a></td>
                                <td><a href="tintuc.html">3</a></td>
                                <td><a href="tintuc.html">4</a></td>
                                <td><a href="tintuc.html">5</a></td>
                                <td><a href="/tintuc.html">6</a></td>
                                <td class=""><a href="tintuc.html"> » </a></td>
                            </tr>
                        </table>
                    </div>


                </div>
            </div>
        </div>
    </div>


</div>
@endsection
