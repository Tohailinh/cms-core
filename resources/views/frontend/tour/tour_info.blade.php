@extends('frontend.layouts.master')
@section('content')
<div class="container">



    <!-- breadcrumb -->
    <div class="n3-breadcrumb mt-30">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="breadcrumb">
                        <span><a href="" itemprop="url"><span itemprop="title">Du lịch</span></a></span> »
                        <span><a href="" itemprop="url"><span itemprop="title">Du lịch trong nước</span></a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container n3-tour-description mg-bot40 mt-15">

        <div class="row">
            <div class="col-xs-12 mg-bot15">
                <h1 class="tour-name" itemprop="name"><a>Miền1 T&#226;y - Ch&#226;u Đốc - Rừng Tr&#224;m Tr&#224; Sư - H&#224; Ti&#234;n - Rạch Gi&#225; - Cần Thơ (Kh&#225;ch sạn 2*&amp; 3*, Tour Tiết Kiệm)- Lễ 02/09</a></h1>
                <span itemprop="brand" style="display: none;">Vietravel</span>
                <img itemprop="image" style="display: none;" src="{{asset('')}}layouts/images/tf_171122084522_719161.jpg" alt="Miền T&#226;y - Ch&#226;u Đốc - Rừng Tr&#224;m Tr&#224; Sư - H&#224; Ti&#234;n - Rạch Gi&#225; - Cần Thơ (Kh&#225;ch sạn 2*&amp; 3*, Tour Tiết Kiệm)- Lễ 02/09">
                <div class="tour-code"><i class="fas fa-barcode"></i>&nbsp;&nbsp;NDSGN847-049-310819XE-V</div>
            </div>
            <div class="slideshow-pt col-lg-8 col-md-12 col-sm-12 col-xs-12 pos-relative">
                <div id="myCarouse2" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#myCarouse2" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarouse2" data-slide-to="1" class=""></li>
                        <li data-target="#myCarouse2" data-slide-to="2" class=""></li>
                        <li data-target="#myCarouse2" data-slide-to="3" class=""></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <div class="item active">
                            <img src="{{asset('')}}layouts/images/tfd_20160531_Hon-Phu-Tu-Ha-Tien.jpg" alt="nhet tieu de vao day" class="img-responsive pic-ss-pt">
                        </div>
                        <div class="item ">
                            <img src="{{asset('')}}layouts/images/tfd_20151202_tra su.jpg" alt="nhet tieu de vao day" class="img-responsive pic-ss-pt">
                        </div>
                        <div class="item ">
                            <img src="{{asset('')}}layouts/images/tfd_20160531_Hon-Phu-Tu-Ha-Tien.jpg" alt="nhet tieu de vao day" class="img-responsive pic-ss-pt">
                        </div>
                        <div class="item ">
                            <img src="{{asset('')}}layouts/images/tfd_140929_em gai mien tay.jpg" alt="nhet tieu de vao day" class="img-responsive pic-ss-pt">
                        </div>
                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control hidden" href="#myCarousel" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control hidden" href="#myCarousel" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <div class="info col-lg-4 col-md-12 col-sm-12 col-xs-12">
                <div class="frame-info pos-relative">
                    <div class="sec2">
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="row border-bt mg-bot10">
                                    <div class="col-lg-6 col-md-3 col-sm-3 col-xs-6">Ngày Khởi hành:</div>
                                    <div class="col-lg-6 col-md-10 col-sm-9 col-xs-6">
                                        <div class="mg-bot-date">
                                            31/08/2019
                                        </div>
                                    </div>
                                </div>
                                <div class="row border-bt mg-bot10">
                                    <div class="col-lg-6 col-md-2 col-sm-3 col-xs-6" style="padding-right: 0px !important;">Nơi khởi hành:</div>
                                    <div class="col-lg-6 col-md-10 col-sm-9 col-xs-6">
                                        Hà Nội
                                    </div>
                                </div>
                                <div class="row border-bt mg-bot10">
                                    <div class="col-lg-6 col-md-3 col-sm-3 col-xs-6">Ngày về:</div>
                                    <div class="col-lg-6 col-md-10 col-sm-9 col-xs-6">
                                        <div class="mg-bot-date">
                                            31/08/2019
                                        </div>
                                    </div>
                                </div>
                                <div class="row border-bt mg-bot10">
                                    <div class="col-lg-6 col-md-2 col-sm-3 col-xs-6" style="padding-right: 0px !important;">Nơi xuất phát về:</div>
                                    <div class="col-lg-6 col-md-10 col-sm-9 col-xs-6">
                                        Hồ Chí Minh
                                    </div>
                                </div>
                                <div class="row border-bt mg-bot10">
                                    <div class="col-lg-6 col-md-2 col-sm-3 col-xs-6">Tổng thời gian:</div>
                                    <div class="col-lg-6 col-md-10 col-sm-9 col-xs-6">
                                        4 Ngày
                                    </div>
                                </div>
                                <div class="price-tour wow animated pulse mt-15" data-wow-delay="300ms" data-wow-iteration="infinite" data-wow-duration="2s" >
                                    Giá từ : <span class="price">3,290,000đ</span>&nbsp
                                    <span class="price-o">3,790,000đ</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="btn-dattour text-center">
                        <button>
                            Đặt ngay
                        </button>
                    </div>
                    <div class="title-price-tour mt-30 text-center">
                        <h3>HOTLINE TƯ VẤN & ĐẶT TOUR</h3>
                        <span class="mt-15 text-center">
                        <a href="tel:0933467875">
                            <span class="glyphicon glyphicon-phone mt-15" aria-hidden="true" style="font-size: 18px; color: red"></span><span style="font-size: 18px; color: red; font-weight: bold">0933467875</span>
                        </a>
                    </span>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="container n3-tour-detail">
        <div class="row">
            <div class="col-lg-8 col-md-9 col-sm-8 col-xs-12">
                <div class="sec-info tab-content">
                    <div class="chuongtrinhtour mg-bot30 tab-pane fade in active" id="chuongtrinhtour_id">
                        <div class="title-lg"><h2>Chương tr&#236;nh tour</h2></div>
                        <div style="line-height: 20px; text-align: justify;padding:20px 20px 0px 20px">

                        </div>
                        <div class="sec-content itinerary">
                            <div class="list">
                                <div class="list__item">
                                    <div class="list__time">
                                        <div class="num">01.</div>
                                        <div class="day">31-08-2019</div>
                                    </div>
                                    <div class="list__border"></div>
                                    <div class="list__desc">
                                        <h3 class="font500 name"><img src="{{asset('')}}layouts/images/i-marker.png" alt="marker">&nbsp;&nbsp;&nbsp;TP.HCM – CH&#194;U ĐỐC – MIẾU B&#192; CH&#218;A XỨ - T&#194;Y AN CỔ TỰ - LĂNG THOẠI NGỌC HẦU (Ăn s&#225;ng, trưa, chiều)</h3>
                                        <div class="d1 detail">
                                            <html>
                                            <head>
                                                <title></title>
                                            </head>
                                            <body>
                                            <div style="text-align: justify;">
                                                Quý khách tập trung tại <a href="https://www.vietravel.com" target="_blank">Vietravel</a>, HDV hướng dẫn khách lên xe và  khởi hành đi <strong>Tiền Giang</strong>, dùng bữa sáng tại <strong>Mekong Rest Stop</strong> trên đường sẽ đi ngang : </div>
                                            <div style="text-align: justify;">
                                                       -<span class="Apple-tab-span" style="white-space:pre"> </span><strong>Cầu Mỹ Thuận</strong> : Cây cầu treo đầu tiên tại Việt Nam, niềm tự hào của người dân đồng bằng sông Cửu Long.</div>
                                            <div style="text-align: justify;">
                                                Đến <a href="https://travel.com.vn/an-giang/tour-chau-doc.aspx" target="_blank">Châu Đốc</a> : </div>
                                            <div style="text-align: justify;">
                                                       -<span class="Apple-tab-span" style="white-space:pre"> </span>Quý khách xuống thuyền khám phá nét đẹp sông nước miền Tây thăm <em><strong>Làng cá bè</strong></em> - làng nghề nuôi cá trên sông đặc trưng vùng sông nước nổi tiếng đất <a href="https://travel.com.vn/an-giang/tour-an-giang.aspx" target="_blank">An Giang</a>.</div>
                                            <div style="text-align: justify;">
                                                Buổi tối, xe đưa khách tham quan : </div>
                                            <div style="text-align: justify;">
                                                       -<span class="Apple-tab-span" style="white-space:pre"> </span><strong>Miếu Bà Chúa Xứ</strong>: công trình kiến trúc đẹp, tọa lạc dưới chân núi Sam, lễ hội vía Bà Chúa Xứ hàng năm rất lớn và được nhà nước công nhận lễ hội quốc gia.</div>
                                            <div style="text-align: justify;">
                                                      -<span class="Apple-tab-span" style="white-space:pre"> </span><strong>Tây An Cổ Tự</strong>: là ngôi chùa Phật Giáo có kiến trúc kết hợp phong cách nghệ thuật Ấn Độ và kiến trúc cổ dân tộc việt.</div>
                                            <div style="text-align: justify;">
                                                       -<span class="Apple-tab-span" style="white-space:pre"> </span><strong>Lăng Thoại Ngọc Hầu</strong> : khu danh thắng núi Sam, được công nhận là di tích lịch sử cấp quốc gia vào năm 1997.</div>
                                            <div style="text-align: justify;">
                                                Quý khách nghỉ đêm tại Châu Đốc.</div>
                                            </body>
                                            </html>

                                        </div>
                                        <div class="an-hien ah1">
                                            <div class="hienra">
                                                <a href="#"> Xem th&#234;m &nbsp;<i class="fas fa-arrow-down"></i></a>
                                            </div>
                                            <div class="andi" style="display: none;">
                                                <a href="#"> Ẩn đi &nbsp;<i class="fas fa-arrow-up"></i></a>
                                            </div>
                                        </div>
                                        <div class="border"></div>
                                    </div>
                                </div>
                                <div class="list__item">
                                    <div class="list__time">
                                        <div class="num">02.</div>
                                        <div class="day">01-09-2019</div>
                                    </div>
                                    <div class="list__border"></div>
                                    <div class="list__desc">
                                        <h3 class="font500 name"><img src="{{asset('')}}layouts/images/i-marker.png" alt="marker">&nbsp;&nbsp;&nbsp;CH&#194;U ĐỐC – RỪNG TR&#192;M TR&#192; SƯ – H&#192; TI&#202;N – MŨI NAI (Ăn s&#225;ng, trưa, chiều)</h3>
                                        <div class="d2 detail">
                                            <html>
                                            <head>
                                                <title></title>
                                            </head>
                                            <body>
                                            <div style="text-align: justify;">
                                                Sau khi dùng bữa sáng, Quý khách tham quan : </div>
                                            <div style="text-align: justify;">
                                                       -<span class="Apple-tab-span" style="white-space:pre"> </span><strong><a href="https://travel.com.vn/an-giang/tour-rung-tram-tra-su.aspx" target="_blank">Rừng Tràm Trà Sư</a></strong>: Khách di chuyển bằng xuồng máy hay những chiếc ghe để len lỏi giữa các lối đi nhỏ xuyên qua khu rừng, tham quan phong cảnh thiên nhiên hoang sơ và hệ sinh thái đa dạng với nhiều loại động, thực vật hoang dã.</div>
                                            <div style="text-align: justify;">
                                                Quý khách tiếp tục hành trình đi <a href="https://travel.com.vn/ha-tien/tour-ha-tien.aspx" target="_blank">Hà Tiên</a>, nhận phòng khách sạn và nghỉ ngơi.</div>
                                            <div style="text-align: justify;">
                                                Buổi chiều, quý khách tiếp tục tham quan :</div>
                                            <div style="text-align: justify;">
                                                      -<span class="Apple-tab-span" style="white-space:pre"> </span><strong>Lăng Mạc Cửu</strong>: nơi thờ dòng họ Mạc mà khởi đầu là ông Mạc Cửu, người đã có công khai phá mảnh đất Hà Tiên.</div>
                                            <div style="text-align: justify;">
                                                      -<span class="Apple-tab-span" style="white-space:pre"> </span><strong>Viếng chùa Phù Dung</strong>: tọa lạc dưới chân núi Bình San, hấp dẫn du khách bởi vẻ đẹp cổ kính, hài hòa với thiên nhiên và những câu chuyện bí ẩn về ngôi chùa.</div>
                                            <div style="text-align: justify;">
                                                      -<span class="Apple-tab-span" style="white-space:pre"> </span><strong>Biển Mũi Nai</strong>: Quý khách tự do thưởng thức hải sản địa phương và khi những tia nắng cuối ngày dần tắt Mũi Nai trong ánh hoàng hôn của biển tây nam sẽ để lại cho Quý khách những cảm xúc khó quên. </div>
                                            <div style="text-align: justify;">
                                                Nghỉ đêm tại Hà Tiên.</div>
                                            </body>
                                            </html>

                                        </div>
                                        <div class="an-hien ah2">
                                            <div class="hienra">
                                                <a href="#"> Xem th&#234;m &nbsp;<i class="fas fa-arrow-down"></i></a>
                                            </div>
                                            <div class="andi" style="display: none;">
                                                <a href="#"> Ẩn đi &nbsp;<i class="fas fa-arrow-up"></i></a>
                                            </div>
                                        </div>
                                        <div class="border"></div>
                                    </div>
                                </div>
                                <div class="list__item">
                                    <div class="list__time">
                                        <div class="num">03.</div>
                                        <div class="day">02-09-2019</div>
                                    </div>
                                    <div class="list__border"></div>
                                    <div class="list__desc">
                                        <h3 class="font500 name"><img src="{{asset('')}}layouts/images/i-marker.png" alt="marker">&nbsp;&nbsp;&nbsp;H&#192; TI&#202;N – H&#210;N PHU TỬ - CH&#217;A HANG – RẠCH GI&#193; – CẦN THƠ (Ăn s&#225;ng, trưa, chiều)</h3>
                                        <div class="d3 detail">
                                            <html>
                                            <head>
                                                <title></title>
                                            </head>
                                            <body>
                                            <div style="text-align: justify;">
                                                Buổi sáng , Quý khách khởi hành đi Kiên Lương tham quan : </div>
                                            <div style="text-align: justify;">
                                                       -<span class="Apple-tab-span" style="white-space:pre"> </span><strong>Chùa Hang, Hòn Phu Tử</strong> : Một biểu tượng đẹp của tỉnh Kiên Giang với một truyền thuyết đẹp về tình cha con.</div>
                                            <div style="text-align: justify;">
                                                Đoàn tiếp tục khởi hành đi <a href="https://travel.com.vn/kien-giang/tour-rach-gia.aspx" target="_blank">Rạch Giá</a> tham quan : </div>
                                            <div style="text-align: justify;">
                                                      -<span class="Apple-tab-span" style="white-space:pre"> </span><strong>Đền Thờ Nguyễn Trung Trực</strong>:  Đây là ngôi đình thờ ông sớm nhất và lớn nhất trong 9 đền thờ ở Kiên Giang.</div>
                                            <div style="text-align: justify;">
                                                Rời Rạch Giá, đoàn tiếp tục khởi hành đi <a href="https://travel.com.vn/can-tho/tour-can-tho.aspx" target="_blank">Cần Thơ</a>, về khách sạn, nhận phòng và nghỉ ngơi.</div>
                                            <div style="text-align: justify;">
                                                 Sau khi dùng bữa tối, Quý khách tự do tản bộ ra <strong>bến Ninh Kiều</strong>: </div>
                                            <div style="text-align: justify;">
                                                     -<span class="Apple-tab-span" style="white-space:pre"> </span>Thưởng thức ly cà phê trong ngọn gió sông Hậu mát lạnh, hay dạo bước trên chiếc cầu đi bộ Ninh Kiều-được bắc qua rạch Khai Luông nối bến Ninh Kiều và cồn Cái Khế, trên cầu còn có hai đài sen và hệ thống đèn led màu tạo nên vẻ đẹp sinh động và hiện đại.</div>
                                            <div style="text-align: justify;">
                                                Nghỉ đêm tại Cần Thơ.</div>
                                            </body>
                                            </html>

                                        </div>
                                        <div class="an-hien ah3">
                                            <div class="hienra">
                                                <a href="#"> Xem th&#234;m &nbsp;<i class="fas fa-arrow-down"></i></a>
                                            </div>
                                            <div class="andi" style="display: none;">
                                                <a href="#"> Ẩn đi &nbsp;<i class="fas fa-arrow-up"></i></a>
                                            </div>
                                        </div>
                                        <div class="border"></div>
                                    </div>
                                </div>
                                <div class="list__item">
                                    <div class="list__time">
                                        <div class="num">04.</div>
                                        <div class="day">03-09-2019</div>
                                    </div>
                                    <div class="list__border"></div>
                                    <div class="list__desc">
                                        <h3 class="font500 name"><img src="{{asset('')}}layouts/images/i-marker.png" alt="marker">&nbsp;&nbsp;&nbsp;CẦN THƠ – CHỢ NỔI C&#193;I RĂNG – TP.HCM (Ăn s&#225;ng, trưa)</h3>
                                        <div class="d4 detail">
                                            <html>
                                            <head>
                                                <title></title>
                                            </head>
                                            <body>
                                            <div>
                                                <div style="text-align: justify;">
                                                    Sau khi dùng bữa sáng, xe đưa Quý khách tham quan Bến Ninh Kiều : </div>
                                                <div style="text-align: justify;">
                                                          -<span class="Apple-tab-span" style="white-space:pre"> </span>Xuống thuyền du ngoạn trên <strong>sông <a href="https://travel.com.vn/can-tho/tour-can-tho.aspx" target="_blank">Cần Thơ</a></strong>, ngắm <strong>cầu Quang Trung</strong>, đến <strong>chợ nổi Cái Răng</strong> - Một hình thức họp chợ đặc trưng cùa vùng Đồng Bằng Sông Cửu Long, Quý khách tự do thưởng thức và chọn mua đặc sản Miền Tây (chi phí tự túc).</div>
                                                <div style="text-align: justify;">
                                                         -<span class="Apple-tab-span" style="white-space:pre"> </span><strong>Thiền Viện Phương Nam</strong>: Quý khách sẽ ấn tượng với nội thất bằng gỗ lim và các loại gỗ quý được mang về từ Nam Phi, đặc biệt là bộ tượng phật được làm từ gỗ hai cây tùng quý hiếm với đường kính hơn 2m chiều ngang.</div>
                                                <div style="text-align: justify;">
                                                          -<span class="Apple-tab-span" style="white-space: pre;"> </span><strong>Khu <a href="https://travel.com.vn/" target="_blank">du lịch</a> Mỹ Khánh</strong> : Điểm du lịch sinh thái hấp dẫn với các trò chơi dân gian vui nhộn: <em>câu cá, chèo thuyền trên sông, đua heo…</em> tham quan <strong>Nhà Cổ</strong>.</div>
                                                <div style="text-align: justify;">
                                                    Đoàn khởi hành về TP.HCM, chia tay quý khách, kết thúc chương trình.</div>
                                            </div>
                                            </body>
                                            </html>

                                        </div>
                                        <div class="an-hien ah4">
                                            <div class="hienra">
                                                <a href="#"> Xem th&#234;m &nbsp;<i class="fas fa-arrow-down"></i></a>
                                            </div>
                                            <div class="andi" style="display: none;">
                                                <a href="#"> Ẩn đi &nbsp;<i class="fas fa-arrow-up"></i></a>
                                            </div>
                                        </div>
                                        <div class="border"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="destination">
                                <div class="title"><img src="{{asset('')}}layouts/images/i-des.png" alt="destination">&nbsp;&nbsp;Nơi đến</div>
                                <div class="row">
                                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 mg-bot30">
                                        <div class="pos-relative">
                                            <a href="/mien-tay/tour-chau-doc.aspx" target="_blank">
                                                <img src="{{asset('')}}layouts/images/dc_150511_{{asset('')}}layouts/images (2).jpg" alt="Ch&#226;u Đốc" class="img-responsive pic-nd">
                                                <div class="frame-item">
                                                    <p class="des-name dot-dot cut-name">Ch&#226;u Đốc</p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 mg-bot30">
                                        <div class="pos-relative">
                                            <a href="/mien-tay/tour-mieu-ba-chau-doc.aspx" target="_blank">
                                                <img src="{{asset('')}}layouts/images/dc_150623_mieu ba.jpg" alt="Miếu B&#224; Ch&#226;u Đốc" class="img-responsive pic-nd">
                                                <div class="frame-item">
                                                    <p class="des-name dot-dot cut-name">Miếu B&#224; Ch&#226;u Đốc</p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 mg-bot30">
                                        <div class="pos-relative">
                                            <a href="/mien-tay/tour-rung-tram-tra-su.aspx" target="_blank">
                                                <img src="{{asset('')}}layouts/images/dc_150623_tra su.jpg" alt="Rừng Tr&#224;m Tr&#224; Sư" class="img-responsive pic-nd">
                                                <div class="frame-item">
                                                    <p class="des-name dot-dot cut-name">Rừng Tr&#224;m Tr&#224; Sư</p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 mg-bot30">
                                        <div class="pos-relative">
                                            <a href="/mien-tay/tour-ha-tien.aspx" target="_blank">
                                                <img src="{{asset('')}}layouts/images/dc_150917_ha-tien.jpg" alt="H&#224; Ti&#234;n" class="img-responsive pic-nd">
                                                <div class="frame-item">
                                                    <p class="des-name dot-dot cut-name">H&#224; Ti&#234;n</p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 mg-bot30">
                                        <div class="pos-relative">
                                            <a href="/mien-tay/tour-rach-gia.aspx" target="_blank">
                                                <img src="{{asset('')}}layouts/images/dc_150622_rach gia.jpg" alt="Rạch Gi&#225;" class="img-responsive pic-nd">
                                                <div class="frame-item">
                                                    <p class="des-name dot-dot cut-name">Rạch Gi&#225;</p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 mg-bot30">
                                        <div class="pos-relative">
                                            <a href="/mien-tay/tour-can-tho.aspx" target="_blank">
                                                <img src="{{asset('')}}layouts/images/dc_150506_{{asset('')}}layouts/images (3).jpg" alt="Cần Thơ" class="img-responsive pic-nd">
                                                <div class="frame-item">
                                                    <p class="des-name dot-dot cut-name">Cần Thơ</p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="travel360">
                                <div class="title"><img src="{{asset('')}}layouts/images/i-des.png" alt="des">&nbsp;&nbsp;Trải nghiệm du lịch với h&#236;nh ảnh 360</div>
                                <div class="mg-bot20">
                                    <ul class="row list-travel360">
                                        <li class="col-lg-3 col-md-4 col-sm-6">
                                            <a class="hidden-sm hidden-xs" onclick="LoadImage360('Bến Ninh Kiều','https://vr360.vietravel.net/vietnam/can-tho/ben-ninh-kieu/')" title="Bến Ninh Kiều" data-toggle="modal" data-target="#div360Img">Bến Ninh Kiều</a>
                                            <a class="hidden-lg hidden-md" title="Bến Ninh Kiều">Bến Ninh Kiều</a>
                                        </li>
                                        <li class="col-lg-3 col-md-4 col-sm-6">
                                            <a class="hidden-sm hidden-xs" onclick="LoadImage360('Chợ Nổi C&#225;i Răng','https://vr360.vietravel.net/vietnam/can-tho/cho-noi-cai-rang/')" title="Chợ Nổi C&#225;i Răng" data-toggle="modal" data-target="#div360Img">Chợ Nổi C&#225;i Răng</a>
                                            <a class="hidden-lg hidden-md" title="Chợ Nổi C&#225;i Răng">Chợ Nổi C&#225;i Răng</a>
                                        </li>
                                        <li class="col-lg-3 col-md-4 col-sm-6">
                                            <a class="hidden-sm hidden-xs" onclick="LoadImage360('Rừng Tr&#224;m Tr&#224; Sư','https://vr360.vietravel.net/vietnam/an-giang/rung-tram-tra-su/')" title="Rừng Tr&#224;m Tr&#224; Sư" data-toggle="modal" data-target="#div360Img">Rừng Tr&#224;m Tr&#224; Sư</a>
                                            <a class="hidden-lg hidden-md" title="Rừng Tr&#224;m Tr&#224; Sư">Rừng Tr&#224;m Tr&#224; Sư</a>
                                        </li>
                                        <li class="col-lg-3 col-md-4 col-sm-6">
                                            <a class="hidden-sm hidden-xs" onclick="LoadImage360('Miếu B&#224; Ch&#250;a Xứ','https://vr360.vietravel.net/vietnam/an-giang/mieu-ba-chua-xu/')" title="Miếu B&#224; Ch&#250;a Xứ" data-toggle="modal" data-target="#div360Img">Miếu B&#224; Ch&#250;a Xứ</a>
                                            <a class="hidden-lg hidden-md" title="Miếu B&#224; Ch&#250;a Xứ">Miếu B&#224; Ch&#250;a Xứ</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="chitiettour mg-bot30 tab-pane fade" id="chitiettour_id">
                        <div class="title-lg"><h2>Chi tiết tour</h2></div>
                        <div class="sec-content tour-detail">
                            <div id="divThongTinVanChuyen"></div>
                            <div id="divThongTinKhachSan">
                            </div>

                            <div id="divThongTinHDV">
                            </div>
                            <div id="divThongTinHDVTien"></div>
                            <div class="info">
                                <div class="title">
                                    <span class="hidden-xs"><img src="{{asset('')}}layouts/images/i-info.png" alt="info">&nbsp;&nbsp;</span>
                                    Th&#244;ng tin tập trung
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <table class="table table-bordered">
                                            <tbody>
                                            <tr>
                                                <td>Ng&#224;y giờ tập trung</td>
                                                <td>
                                                    31/08/2019 05:00
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Nơi tập trung
                                                </td>
                                                <td>
                                                    <span> Vietravel </span>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="tour-price">
                                <div class="title">
                                    <span class="hidden-xs"><img src="{{asset('')}}layouts/images/i-tourprice.png" alt="tourprice">&nbsp;&nbsp;</span>
                                    Gi&#225; tour &amp; Phụ thu ph&#242;ng đơn
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr class="bold">
                                                <td>Loại kh&#225;ch</td>
                                                <td>Gi&#225; tour</td>
                                                <td>Land tour</td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td data-title="Loại kh&#225;ch">Người lớn (Từ 12 tuổi trở l&#234;n)</td>
                                                <td data-title="Gi&#225; tour">3,290,000<span> đ</span></td>
                                                <td data-title="Land tour">0 đ</td>
                                            </tr>
                                            <tr>
                                                <td data-title="Loại kh&#225;ch">Trẻ em (Từ 5 tuổi đến dưới 12 tuổi)</td>
                                                <td data-title="Gi&#225; tour">1,645,000<span> đ</span></td>
                                                <td data-title="Land tour">0 đ</td>
                                            </tr>
                                            <tr>
                                                <td data-title="Loại kh&#225;ch">Trẻ nhỏ (Từ 2 tuổi đến dưới 5 tuổi)</td>
                                                <td data-title="Gi&#225; tour">0<span> đ</span></td>
                                                <td data-title="Land tour">0 đ</td>
                                            </tr>
                                            <tr>
                                                <td data-title="Loại kh&#225;ch">Em b&#233; (Dưới 2 tuổi)</td>
                                                <td data-title="Gi&#225; tour">0 đ</td>
                                                <td data-title="Land tour">0 đ</td>
                                            </tr>
                                            <tr style="color: #cd2c24">
                                                <td data-title="Loại kh&#225;ch">Phụ thu ph&#242;ng đơn</td>
                                                <td data-title="Gi&#225; tour">1,000,000<span> đ</span></td>
                                                <td data-title="Land tour">0 đ</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="note">
                                <div class="title">
                                <span class="hidden-xs">
                                    <img src="{{asset('')}}layouts/images/i-note.png" alt="note">&nbsp;&nbsp;
                                </span>
                                    Ghi ch&#250;
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <p class="note-content">
                                            Li&#234;n hệ tổng đ&#224;i 1900 1839 từ 08:00 - 23:00                                    </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ykien mg-bot30 tab-pane fade" id="tab-ykien">
                        <!--Ý kiến khách hàng-->

                        <div class="title-lg">&#221; KIẾN KH&#193;CH H&#192;NG (0)</div>
                        <div class="sec-content customer-idea">
                            <form action="/Comment/Reply" id="mainform" method="post">
                                <input name="__RequestVerificationToken" type="hidden" value="niWw5_n5UOWGzUVhsEsED4R-jXihzTAJZQ-TKetC1X1vz3_gn7xNfFjYdQk0XDbhqs9uuOL4lGjkZYfn4beoPB6etpmXCeJCXW3d2VWRiSs1">
                                <input id="HeaderTour" name="HeaderTour" type="hidden" value="NDSGN847">
                                <div class="frame-feedback">
                                    <div class="title">
                                        <img src="{{asset('')}}layouts/images/i-feedback.png" alt="feedback">&nbsp;&nbsp;Gửi &#253; kiến
                                    </div>
                                    <div class="form-feedback">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-12 col-xs-12 mg-bot15">
                                                <input class="form-control input-md" id="UserName" name="UserName" placeholder="Họ tên(*)" required="required" type="text" value="">
                                            </div>
                                            <div class="col-md-6 col-sm-12 col-xs-12 mg-bot15">
                                                <input class="form-control input-md" id="Email" name="Email" placeholder="Email(*)" required="required" type="email" value="">
                                            </div>
                                        </div>
                                        <div class="row mg-bot15">
                                            <div class="col-xs-12">
                        <textarea class="form-control" cols="20" id="Content" name="Content" placeholder="Ý kiến của bạn(*)" required="required" rows="5">
</textarea>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-xs-12 text-center mg-bot15">
                                                <button type="submit" class="btn btn-md btn-detail" id="btn_submitmainform">Gửi đi&nbsp;&nbsp;<i class="fas fa-paper-plane"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </form>
                            <div class="clear"></div>
                        </div>

                    </div>

                    <div class="lienhe mg-bot30 tab-pane fade" id="tab-lienhe">
                        <div class="title-lg"><h2>Li&#234;n hệ</h2></div>
                        <div class="sec-content contact">
                            <div class="row vpc">
                                <div class="col-xs-12 title">
                                    Trụ sở ch&#237;nh
                                </div>
                                <div class="col-sm-6 col-xs-12 mg-bot30">
                                    <p class="address mg-bot10"><strong>Địa chỉ: </strong> 190 Pasteur, Phường 6, Quận 3, Tp. Hồ Chí Minh</p>
                                    <p class="mg-bot10"><strong>Điện thoại:</strong> (84-28) 3822 8898</p>
                                    <p class="mg-bot10"><strong>Fax:</strong> (84-28) 3829 9142</p>
                                    <p><strong>Email:</strong> info@vietravel.com</p>
                                </div>
                            </div>
                            <div class="row cn">
                                <div class="col-xs-12 mg-bot30">
                                    <ul class="list-mien list-inline">
                                        <li class="active item-mn" id="tabMienNam"><a href="#">Miền Nam</a></li>
                                        <li class="item-mb"><a href="#" id="tabMienBac">Miền Bắc</a></li>
                                        <li class="item-mt"><a href="#" id="tabMienTrung">Miền Trung</a></li>
                                    </ul>
                                </div>
                                <div class="mien miennam" id="divMienNam">
                                </div>
                                <div class="mien mienbac" id="divMienBac">
                                </div>
                                <div class="mien mientrung" id="divMienTrung">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-3 col-sm-4 col-xs-12">
                <div class="menu-left mg-bot30">
                    <div class="panel panel-default panel-side-menu">
                        <div class="panel-body panel-body-nav">
                            <div class="side-menu">
                                <ul class="list-unstyled">
                                    <li id="cct" class="tab-chuongtrinhtour active">
                                        <a data-toggle="tab" href="#chuongtrinhtour_id">
                                            <i class="fas fa-spinner"></i>
                                            <span>Chương trình tour</span>
                                        </a>
                                    </li>
                                    <li class="tab-chitiettour" id="tabChiTiet">
                                        <a data-toggle="tab" href="#chitiettour_id">
                                            <i class="fas fa-list"></i>
                                            <span>Lịch trình tour</span>
                                        </a>
                                    </li>
                                    <li class="tab-lienhe" id="tabLienHe">
                                        <a data-toggle="tab" href="#tab-lienhe">
                                            <i class="fas fa-podcast"></i>
                                            <span>Ghi chú</span>
                                        </a>
                                    </li>

                                    <li class="tab-ykien">
                                        <a data-toggle="tab" href="#tab-ykien">
                                            <i class="far fa-comments"></i>
                                            <span>Ý kiến khách hàng</span>
                                        </a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="line-ct"></div>
            </div>
            <div class="col-xs-12 tour-similar">
                <div class="row">
                    <div class="col-xs-12 mg-bot20">
                        <div class="title-ft">Các tour liên quan</div>
                    </div>
                    <div id="theogia">
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 mg-bot30">
                            <a href="tour_info.html" title="Ninh Chữ - Vườn Nho - Đảo Tôm Hùm Bình Hưng (Resort Tương Đương 4*. Tour Tiết Kiệm)">
                                <div class="pos-relative">
                                    <img src="https://travel.com.vn/Images/destination/tf_160909092754_635463.jpg" class="img-responsive pic-ttt" alt="Ninh Chữ - Vườn Nho - Đảo Tôm Hùm Bình Hưng (Resort Tương Đương 4*. Tour Tiết Kiệm)">
                                    <div class="frame-ttt1">
                                        <div class="f-left">
                                            <img src="{{asset('')}}layouts/images/i-date-w.png" alt="date">
                                        </div>
                                        <div class="f-left date">
                                            <span class="yellow">06/09/2019</span> -
                                            <span class="yellow">3 ngày</span> - <span class="yellow">3 chỗ</span></div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </a>
                            <div class="frame-ttt2">
                                <a href="" title="Ninh Chữ - Vườn Nho - Đảo Tôm Hùm Bình Hưng (Resort Tương Đương 4*. Tour Tiết Kiệm)">
                                    <div class="ttt-title dot-dot cut-ttt">Ninh Chữ - Vườn Nho - Đảo Tôm  ... </div>
                                </a>
                                <div class="ttt-line"></div>
                                <div>
                                    <img src="{{asset('')}}layouts/images/i-price.png" class="f-left" alt="price">
                                    <div class="f-left ttt-info">

                                        <span class="price-n">3,390,000 đ</span>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 mg-bot30">
                            <a href="tour_info.html" title="Phan Thiết - Mũi Né - Bàu Trắng (Tặng vé Fishermen Show. Resort 3*. Tour Tiết kiệm)">
                                <div class="pos-relative">
                                    <img src="https://travel.com.vn/Images/destination/tf_171023095344_184469.jpeg" class="img-responsive pic-ttt" alt="Phan Thiết - Mũi Né - Bàu Trắng (Tặng vé Fishermen Show. Resort 3*. Tour Tiết kiệm)">
                                    <div class="frame-ttt1">
                                        <div class="f-left">
                                            <img src="{{asset('')}}layouts/images/i-date-w.png" alt="date">
                                        </div>
                                        <div class="f-left date">
                                            <span class="yellow">24/09/2019</span> -
                                            <span class="yellow">3 ngày</span> - <span class="yellow">9 chỗ</span></div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </a>
                            <div class="frame-ttt2">
                                <a href="" title="Phan Thiết - Mũi Né - Bàu Trắng (Tặng vé Fishermen Show. Resort 3*. Tour Tiết kiệm)">
                                    <div class="ttt-title dot-dot cut-ttt">Phan Thiết - Mũi Né - Bàu Trắn ... </div>
                                </a>
                                <div class="ttt-line"></div>
                                <div>
                                    <img src="{{asset('')}}layouts/images/i-price.png" class="f-left" alt="price">
                                    <div class="f-left ttt-info">

                                        <span class="price-n">3,190,000 đ</span>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 mg-bot30">
                            <a href="tour_info.html" title="Miền Tây - Sa Đéc - Cần Thơ - Cà Mau - Bạc Liêu - Sóc Trăng (khách sạn 3* và 4*, Tour Tiêu Chuẩn)">
                                <div class="pos-relative">
                                    <img src="https://travel.com.vn/Images/destination/tf_180105030136_386697.jpg" class="img-responsive pic-ttt" alt="Miền Tây - Sa Đéc - Cần Thơ - Cà Mau - Bạc Liêu - Sóc Trăng (khách sạn 3* và 4*, Tour Tiêu Chuẩn)">
                                    <div class="frame-ttt1">
                                        <div class="f-left">
                                            <img src="{{asset('')}}layouts/images/i-date-w.png" alt="date">
                                        </div>
                                        <div class="f-left date">
                                            <span class="yellow">14/12/2019</span> -
                                            <span class="yellow">4 ngày</span> - <span class="yellow">6 chỗ</span></div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </a>
                            <div class="frame-ttt2">
                                <a href="/tourNDSGN846-173-141219XE-D-1/mien-tay-sa-dec-can-tho-ca-mau-bac-lieu-soc-trang-khach-san-3sao-va-4sao,-tour-tieu-chuan.aspx" title="Miền Tây - Sa Đéc - Cần Thơ - Cà Mau - Bạc Liêu - Sóc Trăng (khách sạn 3* và 4*, Tour Tiêu Chuẩn)">
                                    <div class="ttt-title dot-dot cut-ttt">Miền Tây - Sa Đéc - Cần Thơ -  ... </div>
                                </a>
                                <div class="ttt-line"></div>
                                <div>
                                    <img src="{{asset('')}}layouts/images/i-price.png" class="f-left" alt="price">
                                    <div class="f-left ttt-info">

                                        <span class="price-n">3,490,000 đ</span>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 mg-bot30">
                            <a href="tour_info.html" title="Miền Tây - Châu Đốc - Rừng Tràm Trà Sư - Hà Tiên - Rạch Giá - Cần Thơ (Khách sạn 2*&amp; 3*, Tour Tiết Kiệm)">
                                <div class="pos-relative">
                                    <img src="https://travel.com.vn/Images/destination/tf_171122084522_719161.jpg" class="img-responsive pic-ttt" alt="Miền Tây - Châu Đốc - Rừng Tràm Trà Sư - Hà Tiên - Rạch Giá - Cần Thơ (Khách sạn 2*&amp; 3*, Tour Tiết Kiệm)">
                                    <div class="frame-ttt1">
                                        <div class="f-left">
                                            <img src="{{asset('')}}layouts/images/i-date-w.png" alt="date">
                                        </div>
                                        <div class="f-left date">
                                            <span class="yellow">14/12/2019</span> -
                                            <span class="yellow">4 ngày</span> - <span class="yellow">5 chỗ</span></div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </a>
                            <div class="frame-ttt2">
                                <a href="/tourNDSGN847-046-141219XE-V/mien-tay-chau-doc-rung-tram-tra-su-ha-tien-rach-gia-can-tho-khach-san-2sao-3sao,-tour-tiet-kiem.aspx" title="Miền Tây - Châu Đốc - Rừng Tràm Trà Sư - Hà Tiên - Rạch Giá - Cần Thơ (Khách sạn 2*&amp; 3*, Tour Tiết Kiệm)">
                                    <div class="ttt-title dot-dot cut-ttt">Miền Tây - Châu Đốc - Rừng Trà ... </div>
                                </a>
                                <div class="ttt-line"></div>
                                <div>
                                    <img src="{{asset('')}}layouts/images/i-price.png" class="f-left" alt="price">
                                    <div class="f-left ttt-info">

                                        <span class="price-n">3,290,000 đ</span>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 mg-bot30">
                            <a href="tour_info.html" title="Ninh Chữ - Vườn Nho - Đảo Tôm Hùm Bình Hưng (Resort Tương Đương 4*. Tour Tiết Kiệm)">
                                <div class="pos-relative">
                                    <img src="https://travel.com.vn/Images/destination/tf_160909092754_635463.jpg" class="img-responsive pic-ttt" alt="Ninh Chữ - Vườn Nho - Đảo Tôm Hùm Bình Hưng (Resort Tương Đương 4*. Tour Tiết Kiệm)">
                                    <div class="frame-ttt1">
                                        <div class="f-left">
                                            <img src="{{asset('')}}layouts/images/i-date-w.png" alt="date">
                                        </div>
                                        <div class="f-left date">
                                            <span class="yellow">06/09/2019</span> -
                                            <span class="yellow">3 ngày</span> - <span class="yellow">3 chỗ</span></div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </a>
                            <div class="frame-ttt2">
                                <a href="" title="Ninh Chữ - Vườn Nho - Đảo Tôm Hùm Bình Hưng (Resort Tương Đương 4*. Tour Tiết Kiệm)">
                                    <div class="ttt-title dot-dot cut-ttt">Ninh Chữ - Vườn Nho - Đảo Tôm  ... </div>
                                </a>
                                <div class="ttt-line"></div>
                                <div>
                                    <img src="{{asset('')}}layouts/images/i-price.png" class="f-left" alt="price">
                                    <div class="f-left ttt-info">

                                        <span class="price-n">3,390,000 đ</span>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 mg-bot30">
                            <a href="tour_info.html" title="Phan Thiết - Mũi Né - Bàu Trắng (Tặng vé Fishermen Show. Resort 3*. Tour Tiết kiệm)">
                                <div class="pos-relative">
                                    <img src="https://travel.com.vn/Images/destination/tf_171023095344_184469.jpeg" class="img-responsive pic-ttt" alt="Phan Thiết - Mũi Né - Bàu Trắng (Tặng vé Fishermen Show. Resort 3*. Tour Tiết kiệm)">
                                    <div class="frame-ttt1">
                                        <div class="f-left">
                                            <img src="{{asset('')}}layouts/images/i-date-w.png" alt="date">
                                        </div>
                                        <div class="f-left date">
                                            <span class="yellow">24/09/2019</span> -
                                            <span class="yellow">3 ngày</span> - <span class="yellow">9 chỗ</span></div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </a>
                            <div class="frame-ttt2">
                                <a href="" title="Phan Thiết - Mũi Né - Bàu Trắng (Tặng vé Fishermen Show. Resort 3*. Tour Tiết kiệm)">
                                    <div class="ttt-title dot-dot cut-ttt">Phan Thiết - Mũi Né - Bàu Trắn ... </div>
                                </a>
                                <div class="ttt-line"></div>
                                <div>
                                    <img src="{{asset('')}}layouts/images/i-price.png" class="f-left" alt="price">
                                    <div class="f-left ttt-info">

                                        <span class="price-n">3,190,000 đ</span>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 mg-bot30">
                            <a href="tour_info.html" title="Miền Tây - Sa Đéc - Cần Thơ - Cà Mau - Bạc Liêu - Sóc Trăng (khách sạn 3* và 4*, Tour Tiêu Chuẩn)">
                                <div class="pos-relative">
                                    <img src="https://travel.com.vn/Images/destination/tf_180105030136_386697.jpg" class="img-responsive pic-ttt" alt="Miền Tây - Sa Đéc - Cần Thơ - Cà Mau - Bạc Liêu - Sóc Trăng (khách sạn 3* và 4*, Tour Tiêu Chuẩn)">
                                    <div class="frame-ttt1">
                                        <div class="f-left">
                                            <img src="{{asset('')}}layouts/images/i-date-w.png" alt="date">
                                        </div>
                                        <div class="f-left date">
                                            <span class="yellow">14/12/2019</span> -
                                            <span class="yellow">4 ngày</span> - <span class="yellow">6 chỗ</span></div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </a>
                            <div class="frame-ttt2">
                                <a href="/tourNDSGN846-173-141219XE-D-1/mien-tay-sa-dec-can-tho-ca-mau-bac-lieu-soc-trang-khach-san-3sao-va-4sao,-tour-tieu-chuan.aspx" title="Miền Tây - Sa Đéc - Cần Thơ - Cà Mau - Bạc Liêu - Sóc Trăng (khách sạn 3* và 4*, Tour Tiêu Chuẩn)">
                                    <div class="ttt-title dot-dot cut-ttt">Miền Tây - Sa Đéc - Cần Thơ -  ... </div>
                                </a>
                                <div class="ttt-line"></div>
                                <div>
                                    <img src="{{asset('')}}layouts/images/i-price.png" class="f-left" alt="price">
                                    <div class="f-left ttt-info">

                                        <span class="price-n">3,490,000 đ</span>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 mg-bot30">
                            <a href="tour_info.html" title="Miền Tây - Châu Đốc - Rừng Tràm Trà Sư - Hà Tiên - Rạch Giá - Cần Thơ (Khách sạn 2*&amp; 3*, Tour Tiết Kiệm)">
                                <div class="pos-relative">
                                    <img src="https://travel.com.vn/Images/destination/tf_171122084522_719161.jpg" class="img-responsive pic-ttt" alt="Miền Tây - Châu Đốc - Rừng Tràm Trà Sư - Hà Tiên - Rạch Giá - Cần Thơ (Khách sạn 2*&amp; 3*, Tour Tiết Kiệm)">
                                    <div class="frame-ttt1">
                                        <div class="f-left">
                                            <img src="{{asset('')}}layouts/images/i-date-w.png" alt="date">
                                        </div>
                                        <div class="f-left date">
                                            <span class="yellow">14/12/2019</span> -
                                            <span class="yellow">4 ngày</span> - <span class="yellow">5 chỗ</span></div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </a>
                            <div class="frame-ttt2">
                                <a href="/tourNDSGN847-046-141219XE-V/mien-tay-chau-doc-rung-tram-tra-su-ha-tien-rach-gia-can-tho-khach-san-2sao-3sao,-tour-tiet-kiem.aspx" title="Miền Tây - Châu Đốc - Rừng Tràm Trà Sư - Hà Tiên - Rạch Giá - Cần Thơ (Khách sạn 2*&amp; 3*, Tour Tiết Kiệm)">
                                    <div class="ttt-title dot-dot cut-ttt">Miền Tây - Châu Đốc - Rừng Trà ... </div>
                                </a>
                                <div class="ttt-line"></div>
                                <div>
                                    <img src="{{asset('')}}layouts/images/i-price.png" class="f-left" alt="price">
                                    <div class="f-left ttt-info">

                                        <span class="price-n">3,290,000 đ</span>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>



</div>
@endsection
