@extends('frontend.layouts.master')
@section('content')
<div class="container">

    <!-- breadcrumb -->
    <div class="n3-breadcrumb mt-30">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="breadcrumb">
                        <span><a href="" itemprop="url"><span itemprop="title">Du lịch</span></a></span> »
                        <span><a href="" itemprop="url"><span itemprop="title">Du lịch trong nước</span></a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- new-bar -->
    <div class="container n3-list-tour mg-bot40">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 mg-bot30">
                <div class="new-bar">
                    <a href="tour.html"><div class="title">Tour trong nước ngoài</div></a>
                    <a href="tour_info.html">
                        <div class="new-hot">
                            <div class="img-new col-md-4">
                                <img src="{{asset('')}}layouts/images/tet-doc-lap-moc-chau.jpg" alt="" class="w-100">
                            </div>
                            <div class="text-new col-md-8">
                                Nha Trang - Đà Lạt (Tour Tiết Kiệm)
                            </div>
                        </div>
                    </a>
                    <a href="tour_info.html">
                        <div class="boder-dashed clearfix"></div>
                        <div class="new-hot">
                            <div class="img-new col-md-4">
                                <img src="{{asset('')}}layouts/images/mua-thu-phuong-bac_1.jpg" alt="" class="w-100">
                            </div>
                            <div class="text-new col-md-8">
                                Nha Trang - Đà Lạt (Tour Tiết Kiệm)
                            </div>
                        </div>
                    </a>
                    <a href="tour_info.html">
                        <div class="boder-dashed clearfix"></div>
                        <div class="new-hot">
                            <div class="img-new col-md-4">
                                <img src="{{asset('')}}layouts/images/net-thu-ha-thanh_1.jpg" alt="" class="w-100">
                            </div>
                            <div class="text-new col-md-8">
                                Nét thu Hà thành
                            </div>
                        </div>
                    </a>
                    <a href="tour_info.html">
                        <div class="boder-dashed clearfix"></div>
                        <div class="new-hot">
                            <div class="img-new col-md-4">
                                <img src="{{asset('')}}layouts/images/tet-doc-lap-moc-chau.jpg" alt="" class="w-100">
                            </div>
                            <div class="text-new col-md-8">
                                Đẹp dịu dàng thu vàng phương Bắc
                            </div>
                        </div>
                    </a>
                    <a href="tour_info.html">
                        <div class="boder-dashed clearfix"></div>
                        <div class="new-hot">
                            <div class="img-new col-md-4">
                                <img src="{{asset('')}}layouts/images/tet-doc-lap-moc-chau.jpg" alt="" class="w-100">
                            </div>
                            <div class="text-new col-md-8">
                                Nha Trang - Đà Lạt (Tour Tiết Kiệm)
                            </div>
                        </div>
                    </a>
                </div>

            </div>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="list-tour">
                    <div class="des-tour">
                        <h1 itemprop="name"><a href="tour.html">Du lịch trong nước</a></h1>
                        <div class="boder-dashed"></div>
                        <div class="clear"></div>
                        <div class="des-content">
                            <div class="mg-bot10">
                                <div class="mt-10">
                                    Du lịch trong nước luôn là lựa chọn tuyệt vời. Đường bờ biển dài hơn 3260km, những khu bảo tồn thiên nhiên tuyệt vời, những thành phố nhộn nhịp, những di tích lịch sử hào hùng, nền văn hóa độc đáo và hấp dẫn, cùng một danh sách dài những món ăn ngon nhất thế giới, Việt Nam có tất cả những điều đó. Với lịch trình dày, khởi hành đúng thời gian cam kết, Vietravel là công ty lữ hành uy tín nhất hiện nay tại Việt Nam, luôn sẵn sàng phục vụ du khách mọi lúc, mọi nơi, đảm bảo tính chuyên nghiệp và chất lượng dịch vụ tốt nhất thị trường
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="list">
                        <div class="row">
                            <!-- ToursHot -->
                            <div class="col-md-12 text-center list-tour-title">
                                <h2 class="title">
                                    <a href="tour.html">Danh sách tour du lịch trong nước</a>
                                </h2>
                            </div>

                            <!--list tour-->
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="item mg-bot30">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="tour-name">
                                                <a href="tour_info.html" title="Nha Trang -Đà Lạt (Tour Tiết Kiệm)"><h3>Phan Thiết - Mũi Né - Bàu Trắng (Tặng vé Fishermen Show. Resort tương đương 3*. Tour Tiết Kiệm) - Lễ 2/9</h3></a>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <div class="pos-relative">
                                                <a href="tour_info.html" title="Nha Trang - Đà Lạt (Tour Tiết Kiệm)"><img src="{{asset('')}}layouts/images/tf_171122084522_719161_1.jpg" class="img-responsive pic-lt" alt="Nha Trang - Đ&#224; Lạt (Tour Tiết Kiệm)"></a>
                                            </div>
                                        </div>
                                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                            <div class="frame-info">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6 col-xs-12 mg-bot10">
                                                        <div class="f-left l"><img src="{{asset('')}}layouts/images/i-code.png" alt="code"></div>
                                                        <div class="f-left r">NDSGN523-081-210819XE-V-O</div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12 mg-bot10">
                                                        <div class="f-left l"><img src="{{asset('')}}layouts/images/i-chair.png" alt="chair"></div>
                                                        <div class="f-left r">
                                                            Số chỗ còn nhận: <span class="font500">7</span>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12 mg-bot10">
                                                        <div class="f-left l"><img src="{{asset('')}}layouts/images/i-date.png" alt="date"></div>
                                                        <div class="f-left r">Ngày đi: <span class="font500">21/08/2019</span></div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12 mg-bot10">
                                                        <div class="f-left l"><img src="{{asset('')}}layouts/images/i-clock.png" alt="clock"></div>
                                                        <div class="f-left r">Ngày về: <span class="font500">24/08/2019</span></div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12 mg-bot10">
                                                        <div class="f-left l"><img src="{{asset('')}}layouts/images/i-price.png" alt="price"></div>
                                                        <div class="f-left r">
                                                            Giá: <span class="font500 price">4,390,000 đ</span>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <div class="f-left l"><img src="{{asset('')}}layouts/images/i-date.png" alt="date"></div>
                                                        <div class="f-left r">Số ngày: <span class="font500">5</span></div>
                                                        <div class="clear"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="item mg-bot30">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="tour-name">
                                                <a href="tour_info.html" title="Nha Trang -Đà Lạt (Tour Tiết Kiệm)"><h3>Nha Trang - Đà Lạt (Tour Tiết Kiệm)</h3></a>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <div class="pos-relative">
                                                <a href="tour_info.html" title="Nha Trang - Đà Lạt (Tour Tiết Kiệm)"><img src="{{asset('')}}layouts/images/tf_180404051952_514767.jpg" class="img-responsive pic-lt" alt="Nha Trang - Đ&#224; Lạt (Tour Tiết Kiệm)"></a>
                                            </div>
                                        </div>
                                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                            <div class="frame-info">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6 col-xs-12 mg-bot10">
                                                        <div class="f-left l"><img src="{{asset('')}}layouts/images/i-code.png" alt="code"></div>
                                                        <div class="f-left r">NDSGN523-081-210819XE-V-O</div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12 mg-bot10">
                                                        <div class="f-left l"><img src="{{asset('')}}layouts/images/i-chair.png" alt="chair"></div>
                                                        <div class="f-left r">
                                                            Số chỗ còn nhận: <span class="font500">7</span>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12 mg-bot10">
                                                        <div class="f-left l"><img src="{{asset('')}}layouts/images/i-date.png" alt="date"></div>
                                                        <div class="f-left r">Ngày đi: <span class="font500">21/08/2019</span></div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12 mg-bot10">
                                                        <div class="f-left l"><img src="{{asset('')}}layouts/images/i-clock.png" alt="clock"></div>
                                                        <div class="f-left r">Ngày về: <span class="font500">24/08/2019</span></div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12 mg-bot10">
                                                        <div class="f-left l"><img src="{{asset('')}}layouts/images/i-price.png" alt="price"></div>
                                                        <div class="f-left r">
                                                            Giá: <span class="font500 price">4,390,000 đ</span>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <div class="f-left l"><img src="{{asset('')}}layouts/images/i-date.png" alt="date"></div>
                                                        <div class="f-left r">Số ngày: <span class="font500">5</span></div>
                                                        <div class="clear"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="item mg-bot30">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="tour-name">
                                                <a href="tour_info.html" title="Nha Trang -Đà Lạt (Tour Tiết Kiệm)"><h3>Miền Tây - Mỹ Tho - Thới Sơn - Bến Tre - Cần Thơ - khách sạn 3 sao (Tour Tiết Kiệm)</h3></a>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <div class="pos-relative">
                                                <a href="tour_info.html" title="Nha Trang - Đà Lạt (Tour Tiết Kiệm)"><img src="{{asset('')}}layouts/images/tf_171205111433_833945.jpg" class="img-responsive pic-lt" alt="Nha Trang - Đ&#224; Lạt (Tour Tiết Kiệm)"></a>
                                            </div>
                                        </div>
                                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                            <div class="frame-info">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6 col-xs-12 mg-bot10">
                                                        <div class="f-left l"><img src="{{asset('')}}layouts/images/i-code.png" alt="code"></div>
                                                        <div class="f-left r">NDSGN523-081-210819XE-V-O</div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12 mg-bot10">
                                                        <div class="f-left l"><img src="{{asset('')}}layouts/images/i-chair.png" alt="chair"></div>
                                                        <div class="f-left r">
                                                            Số chỗ còn nhận: <span class="font500">7</span>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12 mg-bot10">
                                                        <div class="f-left l"><img src="{{asset('')}}layouts/images/i-date.png" alt="date"></div>
                                                        <div class="f-left r">Ngày đi: <span class="font500">21/08/2019</span></div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12 mg-bot10">
                                                        <div class="f-left l"><img src="{{asset('')}}layouts/images/i-clock.png" alt="clock"></div>
                                                        <div class="f-left r">Ngày về: <span class="font500">24/08/2019</span></div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12 mg-bot10">
                                                        <div class="f-left l"><img src="{{asset('')}}layouts/images/i-price.png" alt="price"></div>
                                                        <div class="f-left r">
                                                            Giá: <span class="font500 price">4,390,000 đ</span>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <div class="f-left l"><img src="{{asset('')}}layouts/images/i-date.png" alt="date"></div>
                                                        <div class="f-left r">Số ngày: <span class="font500">5</span></div>
                                                        <div class="clear"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="item mg-bot30">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="tour-name">
                                                <a href="tour_info.html" title="Nha Trang -Đà Lạt (Tour Tiết Kiệm)"><h3>Vé Máy Bay Đà Lạt Khứ Hồi - Vietjet Air - Bao gồm 20kg hành lý ký gửi (Lễ 2/9)</h3></a>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <div class="pos-relative">
                                                <a href="tour_info.html" title="Nha Trang - Đà Lạt (Tour Tiết Kiệm)"><img src="{{asset('')}}layouts/images/tf_160909092754_635463.jpg" class="img-responsive pic-lt" alt="Nha Trang - Đ&#224; Lạt (Tour Tiết Kiệm)"></a>
                                            </div>
                                        </div>
                                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                            <div class="frame-info">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6 col-xs-12 mg-bot10">
                                                        <div class="f-left l"><img src="{{asset('')}}layouts/images/i-code.png" alt="code"></div>
                                                        <div class="f-left r">NDSGN523-081-210819XE-V-O</div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12 mg-bot10">
                                                        <div class="f-left l"><img src="{{asset('')}}layouts/images/i-chair.png" alt="chair"></div>
                                                        <div class="f-left r">
                                                            Số chỗ còn nhận: <span class="font500">7</span>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12 mg-bot10">
                                                        <div class="f-left l"><img src="{{asset('')}}layouts/images/i-date.png" alt="date"></div>
                                                        <div class="f-left r">Ngày đi: <span class="font500">21/08/2019</span></div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12 mg-bot10">
                                                        <div class="f-left l"><img src="{{asset('')}}layouts/images/i-clock.png" alt="clock"></div>
                                                        <div class="f-left r">Ngày về: <span class="font500">24/08/2019</span></div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12 mg-bot10">
                                                        <div class="f-left l"><img src="{{asset('')}}layouts/images/i-price.png" alt="price"></div>
                                                        <div class="f-left r">
                                                            Giá: <span class="font500 price">4,390,000 đ</span>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <div class="f-left l"><img src="{{asset('')}}layouts/images/i-date.png" alt="date"></div>
                                                        <div class="f-left r">Số ngày: <span class="font500">5</span></div>
                                                        <div class="clear"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="item mg-bot30">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="tour-name">
                                                <a href="tour_info.html" title="Nha Trang -Đà Lạt (Tour Tiết Kiệm)"><h3>Nha Trang - Đà Lạt (Tour Tiết Kiệm)</h3></a>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <div class="pos-relative">
                                                <a href="tour_info.html" title="Nha Trang - Đà Lạt (Tour Tiết Kiệm)"><img src="{{asset('')}}layouts/images/tf_180404051952_514767.jpg" class="img-responsive pic-lt" alt="Nha Trang - Đ&#224; Lạt (Tour Tiết Kiệm)"></a>
                                            </div>
                                        </div>
                                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                            <div class="frame-info">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6 col-xs-12 mg-bot10">
                                                        <div class="f-left l"><img src="{{asset('')}}layouts/images/i-code.png" alt="code"></div>
                                                        <div class="f-left r">NDSGN523-081-210819XE-V-O</div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12 mg-bot10">
                                                        <div class="f-left l"><img src="{{asset('')}}layouts/images/i-chair.png" alt="chair"></div>
                                                        <div class="f-left r">
                                                            Số chỗ còn nhận: <span class="font500">7</span>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12 mg-bot10">
                                                        <div class="f-left l"><img src="{{asset('')}}layouts/images/i-date.png" alt="date"></div>
                                                        <div class="f-left r">Ngày đi: <span class="font500">21/08/2019</span></div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12 mg-bot10">
                                                        <div class="f-left l"><img src="{{asset('')}}layouts/images/i-clock.png" alt="clock"></div>
                                                        <div class="f-left r">Ngày về: <span class="font500">24/08/2019</span></div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12 mg-bot10">
                                                        <div class="f-left l"><img src="{{asset('')}}layouts/images/i-price.png" alt="price"></div>
                                                        <div class="f-left r">
                                                            Giá: <span class="font500 price">4,390,000 đ</span>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <div class="f-left l"><img src="{{asset('')}}layouts/images/i-date.png" alt="date"></div>
                                                        <div class="f-left r">Số ngày: <span class="font500">5</span></div>
                                                        <div class="clear"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divEndTour"></div>
                        </div>
                    </div>

                    <div class="col-xs-12 text-right mg-bot30">
                        <div class="pager_simple_orange">
                            <table>
                                <tbody><tr>
                                    <td class=""><a href=""> « </a></td>
                                    <td class="active"><a href="">1</a></td>
                                    <td><a href="">2</a></td>
                                    <td><a href="">3</a></td>
                                    <td><a href="">4</a></td>
                                    <td><a href="">5</a></td>
                                    <td><a href="">6</a></td>
                                    <td class=""><a href=""> » </a></td>
                                </tr>
                                </tbody></table>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--NEW-->
    <div class="container n3-tour-hour">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="title mg-bot10">
                    <a href="">Tin tức</a>
                </h2>
            </div>
            <div id="NewTour">
                <div class="col-md-3 col-md-3 col-sm-6 col-xs-12 mg-bot30">
                    <a href="new_info.html" title="Nét thu Hà thành">
                        <div class="pos-relative">
                            <img src="{{asset('')}}layouts/images/net-thu-ha-thanh_1.jpg" class="img-responsive pic-tgc" alt="Nét thu Hà thành">
                        </div>
                        <div class="frame-tgc2">
                            <div class="tgc-title dot-dot-ajax cut-tgc ddd-truncated" style="overflow-wrap: break-word;">Nét thu Hà thành</div>
                            <div class="tgc-line"></div>
                            <div class="mg-bot10 text-center">
                                <div class="clear"></div>
                                <div class="text-right">
                                    <a href="" class="view_more text-red" title="Có gì ở lễ hội hoa Floriade lớn nhất Nam bán cầu?">Xem thêm &nbsp;<i class="fas fa-long-arrow-alt-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-md-3 col-sm-6 col-xs-12 mg-bot30">
                    <a href="new_info.html" title="Đẹp dịu dàng thu vàng phương Bắc">
                        <div class="pos-relative">
                            <img src="{{asset('')}}layouts/images/mua-thu-phuong-bac_1.jpg" class="img-responsive pic-tgc" alt="Đẹp dịu dàng thu vàng phương Bắc">
                        </div>
                        <div class="frame-tgc2">
                            <div class="tgc-title dot-dot-ajax cut-tgc ddd-truncated" style="overflow-wrap: break-word;">Đẹp dịu dàng thu vàng phương Bắc</div>
                            <div class="tgc-line"></div>
                            <div class="mg-bot10 text-center">
                                <div class="clear"></div>
                                <div class="text-right">
                                    <a href="" class="view_more text-red" title="Có gì ở lễ hội hoa Floriade lớn nhất Nam bán cầu?">Xem thêm &nbsp;<i class="fas fa-long-arrow-alt-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-md-3 col-sm-6 col-xs-12 mg-bot30">
                    <a href="new_info.html" title="Tháng 9 này, lên Mộc Châu đón Tết Độc Lập">
                        <div class="pos-relative">
                            <img src="{{asset('')}}layouts/images/tet-doc-lap-moc-chau.jpg" class="img-responsive pic-tgc" alt="Tháng 9 này, lên Mộc Châu đón Tết Độc Lập">
                        </div>
                        <div class="frame-tgc2">
                            <div class="tgc-title dot-dot-ajax cut-tgc ddd-truncated" style="overflow-wrap: break-word;">Tháng 9 này, lên Mộc Châu...</div>
                            <div class="tgc-line"></div>
                            <div class="mg-bot10 text-center">
                                <div class="clear"></div>
                                <div class="text-right">
                                    <a href="" class="view_more text-red" title="Có gì ở lễ hội hoa Floriade lớn nhất Nam bán cầu?">Xem thêm &nbsp;<i class="fas fa-long-arrow-alt-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-md-3 col-sm-6 col-xs-12 mg-bot30">
                    <a href="new_info.html" title="Tháng 9 này, lên Mộc Châu đón Tết Độc Lập">
                        <div class="pos-relative">
                            <img src="{{asset('')}}layouts/images/tet-doc-lap-moc-chau.jpg" class="img-responsive pic-tgc" alt="Tháng 9 này, lên Mộc Châu đón Tết Độc Lập">
                        </div>
                        <div class="frame-tgc2">
                            <div class="tgc-title dot-dot-ajax cut-tgc ddd-truncated" style="overflow-wrap: break-word;">Tháng 9 này, lên Mộc Châu...</div>
                            <div class="tgc-line"></div>
                            <div class="mg-bot10 text-center">
                                <div class="clear"></div>
                                <div class="text-right">
                                    <a href="" class="view_more text-red" title="Có gì ở lễ hội hoa Floriade lớn nhất Nam bán cầu?">Xem thêm &nbsp;<i class="fas fa-long-arrow-alt-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>



</div>
@endsection
