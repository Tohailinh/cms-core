@extends('frontend.layouts.master')
@section('content')
<div class="container">

    <!-- tour hour -->
    <div class="container n3-tour-hour mt-30">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="title mg-bot10 animated" data-wow-delay="100ms" data-wow-iteration="infinite" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-delay: 300ms; animation-iteration-count: infinite; animation-name: pulse;">
                    <a href="tour.html">Tour khuyến mại</a>
                </h2>
            </div>
            <div id="TourDiscount">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 mg-bot30 wow animated rollIn">
                    <a href="tour_info.html" title="Nha Trang - Hòn Lao - Thế Giới Giải Trí Vinpearl Land (Xe. Tour Mua Ngay) - Giá đã giảm 1.000.000đồng/1 khách - Lễ 2/9">
                        <div class="pos-relative">
                            <img src="{{asset('')}}layouts/images/lm_190828011845_382500.jpg" class="img-responsive pic-tgc" alt="Nha Trang - Hòn Lao - Thế Giới Giải ">
                            <div class="frame-tgc1">
                                <div class="row">
                                    <div class="col-lg-9 col-md-8 col-sm-8 col-xs-8">
                                        <div class="f-left">
                                            <img src="{{asset('')}}layouts/images/i-date-w.png" alt="date">
                                        </div>
                                        <div class="f-left date"><span class="yellow">30/08/2019</span> - <span class="yellow">4 ngày</span></div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4">
                                        <div class="f-left">
                                            <img src="{{asset('')}}layouts/images/i-chair-w.png" alt="chair">
                                        </div>
                                        <div class="f-left chair"><span class="yellow">5 chỗ </span></div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="frame-tgc2">
                            <div class="tgc-title dot-dot-ajax cut-tgc ddd-truncated" style="overflow-wrap: break-word;">Nha Trang - Hòn Lao - Thế Giới Giải Trí Vinpearl… </div>
                            <div class="tgc-line"></div>
                            <div class="mg-bot10">
                                <img src="{{asset('')}}layouts/images/i-price.png" class="f-left" alt="price">
                                <div class="f-left tgc-info"><span class="price-o">3,790,000đ
                                </span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="price-n">3,290,000 đ</span>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 mg-bot30 wow animated bounceInDown ">
                    <a href="tour_info.html" title="Nha Trang - Hòn Lao - Thế Giới Giải Trí Vinpearl Land (Xe. Tour Mua Ngay) - Giá đã giảm 1.000.000đồng/1 khách - Lễ 2/9">
                        <div class="pos-relative">
                            <img src="{{asset('')}}layouts/images/lm_190826042058_677653.jpg" class="img-responsive pic-tgc" alt="Nha Trang - Hòn Lao - Thế Giới Giải Trí Vinpearl Land ">
                            <div class="frame-tgc1">
                                <div class="row">
                                    <div class="col-lg-9 col-md-8 col-sm-8 col-xs-8">
                                        <div class="f-left">
                                            <img src="{{asset('')}}layouts/images/i-date-w.png" alt="date">
                                        </div>
                                        <div class="f-left date"><span class="yellow">30/08/2019</span> - <span class="yellow">4 ngày</span></div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4">
                                        <div class="f-left">
                                            <img src="{{asset('')}}layouts/images/i-chair-w.png" alt="chair">
                                        </div>
                                        <div class="f-left chair"><span class="yellow">5 chỗ </span></div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="frame-tgc2">

                            <div class="tgc-title dot-dot-ajax cut-tgc ddd-truncated" style="overflow-wrap: break-word;">Nha Trang - Hòn Lao - Thế Giới Giải Trí Vinpearl… </div>

                            <div class="tgc-line"></div>
                            <div class="mg-bot10">
                                <img src="{{asset('')}}layouts/images/i-price.png" class="f-left" alt="price">
                                <div class="f-left tgc-info"><span class="price-o">3,790,000đ
                                </span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="price-n">3,290,000 đ</span>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 mg-bot30 wow animated lightSpeedIn">
                    <a href="tour_info.html" title="Nha Trang - Hòn Lao - Thế Giới Giải Trí Vinpearl Land (Xe. Tour Mua Ngay) - Giá đã giảm 1.000.000đồng/1 khách - Lễ 2/9">
                        <div class="pos-relative">
                            <img src="{{asset('')}}layouts/images/lm_190826022923_622124.jpg" class="img-responsive pic-tgc" alt="Nha Trang - Hòn Lao - Thế Giới Giải Trí Vinpearl Land (Xe. Tour Mua Ngay) - Giá đã giảm 1.000.000đồng/1 khách - Lễ 2/9">
                            <div class="frame-tgc1">
                                <div class="row">
                                    <div class="col-lg-9 col-md-8 col-sm-8 col-xs-8">
                                        <div class="f-left">
                                            <img src="{{asset('')}}layouts/images/i-date-w.png" alt="date">
                                        </div>
                                        <div class="f-left date"><span class="yellow">30/08/2019</span> - <span class="yellow">4 ngày</span></div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4">
                                        <div class="f-left">
                                            <img src="{{asset('')}}layouts/images/i-chair-w.png" alt="chair">
                                        </div>
                                        <div class="f-left chair"><span class="yellow">5 chỗ </span></div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="frame-tgc2">

                            <div class="tgc-title dot-dot-ajax cut-tgc ddd-truncated" style="overflow-wrap: break-word;">Nha Trang - Hòn Lao - Thế Giới Giải Trí Vinpearl… </div>

                            <div class="tgc-line"></div>
                            <div class="mg-bot10">
                                <img src="{{asset('')}}layouts/images/i-price.png" class="f-left" alt="price">
                                <div class="f-left tgc-info"><span class="price-o">3,790,000đ
                                </span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="price-n">3,290,000 đ</span>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="container n3-tour-hour">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="title mg-bot10">
                    <a href="">Điểm đến hấp dẫn</a>
                </h2>
            </div>
            <div id="TourHot">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 mg-bot30 wow animated rollIn">
                    <a href="tour_info.html" title="Nha Trang - Hòn Lao - Thế Giới Giải Trí Vinpearl Land (Xe. Tour Mua Ngay) - Giá đã giảm 1.000.000đồng/1 khách - Lễ 2/9">
                        <div class="pos-relative">
                            <img src="{{asset('')}}layouts/images/lm_190830073910_853210.jpg" class="img-responsive pic-tgc" alt="Nha Trang - Hòn Lao - Thế Giới Giải Trí Vinpearl Land (Xe. Tour Mua Ngay) - Giá đã giảm 1.000.000đồng/1 khách - Lễ 2/9">
                            <div class="frame-tgc1">
                                <div class="row">
                                    <div class="col-lg-9 col-md-8 col-sm-8 col-xs-8">
                                        <div class="f-left">
                                            <img src="{{asset('')}}layouts/images/i-date-w.png" alt="date">
                                        </div>
                                        <div class="f-left date"><span class="yellow">30/08/2019</span> - <span class="yellow">4 ngày</span></div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4">
                                        <div class="f-left">
                                            <img src="{{asset('')}}layouts/images/i-chair-w.png" alt="chair">
                                        </div>
                                        <div class="f-left chair"><span class="yellow">5 chỗ </span></div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="frame-tgc2">

                            <div class="tgc-title dot-dot-ajax cut-tgc ddd-truncated" style="overflow-wrap: break-word;">Hà Lan - Đức - Luxembourg - Bỉ - Pháp..</div>

                            <div class="tgc-line"></div>
                            <div class="mg-bot10">
                                <img src="{{asset('')}}layouts/images/i-price.png" class="f-left" alt="price">
                                <div class="f-left tgc-info"><span class="price-o">3,790,000đ
                                </span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="price-n">3,290,000 đ</span>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 mg-bot30 wow animated rollIn">
                    <a href="tour_info.html" title="Nha Trang - Hòn Lao - Thế Giới Giải Trí Vinpearl Land (Xe. Tour Mua Ngay) - Giá đã giảm 1.000.000đồng/1 khách - Lễ 2/9">
                        <div class="pos-relative">
                            <img src="{{asset('')}}layouts/images/lm_190828014717_777506.jpg" class="img-responsive pic-tgc" alt="Nha Trang - Hòn Lao - Thế Giới Giải Trí Vinpearl">
                            <div class="frame-tgc1">
                                <div class="row">
                                    <div class="col-lg-9 col-md-8 col-sm-8 col-xs-8">
                                        <div class="f-left">
                                            <img src="{{asset('')}}layouts/images/i-date-w.png" alt="date">
                                        </div>
                                        <div class="f-left date"><span class="yellow">30/08/2019</span> - <span class="yellow">4 ngày</span></div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4">
                                        <div class="f-left">
                                            <img src="{{asset('')}}layouts/images/i-chair-w.png" alt="chair">
                                        </div>
                                        <div class="f-left chair"><span class="yellow">5 chỗ </span></div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="frame-tgc2">

                            <div class="tgc-title dot-dot-ajax cut-tgc ddd-truncated" style="overflow-wrap: break-word;">Busan - Gyeongju - Seoul - Nami - Everland… </div>

                            <div class="tgc-line"></div>
                            <div class="mg-bot10">
                                <img src="{{asset('')}}layouts/images/i-price.png" class="f-left" alt="price">
                                <div class="f-left tgc-info"><span class="price-o">3,790,000đ
                                </span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="price-n">3,290,000 đ</span>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 mg-bot30 wow animated rollIn">
                    <a href="tour_info.html" title="Nha Trang - Hòn Lao - Thế Giới Giải Trí Vinpearl Land (Xe. Tour Mua Ngay) - Giá đã giảm 1.000.000đồng/1 khách - Lễ 2/9">
                        <div class="pos-relative">
                            <img src="{{asset('')}}layouts/images/lm_190826022923_622124.jpg" class="img-responsive pic-tgc" alt="Nha Trang - Hòn Lao - Thế Giới Giải Trí Vinpearl Land (Xe. Tour Mua Ngay) - Giá đã giảm 1.000.000đồng/1 khách - Lễ 2/9">
                            <div class="frame-tgc1">
                                <div class="row">
                                    <div class="col-lg-9 col-md-8 col-sm-8 col-xs-8">
                                        <div class="f-left">
                                            <img src="{{asset('')}}layouts/images/i-date-w.png" alt="date">
                                        </div>
                                        <div class="f-left date"><span class="yellow">30/08/2019</span> - <span class="yellow">4 ngày</span></div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4">
                                        <div class="f-left">
                                            <img src="{{asset('')}}layouts/images/i-chair-w.png" alt="chair">
                                        </div>
                                        <div class="f-left chair"><span class="yellow">5 chỗ </span></div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="frame-tgc2">

                            <div class="tgc-title dot-dot-ajax cut-tgc ddd-truncated" style="overflow-wrap: break-word;">Nha Trang - Hòn Lao - Thế Giới Giải Trí Vinpearl… </div>

                            <div class="tgc-line"></div>
                            <div class="mg-bot10">
                                <img src="{{asset('')}}layouts/images/i-price.png" class="f-left" alt="price">
                                <div class="f-left tgc-info"><span class="price-o">3,790,000đ
                                </span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="price-n">3,290,000 đ</span>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <section id="br-section">
        <div class="container">
            <div class="col-sm-8 col-sm-push-2">
                <div class="br-text">
                    <h2 style="">
                        <strong>Du lịch </strong>cho cuộc sống
                        <span class="subtitle" style="color:#ffffff">Hưởng niềm vui chọn vẹn</span>
                    </h2>
                </div>
            </div>
        </div>
    </section>

    <!-- destination -->
    <div class="container n3-destination mt-30">
        <div class="row">
            <div class="col-xs-12 mg-bot30">
                <h2 class="title mg-bot10"><a>Điểm đến yêu thích</a></h2>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 mg-bot30 wow animated rollIn" style="animation-delay: 0.6s;">
                <div class="pos-relative">
                    <a href="/du-lich-ha-long.aspx" class="hvr-bounce-to-bottom">
                        <img src="{{asset('')}}layouts/images/dd1.jpg" alt="Vịnh Hạ Long" class="img-responsive dd-img">
                        <div class="frame-destination d1">
                            <div class="destination-name">Vịnh Hạ Long</div>
                            <div class="destination-like">Đ&#227; c&#243; <span class="num-like">1,600<sup class="k">+</sup></span> lượt kh&#225;ch</div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 mg-bot30 wow animated rollIn" style="animation-delay: 0.8s;">
                <div class="pos-relative">
                    <a href="/du-lich-lao-cai.aspx" class="hvr-bounce-to-bottom">
                        <img src="{{asset('')}}layouts/images/dd2.jpg" alt="Sapa" class="img-responsive dd-img">
                        <div class="frame-destination d2">
                            <div class="destination-name">Sapa</div>
                            <div class="destination-like">Đ&#227; c&#243; <span class="num-like">1,200<sup class="k">+</sup></span> lượt kh&#225;ch</div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 mg-bot30 wow animated rollIn" style="animation-delay: 1.0s;">
                <div class="pos-relative">
                    <a href="/du-lich-da-nang.aspx" class="hvr-bounce-to-bottom">
                        <img src="{{asset('')}}layouts/images/dd3.jpg" alt="Đà Nẵng" class="img-responsive dd-img">
                        <div class="frame-destination d3">
                            <div class="destination-name">Đ&#224; Nẵng</div>
                            <div class="destination-like">Đ&#227; c&#243; <span class="num-like">1,100<sup class="k">+</sup></span> lượt kh&#225;ch</div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 mg-bot30 wow animated rollIn" style="animation-delay: 1.2s;">
                <div class="pos-relative">
                    <a href="/du-lich-binh-dinh.aspx" class="hvr-bounce-to-bottom">
                        <img src="{{asset('')}}layouts/images/dd4.jpg" alt="Qui Nhơn" class="img-responsive dd-img">
                        <div class="frame-destination d4">
                            <div class="destination-name">Qui Nhơn</div>
                            <div class="destination-like">Đ&#227; c&#243; <span class="num-like">1,900<sup class="k">+</sup></span> lượt kh&#225;ch</div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-xs-12">
                <div style="border-top: 1px dashed #ccc;padding-bottom: 30px;padding-top: 0px;"></div>
            </div>
            <div class="nation">
                <div class="col-lg-2 col-xs-12 mg-bot30 wow animated rollIn">
                    <a href="/du-lich-nuoc-ngoai/tour-chau-au.aspx" class="hvr-sweep-to-right" title="Ch&#226;u &#194;u">
                        <div class="pos-relative">
                            <img src="{{asset('')}}layouts/images/qg1.jpg" alt="Ch&#226;u &#194;u" class="img-responsive qg-img">
                            <div class="frame-nation">
                                <div class="nation-name"><span class="imp">Ch&#226;u &#194;u</span></div>
                                <div>
                                    <div class="nation-expl f-left">Kh&#225;m ph&#225; ngay</div>
                                    <div class="f-left"><img src="{{asset('')}}layouts/images/i-arrow.png" alt="arrow" class="img-responsive"></div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-2 col-xs-12 mg-bot30 wow animated bounceInDown" style="animation-delay: 0.6s;">
                    <a href="/du-lich-nuoc-ngoai/tour-chau-a.aspx" class="hvr-sweep-to-right" title="Ch&#226;u &#193;">
                        <div class="pos-relative">
                            <img src="{{asset('')}}layouts/images/qg2.jpg" alt="Ch&#226;u &#193;" class="img-responsive qg-img">
                            <div class="frame-nation">
                                <div>
                                    <div class="nation-name"><span class="imp">Ch&#226;u &#193;</span></div>
                                    <div class="nation-expl f-left">Kh&#225;m ph&#225; ngay</div>
                                    <div class="f-left"><img src="{{asset('')}}layouts/images/i-arrow.png" alt="arrow" class="img-responsive"></div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-2 col-xs-12 mg-bot30 wow animated shake" style="animation-delay: 0.6s;">
                    <a href="/du-lich-nuoc-ngoai/tour-chau-uc.aspx" class="hvr-sweep-to-right" title="Ch&#226;u &#218;c">
                        <div class="pos-relative">
                            <img src="{{asset('')}}layouts/images/qg3.jpg" alt="Ch&#226;u &#218;c" class="img-responsive qg-img">
                            <div class="frame-nation">
                                <div class="nation-name"><span class="imp">Ch&#226;u &#218;c</span></div>
                                <div>
                                    <div class="nation-expl f-left">Kh&#225;m ph&#225; ngay</div>
                                    <div class="f-left"><img src="{{asset('')}}layouts/images/i-arrow.png" alt="arrow" class="img-responsive"></div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-2 col-xs-12 mg-bot30 wow animated swing " style="animation-delay: 1.2s;">
                    <a href="/du-lich-nuoc-ngoai/tour-chau-my.aspx" title="Ch&#226;u Mỹ" class="hvr-sweep-to-right">
                        <div class="pos-relative">
                            <img src="{{asset('')}}layouts/images/qg4.jpg" alt="Ch&#226;u Mỹ" class="img-responsive qg-img">
                            <div class="frame-nation">
                                <div class="nation-name"><span class="imp">Ch&#226;u Mỹ</span></div>
                                <div>
                                    <div class="nation-expl f-left">Kh&#225;m ph&#225; ngay</div>
                                    <div class="f-left"><img src="{{asset('')}}layouts/images/i-arrow.png" alt="arrow" class=""></div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-2 col-xs-12 mg-bot30 wow animated bounceInDown " style="animation-delay: 1.2s;">
                    <a href="/du-lich-nuoc-ngoai/tour-chau-phi.aspx" class="hvr-sweep-to-right" title="Ch&#226;u Phi">
                        <div class="pos-relative">
                            <img src="{{asset('')}}layouts/images/qg5.jpg" alt="Ch&#226;u Phi" class="img-responsive qg-img">
                            <div class="frame-nation">
                                <div class="nation-name"><span class="imp">Ch&#226;u Phi</span></div>
                                <div>
                                    <div class="nation-expl f-left">Kh&#225;m ph&#225; ngay</div>
                                    <div class="f-left"><img src="{{asset('')}}layouts/images/i-arrow.png" alt="arrow" class="img-responsive"></div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <!--NEW-->
    <div class="container n3-tour-hour">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="title mg-bot10">
                    <a href="">Tin tức</a>
                </h2>
            </div>
            <div id="NewTour">
                <div class="col-md-3 col-md-3 col-sm-6 col-xs-12 mg-bot30">
                    <a href="new_info.html" title="Nét thu Hà thành">
                        <div class="pos-relative">
                            <img src="{{asset('')}}layouts/images/net-thu-ha-thanh_1.jpg" class="img-responsive pic-tgc" alt="Nét thu Hà thành">
                        </div>
                        <div class="frame-tgc2">
                            <div class="tgc-title dot-dot-ajax cut-tgc ddd-truncated" style="overflow-wrap: break-word;">Nét thu Hà thành</div>
                            <div class="tgc-line"></div>
                            <div class="mg-bot10 text-center">
                                <div class="clear"></div>
                                <div class="text-right">
                                    <a href="" class="view_more text-red" title="Có gì ở lễ hội hoa Floriade lớn nhất Nam bán cầu?">Xem thêm &nbsp;<i class="fas fa-long-arrow-alt-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-md-3 col-sm-6 col-xs-12 mg-bot30">
                    <a href="new_info.html" title="Đẹp dịu dàng thu vàng phương Bắc">
                        <div class="pos-relative">
                            <img src="{{asset('')}}layouts/images/mua-thu-phuong-bac_1.jpg" class="img-responsive pic-tgc" alt="Đẹp dịu dàng thu vàng phương Bắc">
                        </div>
                        <div class="frame-tgc2">
                            <div class="tgc-title dot-dot-ajax cut-tgc ddd-truncated" style="overflow-wrap: break-word;">Đẹp dịu dàng thu vàng phương Bắc</div>
                            <div class="tgc-line"></div>
                            <div class="mg-bot10 text-center">
                                <div class="clear"></div>
                                <div class="text-right">
                                    <a href="" class="view_more text-red" title="Có gì ở lễ hội hoa Floriade lớn nhất Nam bán cầu?">Xem thêm &nbsp;<i class="fas fa-long-arrow-alt-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-md-3 col-sm-6 col-xs-12 mg-bot30">
                    <a href="new_info.html" title="Tháng 9 này, lên Mộc Châu đón Tết Độc Lập">
                        <div class="pos-relative">
                            <img src="{{asset('')}}layouts/images/tet-doc-lap-moc-chau.jpg" class="img-responsive pic-tgc" alt="Tháng 9 này, lên Mộc Châu đón Tết Độc Lập">
                        </div>
                        <div class="frame-tgc2">
                            <div class="tgc-title dot-dot-ajax cut-tgc ddd-truncated" style="overflow-wrap: break-word;">Tháng 9 này, lên Mộc Châu...</div>
                            <div class="tgc-line"></div>
                            <div class="mg-bot10 text-center">
                                <div class="clear"></div>
                                <div class="text-right">
                                    <a href="" class="view_more text-red" title="Có gì ở lễ hội hoa Floriade lớn nhất Nam bán cầu?">Xem thêm &nbsp;<i class="fas fa-long-arrow-alt-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-md-3 col-sm-6 col-xs-12 mg-bot30">
                    <a href="new_info.html" title="Tháng 9 này, lên Mộc Châu đón Tết Độc Lập">
                        <div class="pos-relative">
                            <img src="{{asset('')}}layouts/images/tet-doc-lap-moc-chau.jpg" class="img-responsive pic-tgc" alt="Tháng 9 này, lên Mộc Châu đón Tết Độc Lập">
                        </div>
                        <div class="frame-tgc2">
                            <div class="tgc-title dot-dot-ajax cut-tgc ddd-truncated" style="overflow-wrap: break-word;">Tháng 9 này, lên Mộc Châu...</div>
                            <div class="tgc-line"></div>
                            <div class="mg-bot10 text-center">
                                <div class="clear"></div>
                                <div class="text-right">
                                    <a href="" class="view_more text-red" title="Có gì ở lễ hội hoa Floriade lớn nhất Nam bán cầu?">Xem thêm &nbsp;<i class="fas fa-long-arrow-alt-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
