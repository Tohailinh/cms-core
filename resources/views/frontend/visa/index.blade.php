@extends('frontend.layouts.master')
@section('content')
<div class="container">

    <!-- breadcrumb -->
    <div class="n3-breadcrumb mt-30">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="breadcrumb">
                        <span><a href="" itemprop="url"><span itemprop="title">Du lịch</span></a></span> »
                        <span><a href="" itemprop="url"><span itemprop="title">Du lịch trong nước</span></a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container n3-contact mg-bot40 mt-30">
        <div class="row">
            <div class="col-xs-12 mg-bot15">
                <div class="title mg-bot15"><h1>Tư vấn visa</h1></div>
                <div class="text-center">Để c&#243; thể đ&#225;p ứng được c&#225;c y&#234;u cầu v&#224; c&#225;c &#253; kiến đ&#243;ng g&#243;p của qu&#253; vị một c&#225;ch nhanh ch&#243;ng xin vui l&#242;ng li&#234;n hệ</div>
            </div>
            <div class="col-xs-12 mg-bot50">
                <form action="" id="SendEmail" method="post">

                    <div class="frame-contact">
                        <div class="row">
                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 mg-bot15">
                                <label>Họ Tên (<span class="star">*</span>)</label>
                                <input type="text" class="form-control" required="required" name="txtHoTen" id="txtHoTen">
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 mg-bot15">
                                <label>Email (<span class="star">*</span>)</label>
                                <input type="email" class="form-control" required="required" name="txtEmail" id="txtEmail">
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 mg-bot15">
                                <label>Điện thoại (<span class="star">*</span>)</label>
                                <input type="text" class="form-control" required="required" name="txtDienThoai" id="txtDienThoai">
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 mg-bot15">
                                <label>Tên công ty</label>
                                <input type="text" class="form-control" name="txtTenCongTy">
                            </div>

                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 mg-bot15">
                                <label>Địa chỉ</label>
                                <input type="text" class="form-control" name="txtDiaChi">
                            </div>

                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 mg-bot15">
                                <label>Ti&#234;u đề (<span class="star">*</span>)</label>
                                <input type="text" class="form-control" required="required" name="txtTieuDe">
                            </div>

                            <div class="col-xs-12 mg-bot30">
                                <label>Nội dung (<span class="star">*</span>)</label>
                                <textarea class="form-control" rows="4" cols="5" required="required" name="txtNoiDung"></textarea>
                            </div>

                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 mg-bot15">
                                <label>Tải </label>
                                <input type="file" class="form-control" required="required" name="txtTieuDe">
                            </div>

                            <div class="col-xs-12 text-center">
                                <button type="submit" class="btn btn-md btn-general">Gửi đi &nbsp;&nbsp;<i class="fas fa-paper-plane"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 mg-bot30">
                        <div class="frame-cn">
                            <div class="tencn">Hồ Ch&#237; Minh</div>
                            <div class="mg-bot7">
                                <div class="i-con"><i class="fas fa-map-signs"></i></div>
                                <div class="i-text">190 Pasteur, Quận 3, Tp. Hồ Chí Minh, Việt Nam</div>
                                <div class="clear"></div>
                            </div>
                            <div class="mg-bot7">
                                <div class="i-con"><i class="fas fa-phone"></i></div>
                                <div class="i-text">Tel: (84-28) 3822 8898</div>
                                <div class="clear"></div>
                            </div>
                            <div class="mg-bot7">
                                <div class="i-con"><i class="fas fa-fax"></i></div>
                                <div class="i-text">Fax: (84-28) 3829 9142</div>
                                <div class="clear"></div>
                            </div>
                            <div class="mg-bot7">
                                <div class="i-con"><i class="fas fa-envelope"></i></div>
                                <div class="i-text">Email: info@vietravel.com</div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 mg-bot30">
                        <div class="frame-cn">
                            <div class="tencn">Chi Nh&#225;nh H&#224; Nội</div>
                            <div class="mg-bot7">
                                <div class="i-con"><i class="fas fa-map-signs"></i></div>
                                <div class="i-text">03 Hai Bà Trưng, Quận Hoàn Kiếm, Hà Nội</div>
                                <div class="clear"></div>
                            </div>
                            <div class="mg-bot7">
                                <div class="i-con"><i class="fas fa-phone"></i></div>
                                <div class="i-text">Tel: (84-24) 3933 1978</div>
                                <div class="clear"></div>
                            </div>
                            <div class="mg-bot7">
                                <div class="i-con"><i class="fas fa-fax"></i></div>
                                <div class="i-text">Fax: (84-24) 3933 1979</div>
                                <div class="clear"></div>
                            </div>
                            <div class="mg-bot7">
                                <div class="i-con"><i class="fas fa-envelope"></i></div>
                                <div class="i-text">Email: vtv.hanoi@vietravel.com</div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 mg-bot30">
                        <div class="frame-cn">
                            <div class="tencn">Chi Nh&#225;nh Đ&#224; Nẵng</div>
                            <div class="mg-bot7">
                                <div class="i-con"><i class="fas fa-map-signs"></i></div>
                                <div class="i-text">58 Pasteur, Hải Châu, Đà Nẵng</div>
                                <div class="clear"></div>
                            </div>
                            <div class="mg-bot7">
                                <div class="i-con"><i class="fas fa-phone"></i></div>
                                <div class="i-text">Tel: (84-236) 3863 544</div>
                                <div class="clear"></div>
                            </div>
                            <div class="mg-bot7">
                                <div class="i-con"><i class="fas fa-fax"></i></div>
                                <div class="i-text">Fax: (84-236) 3863 571</div>
                                <div class="clear"></div>
                            </div>
                            <div class="mg-bot7">
                                <div class="i-con"><i class="fas fa-envelope"></i></div>
                                <div class="i-text">Email: vtv.danang@vietravel.com</div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
@endsection
