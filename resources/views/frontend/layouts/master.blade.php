<!DOCTYPE html>
<html>
<head>
    <title>@if(isset($title)){{$title}}@else TvTravel @endif</title>
    @if(isset($description))
        <meta name="description" content="{{$description}}">
    @endif
    @if(isset($keywords))
        <meta name="keywords" content="{{$keywords}}" />
    @endif
    @if(isset($og_image))
        <meta property="og:image" content="{{$og_image}}" />
    @else
        <meta property="og:image" content="{{asset('')}}user/img/logo_2.jpg?v=1" />
    @endif
    <meta property='og:type' content='website'/>
    <meta name="apple-itunes-app" content="app-id=1444566830, app-argument=spaceships://spaceship/7" />
    {{--<link rel="icon" href="{{asset('')}}user/img/icon.png?v=1" sizes="32x32">--}}
    <meta name="viewport" content="width=device-width, initial-scale=1">

    @include('frontend.layouts.meta_seo')
    <meta charset="utf-8">
    @include('frontend.layouts.datatables_css')
    @yield('css_frontend')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>
<body>
    @include('frontend.layouts.menu')
    @include('frontend.layouts.slide')
    @include('frontend.layouts.search')

    @yield('content')

    @include('frontend.layouts.footer')

</body>
</html>

@include('frontend.layouts.datatables_js')
@yield('js_frontend')

<div id="fb-root"></div>
{{--<script>--}}
    {{--window.fbAsyncInit = function() {--}}
        {{--FB.init({--}}
            {{--appId            : '578734159326726',--}}
            {{--autoLogAppEvents : true,--}}
            {{--xfbml            : true,--}}
            {{--version          : 'v4.0'--}}
        {{--});--}}
    {{--};--}}

    {{--(function(d, s, id) {--}}
        {{--var js, fjs = d.getElementsByTagName(s)[0];--}}
        {{--if (d.getElementById(id)) return;--}}
        {{--js = d.createElement(s); js.id = id;--}}
        {{--js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';--}}
        {{--fjs.parentNode.insertBefore(js, fjs);--}}
    {{--}(document, 'script', 'facebook-jssdk'));</script>--}}

{{--<!-- Your customer chat code -->--}}
{{--<div class="fb-customerchat"--}}
     {{--attribution=setup_tool--}}
     {{--page_id="113414180035647"--}}
     {{--logged_in_greeting="Xin chào!"--}}
     {{--logged_out_greeting="Xin chào!">--}}
{{--</div>--}}
