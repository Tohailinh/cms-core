
<div class="n3-form-search" style="z-index: initial; position: initial; background: rgb(241, 241, 241);">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="search-box">
                    <form action="" method="GET" role="form">
                        <div class="row">
                            <div class="form-group col-xs-12 col-sm-12 col-md-4">
                                <input type="text" name="s" class="form-control" placeholder="Từ khóa tìm kiếm...">
                            </div>
                            <div class="form-group col-xs-12 col-sm-12 col-md-3">
                                <select name="dia-diem-du-lich" id="tour-search" class="form-control">
                                    <option value="">Chọn Tour</option>
                                    <option value="du-lich-ha-noi"> Tour trong nước</option>
                                    <option value="du-lich-da-nang"> Tour nước ngoài</option>
                                </select>
                            </div>
                            <div class="form-group col-xs-12 col-sm-12 col-md-3">
                                <select name="dia-diem-du-lich" id="" class="form-control">
                                    <option value="">Chọn địa điểm</option>
                                    <option value="du-lich-ha-noi"> Du lịch Hà Nội</option>
                                    <option value="du-lich-da-nang"> Du lịch Đà Nẵng</option>
                                </select>
                            </div>
                            <div class="form-group col-xs-12 col-sm-12 col-md-2 text-center">
                                <button type="submit" class="btn btn-primary">Tìm kiếm</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="form-info-search bg-dltn pos-relative" style="display: none;">
        <form action="/Tour/SearchFirst" method="post"><input name="__RequestVerificationToken" type="hidden" value="">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="triangle-up"></div>
                        <div class="pad-tb">
                            <div class="row">
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="mg-b">Nơi khởi h&#224;nh:</label>
                                        <select class="form-control input-md" id="departureID" name="DepartureId" style="padding-left:5px !important;"></select>
                                        <input type="hidden" name="TourTypeId" value="1">
                                        <input type="hidden" name="ConCho" value="-1">
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="mg-b">Nơi đến:</label>
                                        <select class="form-control input-md" id="group_id" name="GroupId" style="padding-left:5px !important;"></select>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="mg-b">Ng&#224;y khởi h&#224;nh:</label>
                                        <div class="pos-relative">
                                            <input autocomplete="off" class="form-control input-md" id="departure_date" name="DepartureDate" placeholder="19/08/2019" type="text" value="">
                                            <span class="i-calendar">
                                            <i class="far fa-calendar-alt fa-lg"></i>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="mg-b">D&#242;ng tour:</label>
                                        <select class="form-control input-md" id="DongTour" name="DongTour" style="padding-left:5px !important;"><option value="0">Tất cả</option>
                                            <option value="1">Cao cấp</option>
                                            <option value="2">Ti&#234;u chuẩn</option>
                                            <option value="3">Tiết kiệm</option>
                                            <option value="4">Gi&#225; Tốt</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="mg-b">Gi&#225;:</label>
                                        <select class="form-control input-md" id="priceID" name="PriceId" style="padding-left:5px !important;"></select>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-md btn-search btn-mg-t" onclick="return checkSearch('departureID'); ga('event','Search', 'SearchClick', 'keywords');"><i class="fas fa-search"></i>&nbsp;&nbsp;T&#236;m kiếm</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="form-info-search bg-dlnn pos-relative" style="display: none;">
        <form action="/Tour/SearchFirst" method="post"><input name="__RequestVerificationToken" type="hidden" value="3Ot9dzhJ4mrXcD50eGLugpFK67szFm8uYEOCGXr2UtPgafLxA41md-TW5_uTk5DZXyGf8CozrwQRMEX5A6oUeNjMLzLSY-p_2q2MZq_x3MA1">        <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="triangle-up"></div>
                        <div class="pad-tb">
                            <div class="row">
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="mg-b">Nơi khởi h&#224;nh:</label>
                                        <select class="form-control input-md" id="departureIDob" name="DepartureId" style="padding-left:5px !important;"></select>
                                        <input type="hidden" name="TourTypeId" value="2">
                                        <input type="hidden" name="ConCho" value="-1">
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="mg-b">Nơi đến:</label>
                                        <select class="form-control input-md" id="group_idob" name="GroupId" style="padding-left:5px !important;"></select>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="mg-b">Ng&#224;y khởi h&#224;nh:</label>
                                        <div class="pos-relative">
                                            <input autocomplete="off" class="form-control" id="departure_dateob" name="DepartureDate" placeholder="19/08/2019" type="text" value="">
                                            <span class="i-calendar">
                                            <i class="far fa-calendar-alt fa-lg"></i>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="mg-b">D&#242;ng tour:</label>
                                        <select class="form-control input-md" id="DongTourob" name="DongTour" style="padding-left:5px !important;"><option value="0">Tất cả</option>
                                            <option value="1">Cao cấp</option>
                                            <option value="2">Ti&#234;u chuẩn</option>
                                            <option value="3">Tiết kiệm</option>
                                            <option value="4">Gi&#225; Tốt</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="mg-b">Gi&#225;:</label>
                                        <select class="form-control input-md" id="priceIDob" name="PriceId" style="padding-left:5px !important;"></select>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-md btn-search btn-mg-t" onclick="return checkSearch('departureIDob'); ga('event','Search', 'SearchClick', 'keywords');"><i class="fas fa-search"></i>&nbsp;&nbsp;T&#236;m kiếm</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>    </div>
    <div class="form-info-search bg-dltc pos-relative" style="display: none;">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="triangle-up"></div>
                    <div class="pad-tb">
                        <div class="row">
                            <div class="col-lg-2 col-md-1 hidden-sm col-xs-12">
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                                <div class="form-group service-type">
                                    <label class="mg-b">Loại dịch vụ:</label>
                                    <select class="form-control input-md">
                                        <option>Land tour</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                                <div class="form-group">
                                    <label class="mg-b">Loại tour:</label>
                                    <select class="form-control input-md">
                                        <option>Trong nước</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                <div class="form-group">
                                    <label class="mg-b">Nơi đến:</label>
                                    <select class="form-control input-md">
                                        <option>Nha Trang</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <div class="form-group">
                                    <button class="btn btn-md btn-search btn-mg-t"><i class="fas fa-search"></i>&nbsp;&nbsp;Tìm kiếm</button>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-1 hidden-sm col-xs-12">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-info-search bg-dltt pos-relative" style="display: none;">
        <form action="/Booking/SearchBookingFirst" method="post">        <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="triangle-up"></div>
                        <div class="pad-tb">
                            <div class="row">
                                <div class="col-lg-3 col-md-2 col-sm-1 col-xs-12">
                                </div>
                                <div class="col-lg-5 col-md-6 col-sm-8 col-xs-12">
                                    <div class="form-group">
                                        <label class="mg-b">Số booking:</label>
                                        <input type="text" id="pin_code" name="pin_code" class="form-control input-md" placeholder="Số booking">
                                    </div>
                                </div>
                                <div class="col-lg-1 col-md-2 col-sm-2 col-xs-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-md btn-search btn-mg-t"><i class="fas fa-search"></i>&nbsp;&nbsp;T&#236;m kiếm</button>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-2 col-sm-1 col-xs-12">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
