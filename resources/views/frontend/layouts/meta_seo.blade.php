@if(isset($og_url))
    <meta property="og:url" content="{{$og_url}}" />
@endif
@if(isset($title))
    <meta property="og:title" content="{{$title}}" />
@endif
@if(isset($description))
    <meta property="og:description" content="{{$description}}" />
@endif
