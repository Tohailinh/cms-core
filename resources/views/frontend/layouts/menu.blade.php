<div class="n3-header">
    <div class="h-top">
        <div class="col-md-4 col-sm-4 col-xs-4">
            <div class="area-info">
                <div class="phone-support">
                    <div class="dropdown-toggle" data-toggle="dropdown">
                        <div class="f-left">
                            <span class="hidden-xs">Hotline:</span> <span class="num-color wow animated pulse">1900 1839</span>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div class="col-md-8 col-sm-8 col-xs-8">
            <div class="area-info">
                <div class="f-right nav-top-mini hidden-lg hidden-md" onclick="onClickBlock()">
                    <div class="i-menu text-right">
                        <div><i class="fas fa-bars fa-lg"></i></div>
                    </div>
                    <ul class="nav-top">
                        <li class="hidden-lg hidden-md"><a href="index.html">Trang chủ</a></li>
                        <li class="hidden-lg hidden-md"><a href="tour.html">Tour du lịch</a></li>
                        <li class="hidden-lg hidden-md"><a href="tour.html">Du lịch trong nước</a></li>
                        <li class="hidden-lg hidden-md"><a href="tour.html">Du lịch nước ngoài</a></li>
                        <li class="hidden-lg hidden-md"><a href="tintuc.html">Tin tức</a></li>
                        <li class="hidden-lg hidden-md"><a href="tintuc.html">Tư vấn visa</a></li>
                        <li class="hidden-lg hidden-md"><a href="khuyenmai.html">Khuyến mại</a></li>
                        <li class="hidden-lg hidden-md"><a href="faq.html">FAQ</a></li>
                        <li class="hidden-lg hidden-md"><a href="lienhe.html">Liên hệ</a></li>
                    </ul>
                </div>
                <div class="f-right h-line hidden-lg hidden-md">
                    <img src="{{asset('')}}layouts/images/h-line.png" alt="line">
                </div>
                <div class="f-right lang-info">
                    <div class="obj pos-relative">
                        <div class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{asset('')}}layouts/images/vn.png" alt="Vietnamese">
                            <span style="padding-left: 5px;" class="hidden-xs">VN</span>
                        </div>
                        <ul class="dropdown-menu">
                            <li style="border-bottom: 1px dashed #ccc;"><a href="https://travel.com.vn"><strong class="mauchudao">VN</strong> - Tiếng Việt</a></li>
                            <li><a href="https://en.travel.com.vn"><strong class="mauchudao">EN</strong> - Tiếng Anh</a></li>
                        </ul>
                    </div>
                </div>
                <div class="f-right h-line">
                    <img src="{{asset('')}}layouts/images/h-line.png" alt="line">
                </div>
                <div class="f-right user-info">
                    <div class="obj pos-relative">
                        <img src="{{asset('')}}layouts/images/user-s.png" alt="Tài khoản" class="dropdown-toggle" data-toggle="dropdown">
                        <ul class="dropdown-menu">
                            <li style="border-bottom: 1px dashed #ccc;"><a href="#" data-toggle="modal" data-target="#dangnhap">Đăng nhập</a></li>
                            <li><a href="#" data-toggle="modal" data-target="#dangky">Đăng k&#253;</a></li>
                        </ul>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="f-right h-line">
                    <img src="{{asset('')}}layouts/images/h-line.png" alt="line">
                </div>
                <div class="f-right hidden-xs">
                    <img src="{{asset('')}}layouts/images/h-line.png" alt="line">
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    <div class="h-bot">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <a href="/">
                        <img src="{{asset('')}}layouts/images/logo.jpg" alt="logo" class="logo">
                    </a>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <nav class="menu hidden-xs">
                        <ul>
                            <li class="hidden-xs">
                                <a href="index.html">Trang chủ</a>
                            </li>
                            <li class="hidden-xs">
                                <a href="tour.html">Tour du lịch</a>
                                <ul class="sub-menu">
                                    <li><a href="tour.html">Du lịch trong nước</a></li>
                                    <li><a href="tour.html">Du lịch nước ngoài</a></li>
                                </ul>
                            </li>

                            <li class="hidden-xs">
                                <a href="tintuc.html">Tin tức</a>
                            </li>

                            <li class="hidden-lg hidden-md hidden-sm"><a href="tintuc.html">Tin tức</a></li>
                            <li class="hidden-xs"><a href="visa.html">Tư vấn visa</a></li>
                            <li class="hidden-xs">
                                <a href="khuyenmai.html">Khuyến mại</a>
                            </li>
                            <li class="hidden-lg hidden-md hidden-sm"><a href="khuyenmai.html">Khuyến mại Thu 2019</a></li>

                            <li><a href="faq.html">FAQ</a></li>
                            <li><a href="lienhe.html">Liên hệ</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="dangnhap" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">ĐĂNG NHẬP</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 mg-bot15">
                        <div class="input-container">
                            <i class="fas fa-envelope icon"></i>
                            <input class="input-field" id="EmailLogin" name="EmailLogin" placeholder="Email" type="email" value="">
                        </div>
                    </div>
                    <div class="col-md-12 mg-bot15">
                        <div class="input-container">
                            <i class="fa fa-key icon"></i>
                            <input class="input-field" id="PasswordLogin" name="PasswordLogin" placeholder="Mật khẩu" type="password" value="">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div>
                            Bạn chưa c&#243; t&#224;i khoản? <a href="#" data-toggle="modal" data-target="#dangky">Đăng k&#253; ngay ho&#224;n to&#224;n miễn ph&#237;</a><br>
                            Qu&#234;n mật khẩu? <a href="#" data-toggle="modal" data-target="#laymatkhau">Lấy lại mật khẩu</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-md btn-default" onclick="LoginAccount();">Đăng nhập&nbsp;&nbsp;<i class="fas fa-sign-in-alt" aria-hidden="true"></i></button>
            </div>
        </div>
    </div>
</div>
<div id="dangky" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">ĐĂNG kí thành viên</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 mg-bot15">
                        <div class="input-container">
                            <i class="fas fa-envelope icon"></i>
                            <input class="input-field" id="EmailRegister" name="EmailRegister" placeholder="Email"
                                   type="email" value="">
                        </div>
                    </div>
                    <div class="col-md-12 mg-bot15">
                        <div class="input-container">
                            <i class="fa fa-key icon"></i>
                            <input class="input-field" id="PasswordRegister" name="PasswordRegister"
                                   placeholder="Mật khẩu" type="password" value="">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="input-container">
                            <i class="fa fa-user icon"></i>
                            <input class="input-field" id="FullnameRegister" name="FullnameRegister"
                                   placeholder="Họ tên" type="text" value="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-md btn-default" onclick="RegisterAccount();">Đăng ký<i
                            class="fas fa-sign-in-alt" aria-hidden="true"></i></button>
            </div>
        </div>
    </div>
</div>
