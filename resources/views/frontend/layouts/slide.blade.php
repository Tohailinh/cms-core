<div class="n3-slideshow hidden-xs">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators hidden">
            <li data-target="#myCarousel1" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel1" data-slide-to="1" class=""></li>
            <li data-target="#myCarousel1" data-slide-to="2" class=""></li>
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <a href="" title="Bay thẳng đến Fukushima">
                    <img alt="Bay thẳng đến Fukushima" src="{{asset('')}}layouts/images/sl_190806_charter-fukushima-06-08-2019_1920x570.webp">
                </a>
            </div>
            <div class="item">
                <a href="" title="Tour Mỹ gi&#225; si&#234;u hấp dẫn">
                    <img alt="Tour Mỹ giá siêu hấp dẫn" src="{{asset('')}}layouts/images/sl_190802_tour-my-gia-soc-2-1920x570.webp">
                </a>
            </div>
            <div class="item ">
                <a href="https://en.travel.com.vn/tim-tour/1/2/0001-01-01/0/159/ket-qua.aspx?statusSeat=1" title="9 ng&#224;y chinh phục Thổ Nhĩ Kỳ">

                    <img alt="9 ngày chinh phục Thổ Nhĩ Kỳ" src="{{asset('')}}layouts/images/sl_190717_TNK_1920x570.webp">

                </a>
            </div>
        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control hidden-xs" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control hidden-xs" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
