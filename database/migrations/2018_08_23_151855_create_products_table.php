<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code_tour',255)->nullable();
            $table->string('name',255)->nullable();
            $table->string('alias',255)->nullable();
            $table->text('image')->nullable();
            $table->text('thumb')->nullable();
            $table->string('thumb_height', 100)->nullable();
            $table->string('thumb_width', 100)->nullable();
            $table->decimal('price', 16, 2)->default(0);
            $table->decimal('price_sale', 16, 2)->default(0);
            $table->string('title_seo',255)->nullable();
            $table->string('description',255)->nullable();
            $table->string('description_seo',255)->nullable();
            $table->string('keyword_seo',255)->nullable();
            $table->text('content')->nullable()->comment('Chương trình tour');
            $table->text('content_info')->nullable()->comment('Chi tiết tour');
            $table->text('note')->nullable()->comment('Lưu ý');
            $table->integer('follow')->default(1)->comment('Lượt theo dõi');
            $table->integer('seat', false, true)->default(1)->comment('Số chỗ ngồi');
            $table->tinyInteger('status', false, true)->default(1)->comment('0: Unactive , 1: active');
            $table->tinyInteger('type', false, true)->default(0)->comment('0: Tour thường , 1: Tour nổi bật , 1: Tour tiêu điểm , 2: Tour giảm giá');

            $table->date('date_start')->nullable()->comment('Thời gian khởi hành');
            $table->date('date_end')->nullable()->comment('Thời gian kêt thúc');
            $table->string('start_point',255)->nullable()->comment('Địa điềm khởi hành');
            $table->string('end_point',255)->nullable()->comment('Địa điểm kết thúc');
            $table->string('sum_time',255)->nullable()->comment('Tổng thời gian');
            $table->integer('transport',false,true)->default(0)->comment('0: oto , 1: tàu hỏa, 2: máy bay');

            $table->integer('category_id', false, true)->default(0);
            $table->integer('province_id', false, true)->default(0);
            $table->integer('district_id', false, true)->default(0);
            $table->integer('product_image_id', false, true)->default(0);

            $table->integer('user_id')->default(0)->unsigned();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->softDeletes();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
