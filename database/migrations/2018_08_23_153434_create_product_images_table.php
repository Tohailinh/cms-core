<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',255)->nullable();
            $table->string('title_seo', 255)->nullable();
            $table->text('description')->nullable();
            $table->string('description_seo', 255)->nullable();
            $table->text('link')->nullable();
            $table->text('image')->nullable();
            $table->text('thumb')->nullable();
            $table->string('thumb_height', 100)->nullable();
            $table->string('thumb_width', 100)->nullable();
            $table->integer('product_id', false, true)->default(0)->unsigned();
            $table->boolean('status')->default(false)->nullable();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->softDeletes();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product_images');
    }
}
