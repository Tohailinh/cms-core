<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', ['as' => 'home.index', 'uses' => 'Frontend\HomeController@index']);
Route::get('tin-tuc/', ['as' => 'new.index', 'uses' => 'Frontend\HomeController@new']);
Route::get('tin-tuc/{slug}/', ['as' => 'newInfo.index', 'uses' => 'Frontend\HomeController@newInfo']);

Route::get('tour/', ['as' => 'tour.index', 'uses' => 'Frontend\HomeController@tour']);
Route::get('tour/{slug}/', ['as' => 'tourInfo.index', 'uses' => 'Frontend\HomeController@tourInfo']);


Route::get('khuyen-mai/', ['as' => 'promotion.index', 'uses' => 'Frontend\HomeController@promotion']);
Route::get('khuyen-mai/{slug}/', ['as' => 'promotionInfo.index', 'uses' => 'Frontend\HomeController@promotionInfo']);


Route::get('lien-he/', ['as' => 'lienhe.index', 'uses' => 'Frontend\HomeController@lienhe']);
Route::get('visa/', ['as' => 'visa.index', 'uses' => 'Frontend\HomeController@visa']);

